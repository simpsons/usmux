all::

CD=cd
SED=sed
XARGS=xargs
FIND=find
MKDIR=mkdir -p
PRINTF=printf
DVIPS=dvips
DVIPDF=dvipdf
PDFLATEX=pdflatex -interaction=batchmode
MAKEINDEX=makeindex
LATEX=latex -interaction=batchmode
BIBTEX=bibtex
GZIP=gzip
PSNUP=psnup
PS2PDF=ps2pdf
PS2EPS=ps2eps
EPS2PDF=epstopdf
PDF2PS=pdf2ps
UNOCONV=unoconv


PREFIX=/usr/local
LIBDIR=$(PREFIX)/lib
JAVASHAREDIR=$(PREFIX)/share/java
BINDIR=$(PREFIX)/bin

USE_PDFLATEX=yes


-include usmux-env.mk
-include excluded-config.mk


usmux_obj += usmux
usmux_obj += method
usmux_obj += buf
usmux_obj += options
usmux_obj += command
usmux_obj += socket
usmux_obj += server
usmux_obj += multipiper
usmux_obj += tcpsock
usmux_lib += -lreact
usmux_lib += -lddslib

oldusmux_obj += oldusmux
oldusmux_obj += buf
oldusmux_obj += mux
oldusmux_obj += mux_proto
oldusmux_lib += -lreact
oldusmux_lib += -lddslib
#usmux_lib += -lefence

dummyserver_obj += dummyserver
dummyserver_obj += buf
dummyserver_obj += mux_proto

stresser_obj += stresser
stresser_obj += buf

test_binaries.c += dummyserver stresser
binaries.c += usmux



JARDEPS_SRCDIR=src/java/module
JARDEPS_DEPDIR=src/java
JARDEPS_TMPDIR=tmp
JARDEPS_MERGEDIR=src/java/merge



jars += $(SELECTED_JARS)

SELECTED_JARS += usmux_session
SELECTED_JARS += usmux_unix
SELECTED_JARS += usmux_server
SELECTED_JARS += usmux_demos

version_usmux_session=$(VERSION)
version_usmux_unix=$(VERSION)
version_usmux_server=$(VERSION)
version_usmux_demos=$(VERSION)

trees_usmux_server += stream_pair_mux
trees_usmux_server += tcp
trees_usmux_server += multipipe

trees_usmux_session += session
trees_usmux_unix += unix
trees_usmux_demos += demos


exports_session += uk.ac.lancs.scc.usmux
roots_session += $(session_classes:%=uk.ac.lancs.scc.usmux.%)
session_classes += SessionServerFactory
session_classes += MetadataHandler


exports_stream_pair_mux += uk.ac.lancs.scc.usmux.stream_pair_mux
roots_stream_pair_mux += $(stream_classes:%=uk.ac.lancs.scc.usmux.stream_pair_mux.%)
stream_classes += FilePairServerFactory
deps_stream_pair_mux += session


roots_unix += $(unix_classes:%=uk.ac.lancs.scc.usmux.unix.%)
unix_classes += UnixDomainSocketAttributes
unix_classes += UnixDomainMetadataHandler
deps_unix += session


exports_tcp += uk.ac.lancs.scc.usmux.tcp
roots_tcp += $(tcp_classes:%=uk.ac.lancs.scc.usmux.tcp.%)
tcp_classes += TCPServerFactory
deps_tcp += session


exports_multipipe += uk.ac.lancs.scc.usmux.multipipe
roots_multipipe += $(multipipe_classes:%=uk.ac.lancs.scc.usmux.multipipe.%)
multipipe_classes += MultipipeServerFactory
deps_multipipe += session


roots_demos += $(demos_classes:%=uk.ac.lancs.scc.usmux.demo.%)
demos_classes += Echo
deps_demos += session
deps_demos += unix

include binodeps.mk
-include jardeps.mk
-include jardeps-install.mk

DOC_PKGS += uk.ac.lancs.scc.usmux
DOC_PKGS += uk.ac.lancs.scc.usmux.unix
DOC_PKGS += uk.ac.lancs.scc.usmux.stream_pair_mux
DOC_PKGS += uk.ac.lancs.scc.usmux.tcp
DOC_PKGS += uk.ac.lancs.scc.usmux.multipipe
DOC_PKGS += uk.ac.lancs.scc.usmux.demo
DOC_OVERVIEW=src/java/overview.html
DOC_CLASSPATH += $(jars:%=$(JARDEPS_OUTDIR)/%.jar)
DOC_SRC=$(jardeps_srcdirs)
DOC_CORE=usmux$(DOC_CORE_SFX)











LATEX_GEN_FILE_SUFFIXES=aux idx ilg ind lof log lot toc bbl blg fls out

ifdef USE_PDFLATEX
tmp/latex/%.pdf: tmp/latex/%.tex
	$(MKDIR) "$(@D)"
	$(RM) $(LATEX_GEN_FILE_SUFFIXES:%=tmp/latex/$*.%)
	$(CD) tmp/latex ; $(PDFLATEX) -recorder "$*.tex"
	-$(CD) tmp/latex ; $(BIBTEX) "$*"
	$(CD) tmp/latex ; $(PDFLATEX) "$*.tex"
	$(CD) tmp/latex ; $(PDFLATEX) "$*.tex"
	$(CD) tmp/latex ; $(MAKEINDEX) "$*"
	$(CD) tmp/latex ; $(PDFLATEX) "$*.tex"

IMG_SUFFIX=.pdf

tmp/%.pdf: gfx/%.eps
	$(MKDIR) "$(@D)"
	$(EPS2PDF) $(EPS2PDFFLAGS) "$<" --outfile "$@"

PART_DEP_SUFFIX=.pdf

else



tmp/latex/%.dvi: tmp/latex/%.tex
	$(MKDIR) "$(@D)"
	$(RM) $(LATEX_GEN_FILE_SUFFIXES:%=tmp/latex/$*.%)
	$(CD) tmp/latex ; $(LATEX) -recorder "$<"
	-$(CD) tmp/latex ; $(BIBTEX) "$*"
	$(CD) tmp/latex ; $(LATEX) "$<"
	$(CD) tmp/latex ; $(LATEX) "$<"
	$(CD) tmp/latex ; $(MAKEINDEX) "$*"
	$(CD) tmp/latex ; $(LATEX) "$<"

IMG_SUFFIX=.eps

PART_DEP_SUFFIX=.dvi

endif



#usmux.dvi usmux.pdf: references.bib


PARTS += abstract
PARTS += forematter
PARTS += mux-protocol
PARTS += multipipe-protocol
PARTS += tcp-protocol
PARTS += java-api
PARTS += arch
PARTS += new-mux
PARTS += meta-data

SPOOLS += java-server
SPOOLS += get-unix-attrs
SPOOLS += reg-server-factory
SPOOLS += reg-meta-handler

IMAGES += arch
IMAGES += arch-pipe-mux
IMAGES += arch-pipe-pairs

tmp/latex/usmux$(PART_DEP_SUFFIX): $(PARTS:%=tmp/latex/%.tex) \
	$(IMAGES:%=tmp/latex/figs/%$(IMG_SUFFIX)) \
	$(SPOOLS:%=tmp/latex/%.spool)



tmp/latex/figs/%.pdf: tmp/latex/figs/%.eps
	$(EPS2PDF) --outfile='$@' '$<'

tmp/latex/figs/%.eps: src/latex/%.odg
	$(MKDIR) '$(@D)'
	$(UNOCONV) $(UNOCONVFLAGS) --stdout -f eps '$<' | $(PS2EPS) > '$@'

tmp/latex/%: src/latex/%
	@$(PRINTF) 'Copying LaTeX source %s\n' '$*'
	@$(MKDIR) '$(@D)'
	@$(CP) '$<' '$@'

out/%.pdf: tmp/latex/%.pdf
	@$(MKDIR) '$(@D)'
	$(CP) '$<' '$@'

%.ps: %.dvi
	$(DVIPS) '$<' '$@'

%.pdf: %.ps
	$(PS2PDF) $(PS2PDFFLAGS) '$<' '$@'

%.pdf: %.eps
	$(EPS2PDF) --outfile='$@' '$<'

%.pdf: %.dvi
	$(DVIPDF) -o '$@' '$<'

%.gz: %
	$(GZIP) < '$<' > '$@'

clean:: tidy
	$(RM) $(LATEX_GEN_FILE_SUFFIXES:%=tmp/latex/*.%)
	-$(RM) tmp/latex/*.dvi

blank:: clean
	-$(RM) out/*.ps
	-$(RM) out/*.pdf


all:: installed-jars
installed-jars:: $(SELECTED_JARS:%=$(JARDEPS_OUTDIR)/%.jar)
installed-jars:: $(SELECTED_JARS:%=$(JARDEPS_OUTDIR)/%-src.zip)

install-jar-%::
	@$(call JARDEPS_INSTALL,$(JAVASHAREDIR),$*,$(version_$*))

install-jars:: $(SELECTED_JARS:%=install-jar-%)

install:: install-jars install-binaries

YEARS=2012,2014-15

update-licence:
	$(FIND) . -name '.svn' -prune -or -type f -print0 | $(XARGS) -0 \
	$(SED) -i 's/Copyright (c)\s[0-9,]\+\sRegents of the University of Lancaster/Copyright (c) $(YEARS), Regents of the University of Lancaster/g'


all:: installed-binaries

tidy::
	@$(PRINTF) 'Removing detritus\n'
	@$(FIND) . -name '*~' -delete
