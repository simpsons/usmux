\documentclass[a4paper,twocolumn,twoside]{article}

\usepackage{hyperref}
\usepackage{bytefield}
\usepackage{makeidx}
\usepackage{times}
\usepackage{ifpdf}
\usepackage{ifdraft}
\usepackage{verbatim}
\usepackage{caption}
\usepackage{balance}
\usepackage{amssymb,amsmath}
\usepackage{enumitem}
\usepackage{titlesec}
\usepackage{caption}

\captionsetup{font=small,format=plain,labelfont={bf,sf},textfont={bf,sf}}
%\pagestyle{headings}

\ifpdf
\usepackage[pdftex]{graphicx}
\DeclareGraphicsExtensions{.pdf,.jpeg,.png}
\else
\usepackage[dvips]{graphicx}
\DeclareGraphicsExtensions{.eps}
\fi

\newcounter{SECTIONLEVEL}
\setcounter{SECTIONLEVEL}{0}

% Use this form with report.
%% \newenvironment{SECTION}[1]%
%% {%
%% \addtocounter{SECTIONLEVEL}{+1}%
%% \ifcase\value{SECTIONLEVEL}%
%% \or %
%% \chapter{#1}%
%% \or %
%% \section{#1}%
%% \or %
%% \subsection{#1}%
%% \or %
%% \subsubsection{#1}% \quad\\*
%% \or %
%% \paragraph{#1}%
%% \or %
%% \subparagraph{#1}%
%% \else %
%% #1 %
%% \fi%
%% }%
%% {\addtocounter{SECTIONLEVEL}{-1}}

%% \newenvironment{SECTION*}[1]%
%% {%
%% \addtocounter{SECTIONLEVEL}{+1}%
%% \ifcase\value{SECTIONLEVEL}%
%% \or %
%% \chapter*{#1}%
%% \or %
%% \section*{#1}%
%% \or %
%% \subsection*{#1}%
%% \or %
%% \subsubsection*{#1}\quad\\*%
%% \or %
%% \paragraph*{#1}%
%% \or %
%% \subparagraph*{#1}%
%% \else %
%% #1 %
%% \fi%
%% }%
%% {\addtocounter{SECTIONLEVEL}{-1}}



\newenvironment{SECTION}[1]%
{%
\addtocounter{SECTIONLEVEL}{+1}%
\ifcase\value{SECTIONLEVEL}%
\or %
\section{#1}%
\or %
\subsection{#1}%
\or %
\subsubsection{#1}%
\or %
\paragraph{#1}% \quad\\*
\or %
\subparagraph{#1}%
\else %
#1 %
\fi%
}%
{\addtocounter{SECTIONLEVEL}{-1}}

\newenvironment{SECTION*}[1]%
{%
\addtocounter{SECTIONLEVEL}{+1}%
\ifcase\value{SECTIONLEVEL}%
\or %
\section*{#1}%
\or %
\subsection*{#1}%
\or %
\subsubsection*{#1}\quad\\*%
\or %
\paragraph*{#1}%
\or %
\subparagraph*{#1}%
\else %
#1 %
\fi%
}%
{\addtocounter{SECTIONLEVEL}{-1}}




\newcommand{\defn}[1]{\emph{#1}}

\newcommand{\diagram}[2]{
\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{figs/#1}
\caption{#2}
\label{fig:#1}
\end{figure}
}

\newcommand{\widediagram}[2]{
\begin{figure*}
\centering
\includegraphics[width=0.8\linewidth]{figs/#1}
\caption{#2}
\label{fig:#1}
\end{figure*}
}

\newcommand{\CLASS}[2]{\texttt{#2}\footnote{\tiny\texttt{#1.#2}}}

\newcommand{\format}[1]{

\begin{centering}

{\scriptsize \texttt{#1}}

\end{centering}}



\newcommand{\spool}[2]{
\begin{table}[htbp]
\small
\verbatiminput{#1.spool}
\caption{#2}
\label{list:#1}
\end{table}
}

\newcommand{\widespool}[2]{
\begin{table*}[tbph]
\small
\verbatiminput{#1.spool}
\caption{#2}
\label{list:#1}
\end{table*}
}

\setdescription{style=nextline}

\titleformat{\paragraph}[hang]{\normalfont\normalsize\bfseries}{\theparagraph}{1em}{}



\newcommand{\ed}[1]{{\footnotesize \textbf{\textsf{[#1]}}}}
\ifdraft{
}{
%\newcommand{\ed}[1]{}
}


\makeindex

\begin{document}

\input{forematter}

\maketitle

\begin{abstract}
\input{abstract}
\end{abstract}

%\tableofcontents
%\listoffigures
%\listoftables

\begin{SECTION}{Introduction}

At first, Java appears inherently slow by its design.
%
Java source code is first compiled into bytecode, an assembly language for a virtual processor called the Java Virtual Machine (JVM).
%
The JVM then processes each (virtual-)machine code instruction to produce the behaviour represented by the source code, and it is this processing which is slower that an equivalent program compiled to a native machine code.
%
The benefit is a write-once, run-anywhere (WORA) environment making Java programs maximally portable.
%
The additional layer of abstraction (the JVM must ultimately be written in native machine code) also provides greater integrity, increasing security, making dynamic loading of foreign code possible, and detecting programmer error.

The creators of Java have also sought speed improvements along several avenues.
%
Start-up times have been reduced by increasing the modularity of the standard library, and by permitting some lazy class initialization.
%
Bytecode can be compiled to native code as it is loaded with a just-in-time (JIT) compiler installed in the JVM.
%
The JVM may also monitor its process, looking for sections of code most frequently executed, in order to prioritize its optimization.
%
It may also in-line sections of code to reduce the overhead of method invocation.
%
Note that these optimizations apply at run time in the JVM, whereas the compiler mostly avoids them, as the effectiveness of these optimizations depend greatly on run-time characteristics not available during compilation.

Altogether, these optimizations can allow a Java process to run \emph{at least as fast} as a natively compiled program, while still retaining the benefits of portability.
%
However, each one also comes with an overhead, which must be amortized by its results being applicable over a sufficiently long period.
%
For example, a Java program used as a short-lived command cannot easily benefit from JIT because JIT-ed code may only be executed a small number of times before exiting (and the next invocation will have to be re-JIT-ed), nor from HotSpot because the process terminates before enough analysis has been performed.
%
Consequently, Java performs best when used for long-running programs, such as desktop applications and server processes.

Some environments are designed to work with short-lived processes.
%
A command shell such as Bash repeatedly executes external commands, and the Common Gateway Interface (CGI) used by web servers to delegate requests to user programs mandates one process per request.
%
These are therefore poor environments in which to use Java.

\begin{SECTION}{Persistence for CGI programs}

Java isn't the only language to suffer great overheads due to repeated start-ups.
%
Any per-process delay on a CGI program, in whatever language, makes CGI the bottleneck for any high-demand service.
%
\href{http://www.fastcgi.com/drupal/}{FastCGI}\footnote{http://www.fastcgi.com/drupal/} was devised to address this issue by using a regular TCP connection between the web server and the CGI program, which runs persistently and serves multiple requests, instead of running once per request.
%
This is a good approach, but the Usmux project aims for a more general mechanism than one built to support CGI;
%
it should be possible to implement a FastCGI-like mechanism over Usmux.

\ed{upcgi}

\end{SECTION}



\begin{SECTION}{Java support for Unix-domain sockets}

Another approach to allow a Java process to run persistently is to provide a library supporting Unix-domain sockets.
%
Several implementations are summarized here:

\begin{description}

\item[\href{https://github.com/mcfunley/juds}{JUDS}\footnote{https://github.com/mcfunley/juds}]

\ed{Investigate.}

\item[\href{http://code.google.com/p/junixsocket/}{junixsocket}\footnote{http://code.google.com/p/junixsocket/}]

This is a JNI library that uses the the Java Sockets API, and supports RMI.
%
Fitting in with the Sockets API is a feat, as much of it is based on Internet-domain sockets, and doesn't seem to fit well when a broader abstraction is required.

\item[J-BUDS]

This is an orphaned project.
%
\ed{Investigate.}

\item[\texttt{gnu.net.local}]

This is another oprhaned project.
%
\ed{Investigate.}

\end{description}

Providing native support for Unix-domain sockets in Java seems to create more problems than it solves.
%
It is necessarily platform-specific, so the Java code that uses the support must be wrapped behind a platform-independent abstraction if the program is to remain platform-independent itself.
%
The fact that the Internet-domain nature of the Java Sockets API leaks through its own abstraction means that that API does not help to hide platform-specificity.

\end{SECTION}



\begin{SECTION}{Usmux}

\defn{Usmux} is an approach to allow one-invocation-per-request environments like CGI to be used with persistent Java processes.
%
At the time of writing, the implementation is POSIX-specific, but the architecture is platform-independent.
%
Implementations on other platforms may be possible.

On a POSIX system, the \texttt{usmux} command takes a Java command as its trailing arguments.
%
It also opens a Unix-domain socket, creates two named pipes, and injects the names of these pipes as a configuration string into the Java command, which is then executed in the background.
%
It then accepts connections on the socket, and presents them as \defn{sessions} to the Java process by creating another pair of named pipes per session, using its original pipes to notify the Java process of them, and relaying data between the pipes and the socket.
%
As a result, the Java process can run persistently, but be invoked by multiple clients over a Unix-domain socket.

On other platforms, a similar command could use other platform-specific features to interact with a server running the same Java software.
%
Only the injected configuration string needs to change, and the Usmux Java library will dynamically load in the matching server-side implementation.

Even under POSIX, other mechanisms are possible too.
%
For example, \texttt{usmux} could create a pair of named pipes, and multiplex several sessions over them.
%
(However, experience has shown that this is error-prone to code, and hampers over-all through-put.)

The use of a Unix-domain socket to communicate with clients has several advantages.
%
First, as its rendezvous point is part of the filesystem, the host's native access control can be applied to it.
%
For more sophisticated control, it is possible to inform the server of which user or group is actually connecting.
%
Finally, invocation of the client could exploit external authentication protocols such as SSH.

\end{SECTION}

\end{SECTION}

\input{arch}

\begin{SECTION}{Multiplexing schemes}

Three multiplexing schemes are defined.
%
Historically, the single-pipe scheme (\S\ref{sec:mux-proto}) was defined first.
%
This attempts to multiplex sessions over a single pair of simplex channels, so communication must be divided into discrete messages, and peers must carefully signal availability of buffer space to avoid session activities impeding each other.
%
The result appears to have poor performance.

The multipipe scheme (\S\ref{sec:multipipe-proto}) attempts to improve performance by allowing each session to use a dedicated pair of pipes.
%
An additional pair are used for signalling.

The TCP scheme (\S\ref{sec:tcp-proto}) simply allows the server process to use a regular TCP socket.
%
However, to keep parity with other schemes, there is an additional step enabling the daemon to detect that the server has reached readiness, and then leave it running in the background.
%
The scheme also supports simple access control.

\input{multipipe-protocol}

\input{mux-protocol}

\input{tcp-protocol}

\end{SECTION}

\input{new-mux}

\onecolumn

%\begin{SECTION*}{Acknowledgements}

%\end{SECTION*}




%\bibliographystyle{alpha}
%\raggedright
%\bibliography{references}

%\printindex




\end{document}
