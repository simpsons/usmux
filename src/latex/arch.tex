\begin{SECTION}{Architecture}

\widediagram{arch}{Basic architecture}

Usmux aims to allow a \defn{client} to initiate a \defn{session} with a \defn{server}.
%
Each session is a pair of simplex streams carrying byte-oriented data.
%
\defn{Upstream} denotes travel from the client to the server, while \defn{downstream} denotes travel in the opposite direction.
%
Either peer may terminate either stream at any time, and the session is terminated when both streams are terminated.

The basic Usmux architecture is shown in Figure~\ref{fig:arch}.
%
The Usmux daemon sits between the server and its clients, and relays messages between them.
%
The daemon is also responsible for forking the server into the background.

\widediagram{arch-pipe-mux}{Multiplexing between daemon and server over named pipes}

Various communication \defn{schemes} may be employed between client and daemon, and between daemon and server.
%
In some schemes, client and server communicate directly, with the daemon playing no part after start-up.
%
Client-daemon schemes may involve a Unix-domain socket, an Internet-domain socket, or any other connection-oriented communication supported by the host platform(s).
%
Daemon-server schemes may be similarly varied, but are constrained mainly by having to appear platform-independent in order to be accessible from Java.
%
Effectively, the daemon acts as an adapter between the client's use of a platform-specific technology and the server's requirement for a platform-independent one.

The session abstraction must necessarily be very primitive, as the only characteristic that a session has under any scheme is that it consists of two streams.
%
However, the server may be able to make use of information about the connecting client, even though it is scheme-specific.
%
When the client connects to the daemon with a Unix-domain socket, its details (process id, user id, group id) can be detected by the daemon, and be relayed to the server.
%
When the client connects directly to the server via TCP, its host and port can be made available to the server.
%
These are exposed to the server in a generic form as \defn{client meta-data}, described in \S\ref{sec:proto-meta-types}.
%
When the daemon is involved in implementing sessions, the protocols involved in talking to the server allow the daemon to relay this information as opaque binary data.

\widediagram{arch-pipe-pairs}{Per-client pipe pairs between daemon and server}

Figure~\ref{fig:arch-pipe-mux} depicts one daemon-server scheme which can appear platform-independent to the server, in which all connections accepted by the daemon are multiplexed over two simplex named pipes.
%
This protocol is described in \S\ref{sec:mux-proto}, and is capable of relaying arbitrary client meta-data to the server.

Alternatively, the daemon may establish a pair of pipes per session, and inform the server of each one, plus client meta-data, over a pipe pair dedicated for signalling, as shown in Figure~\ref{fig:arch-pipe-pairs}.
%
This scheme is described in \S\ref{sec:multipipe-scheme}, and is also capable of relaying arbitrary client meta-data to the server.

Under another alternative, the daemon serves only to fork the server into the background.
%
The server then opens an Internet-domain socket, and accepts TCP connections directly from the client as sessions.

It is essential that a Java implementation of the server component can operate as independently as possible from these various communication schemes between client, daemon and server.
%
To this end, a Java API (\S\ref{sec:java-api}) is defined to provide an abstraction of sessions.
%
This abstraction provides only an input stream and an output stream, with generic access to client meta-data.

\input{meta-data}

\input{java-api}



\end{SECTION}
