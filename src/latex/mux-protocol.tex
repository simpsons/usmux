\begin{SECTION}{The single-pipe scheme}\label{sec:mux-proto}

The single-pipe scheme allows multiple sessions to share a single duplex channel, over which a multiplexing protocol is run.
%
The scheme uses a configuration string of the form \texttt{pipe:\textit{downfile};\textit{upfile}}, where \textit{upfile} is the name of a named pipe that forms the upstream half of the channel, and \textit{downfile} is the name of the pipe that forms the downstream half.

The multiplexing protocol consists of several byte-oriented messages transmitted over a reliable, full-duplex channel between two \defn{end-points}.
%
Each end-point is the \defn{peer} of the other.
%
One end-point is the \defn{server process} (or just \defn{server}), while the other is the \defn{daemon process} (or just \defn{daemon}).
%
Each \defn{session} consists of a pair of \defn{streams}, one in each direction between the two end-points.

The protocol is designed to support connections accepted on a Unix-domain socket as sessions multiplexed over the pair of named pipes used by Usmux, and permits meta-data about each connection to be supplied to the Java process.
%
However, it is not dependent on named pipes or Unix-domain sockets, so it can be applied to hosts that support neither of these concepts.
%
To this end, the protocol regards the meta-data as an opaque block.
%
Similarly, it is not tied to Java in any way, so the server process can be written in any language.

The protocol has an initialization phase, in which the server process transmits a `ready' message to indicate its readiness to receive sessions.
%
This phase also allows the server to indicate how much meta-data it will accept per session.

After intitialization, sessions can be relayed over the channel.
%
Each session has a handshake phase, during which meta-data is supplied to the server, and the server and daemon exchange references.
%
A data phase follows, in which each end-point repeatedly updates its peer about the space available in its reception buffer, and transmits data according to space available in its peer's buffer.




\begin{SECTION}{Initialization phase}\label{sec:proto-init}

The initialization phase consists of the daemon waiting for the server to send a \texttt{RDY} message.
%
This message may contain a meta-data limit, the maximum number of bytes the server is prepared to receive as payload for future \texttt{NEW} messages sent to initiate each session.

No other messages may be transmitted over the channel until \texttt{RDY} has been received.

\end{SECTION}

\begin{SECTION}{Handshake phase}\label{sec:proto-handshake}

Each session begins with a handshake phase.
%
The daemon initiates each session by transmitting a \texttt{NEW} message.
%
This includes the session id that the daemon will use in all future messages concerning this session.
%
Any additional data is considered to be meta-data, which the server may use in any way it sees fit.

The server responds to each \texttt{NEW} message with an \texttt{ACK} message.
%
This echoes back the daemon's session id, and includes the server's session id.

From this point on, each message related to this session and transmitted by the daemon will use the server's session id, while the server will correspondingly use the daemon's session id.

\end{SECTION}

\begin{SECTION}{Data phase}\label{sec:proto-data}

After a session has been established by a handshake phase, either party may transmit \texttt{CTS} messages according to the space it has available to receive data for the relevant session.
%
The effect of multiple \texttt{CTS} messages is cummulative, so a peer can send upto the sum of the amounts specified by received \texttt{CTS} messages, minus that which it has already sent.
%
An end-point can use this to ensure that its peer can send a complete message without blocking due to the receiver's buffer being full.

To transmit data, a peer sends a \texttt{STR} message, whose payload starts by identifying the session id, and continues with the data.
%
The sum of the message's data length (the payload length minus two) and of the data lengths of all prior \texttt{STR} messages for the same session must not exceed the sum of the clearances of all \texttt{CTS} messages received for the same session.

When a peer has no more data to send, it must send an \texttt{EOS} message to close the session.
%
A peer can also indicate that it will not use any more data by sending a \texttt{STP} message.

\end{SECTION}

\begin{SECTION}{Messages}\label{sec:proto-msgs}

\begin{table*}[htbp]
\centering
\begin{tabular}{ | c | c | c | l | l | }

\hline

Type & Value & Direction & Meaning & Parameters \\

\hline

\texttt{UNK} & 0 & never sent & Unknown & None \\

\texttt{NOP} & 1 & never sent & No operation & None \\

\texttt{RDY} & 2 & server-to-daemon & Server ready & Meta-data limit \\

\texttt{NEW} & 3 & daemon-to-server & New session & Session id and meta-data \\

\texttt{ACK} & 4 & server-to-daemon & Session acknowledged & Session ids \\

\texttt{CTS} & 5 & both & Clear to send & Session id and clearance \\

\texttt{STR} & 6 & both & Stream data & Session id, data length and data \\

\texttt{EOS} & 7 & both & End of stream & Session id \\

\texttt{STP} & 8 & both & Stop stream & Session id \\

\hline

\end{tabular}
\caption{Message types}\label{tab:proto-msgs}
\end{table*}

Each message has a type and a length (Figure~\ref{tab:proto-msg}), so unrecognized messages can be skipped.
%
The message type is a two-byte big-endian field at the start of the message.
%
The length is the two-byte big-endian field that follows it.
%
The whole message need not be word-aligned.
%
Message types are summarized in Table~\ref{tab:proto-msgs}.



\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{message type} & \bitbox{16}{payload length} \\
\wordbox[lrt]{1}{payload} \\
\skippedwords \\
\wordbox[lrb]{1}{payload}
\end{bytefield}
\caption{Message format}\label{tab:proto-msg}
\end{figure}



\begin{SECTION}{Unused message types}\label{sec:proto-msg-unused}

The \texttt{UNK} and \texttt{NOP} message types must never be sent.
%
The receiver may ignore them entirely.
%
They are intended to be used by the daemon or the server internally to indicate that no complete message header has been received, or that the remainder of a message may be ignored.

\end{SECTION}


\begin{SECTION}{\texttt{RDY} -- Server ready}\label{sec:proto-msg-ready}

The \texttt{RDY} message type (Figure~\ref{tab:proto-msg-rdy}) is the first one sent over the channel.
%
It is sent only once, and by the server to the daemon.
%
If its payload is at least two bytes, these are taken as a big-endian unsigned integer specifying the maximum number of meta-data bytes that the server will accept with each session.
%
Any other payload, or an incomplete payload, may be ignored.

\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{2 (\texttt{RDY})} & \bitbox{16}{payload length} \\
\bitbox{16}{meta-data limit}
\end{bytefield}
\caption{The \texttt{RDY} message -- server ready}\label{tab:proto-msg-rdy}
\end{figure}

\end{SECTION}


\begin{SECTION}{Handshake message types}\label{sec:proto-msg-handshake}

Sessions are initiated by the daemon.
%
For each session, it issues a \texttt{NEW} message (Figure~\ref{tab:proto-msg-new}), with at least two bytes of payload giving the daemon's session id, which the server must use in all future communication pertaining to this session.

Any remaining payload is interpreted by the server as meta-data.
%
For example, if the session is a relay for a Unix-domain socket, the meta-data may contain the process id, user id and group id of the calling process.
%
\S\ref{sec:proto-meta-types} lists some meta-data types.


\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{3 (\texttt{NEW})} & \bitbox{16}{payload length} \\
\bitbox{16}{daemon session id} & \bitbox[lrt]{16}{} \\
\wordbox[lr]{1}{meta-data} \\
\skippedwords \\
\wordbox[lrb]{1}{}
\end{bytefield}
\caption{The \texttt{NEW} message -- new session}\label{tab:proto-msg-new}
\end{figure}



On receipt of a \texttt{NEW} message, the server must generate a session id of its own, and respond with an \texttt{ACK} message (Figure~\ref{tab:proto-msg-ack}).
%
The first two bytes of its payload form the daemon's session id; the next two form the server's session id, which the daemon must use in all future communication pertaining to this session.
%
Additional payload bytes carry no meaning, and can be ignored.


\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{4 (\texttt{ACK})} & \bitbox{16}{payload length} \\
\bitbox{16}{daemon session id} & \bitbox{16}{server session id}
\end{bytefield}
\caption{The \texttt{ACK} message -- session acknowledged}\label{tab:proto-msg-ack}
\end{figure}


\end{SECTION}


\begin{SECTION}{Data message types}\label{sec:proto-msg-data}

For each session, each end-point must only send the amount of data it has been permitted by its peer.
%
One end-point permits its peer to send data by sending \texttt{CTS} messages (Figure~\ref{tab:proto-msg-cts}), which includes the peer's session id, and a 32-bit \emph{signed} length.
%
Each message instructs the receiver to accumulate provided lengths, and to send no more data than the accumulated value, including data already send on the session.

\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{5 (\texttt{CTS})} & \bitbox{16}{payload length} \\
\bitbox{16}{peer's session id} & \bitbox{16}{clearance} \\
\bitbox{16}{clearance}
\end{bytefield}
\caption{The \texttt{CTS} message -- clear to send}\label{tab:proto-msg-cts}
\end{figure}



To send data, an end-point sends a \texttt{STR} message (Figure~\ref{tab:proto-msg-str}) to its peer.
%
This includes the peer's session id, with the remaining payload being the stream data.

\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{6 (\texttt{STR})} & \bitbox{16}{payload length} \\
\bitbox{16}{peer's session id} & \bitbox[lrt]{16}{} \\
\wordbox[lr]{1}{stream data} \\
\skippedwords \\
\wordbox[lrb]{1}{}
\end{bytefield}
\caption{The \texttt{STR} message -- stream data}\label{tab:proto-msg-str}
\end{figure}



When an end-point has no more data to send on its stream, it must send an \texttt{EOS} message (Figure~\ref{tab:proto-msg-eos}), unless it has already received a \texttt{STP} message for the session.
%
This includes the peer's session id.
%
It must not send any further \texttt{STR} messages after that.

\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{7 (\texttt{EOS})} & \bitbox{16}{payload length} \\
\bitbox{16}{peer's session id}
\end{bytefield}
\caption{The \texttt{EOS} message -- end of stream}\label{tab:proto-msg-eos}
\end{figure}


When an end-point cannot use data received on a stream, it may send a \texttt{STP} message (Figure~\ref{tab:proto-msg-stp}).
%
This includes the peer's session id.
%
The peer need not send any further \texttt{STR} or \texttt{EOS} messages on the session, and the end-point need not use them.

\begin{figure}[htbp]
\centering
\begin{bytefield}{32}
\bitheader{0,15,16,31} \\
\bitbox{16}{8 (\texttt{STP})} & \bitbox{16}{payload length} \\
\bitbox{16}{peer's session id}
\end{bytefield}
\caption{The \texttt{STP} message -- stop stream}\label{tab:proto-msg-stp}
\end{figure}



\end{SECTION}

\end{SECTION}


\end{SECTION}
