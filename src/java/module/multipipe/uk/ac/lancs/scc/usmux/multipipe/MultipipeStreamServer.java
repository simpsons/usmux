/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.multipipe;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import uk.ac.lancs.scc.usmux.Session;
import uk.ac.lancs.scc.usmux.SessionException;
import uk.ac.lancs.scc.usmux.SessionServer;

/**
 * Uses an upstream control pipe to detect new sessions, each using its
 * own pair of pipes. A downstream control pipe is also used to signal
 * readiness of the server.
 * 
 * @author simpsons
 */
public final class MultipipeStreamServer implements SessionServer {
    private final BufferedReader upstreamControl;
    private final OutputStream downstreamControl;

    /**
     * Create a server that expects each new session to have its own
     * pair of pipes. Two control pipes are provided. The upstream pipe
     * is opened for reading, and the downstream pipe for writing, and
     * then the server deletes them. When ready, the server sends on the
     * downstream pipe to signal readiness. It then continuously reads
     * pairs of lines from the upstream pipe. Each pair specifies the
     * names of the upstream and downstream pipes to be used for a new
     * session. These pipes are deleted as soon as they are opened, so
     * it is assumed that the peer has also already opened them.
     * 
     * <p>
     * A blank line means the server is ready. Closing the downstream
     * control pipe means the server is no longer accepting connections.
     * 
     * @param upstreamControlName the filename of the upstream control
     * pipe
     * 
     * @param downstreamControlName the filename of the downstream
     * control pipe
     * 
     * @throws IOException if there was an error in opening the pipes
     */
    public MultipipeStreamServer(File upstreamControlName,
				 File downstreamControlName)
	    throws IOException {
	try {
	    /* We must open the downstream control channel first, to
	     * ensure that the daemon will unblock while waiting to open
	     * it for reading. */
	    downstreamControl = new FileOutputStream(downstreamControlName);
	    try {
		upstreamControl =
		    new BufferedReader(new FileReader(upstreamControlName));
	    } catch (IOException ex) {
		try {
		    downstreamControl.close();
		} catch (IOException ex2) {
		    ex.addSuppressed(ex2);
		}
		throw ex;
	    }
	} finally {
	    upstreamControlName.delete();
	    downstreamControlName.delete();
	}
    }

    private boolean terminated = false;

    @Override
    public synchronized Session accept() throws SessionException {
	if (terminated) return null;
	try {
	    String inName = upstreamControl.readLine();
	    if (inName == null) {
		terminated = true;
		return null;
	    }
	    File inFile = new File(inName);
	    try {
		String outName = upstreamControl.readLine();
		if (outName == null) {
		    terminated = true;
		    return null;
		}
		File outFile = new File(outName);
		try {
		    OutputStream out = new FileOutputStream(outFile);
		    InputStream in = new FileInputStream(inFile);
		    SimplePipePairSession result =
			new SimplePipePairSession(in, out);
		    result.start();
		    outFile.delete();
		    return result;
		} catch (IOException ex) {
		    throw new SessionException("opening session on input "
			+ inFile + " and output " + outFile, ex);
		} finally {
		    outFile.delete();
		}
	    } catch (IOException ex) {
		throw new SessionException("opening session on input "
		    + inFile, ex);
	    } finally {
		inFile.delete();
	    }
	} catch (IOException ex) {
	    throw new SessionException("opening session", ex);
	}
    }

    @Override
    public void close() throws SessionException {
	SessionException res = null;
	try {
	    downstreamControl.close();
	} catch (IOException ex) {
	    res = new SessionException("closing", ex);
	}
	try {
	    upstreamControl.close();
	} catch (IOException ex) {
	    if (res == null)
		res = new SessionException("closing", ex);
	    else
		res.addSuppressed(ex);
	}
	if (res != null) throw res;
    }

    private boolean started = false;

    @Override
    public void start() throws SessionException {
	synchronized (this) {
	    if (started) return;
	    started = true;
	}

	try {
	    /* We tell the daemon that we're ready for sessions by
	     * sending a single byte. */
	    downstreamControl.write(0x0a);
	    downstreamControl.flush();
	} catch (IOException ex) {
	    terminated = true;
	    throw new SessionException("starting", ex);
	}
    }
}
