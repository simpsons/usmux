/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.multipipe;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ServiceLoader;

import uk.ac.lancs.scc.usmux.MetadataBinding;
import uk.ac.lancs.scc.usmux.MetadataHandler;
import uk.ac.lancs.scc.usmux.Session;

/**
 * Operates a session on two streams. The input stream begins with two
 * bytes giving the meta-data length, followed by the meta-data. After
 * that, everything is stream content.
 * 
 * @author simpsons
 */
public final class SimplePipePairSession implements Session {
    private final DataInputStream in;
    private final OutputStream out;

    private volatile boolean bindingReady;
    private MetadataBinding binding;
    private IOException metaError;

    /**
     * Create a session on two streams.
     * 
     * @param in the input stream, beginning with meta-data
     * 
     * @param out the output stream
     */
    public SimplePipePairSession(InputStream in, OutputStream out) {
	this.in =
	    in instanceof DataInputStream ? (DataInputStream) in
		: new DataInputStream(in);
	this.out = out;
    }

    /**
     * Start processing the meta-data on the input stream. The methods
     * {@link #close()}, {@link #getInputStream()} and
     * {@link #getAttributes(Class)} block until this is done, so this
     * processing must be started first.
     */
    void start() {
	new Thread() {
	    public void run() {
		try {
		    /* Read the meta-data length from the input stream. */
		    int len = in.readUnsignedShort();

		    /* Read the meta-data from the input stream. */
		    byte[] buf = new byte[len];
		    in.readFully(buf);

		    /* Interpret the meta-data. */
		    ServiceLoader<MetadataHandler> service =
			ServiceLoader.load(MetadataHandler.class);
		    for (MetadataHandler handler : service) {
			binding = handler.convert(buf);
			if (binding != null) break;
		    }
		} catch (IOException ex) {
		    metaError = ex;
		    try {
			in.close();
		    } catch (IOException ex1) {
			ex.addSuppressed(ex1);
		    }
		    try {
			out.close();
		    } catch (IOException ex1) {
			ex.addSuppressed(ex1);
		    }
		} finally {
		    /* Signal that the stream is available for use. */
		    metaReady();
		}
	    }
	}.start();
    }

    private synchronized void metaReady() {
	bindingReady = true;
	notifyAll();
    }

    /**
     * Wait until the meta-data has been prepared. If interrupted, it
     * will continue waiting until the meta-data is ready, but will
     * re-interrupt the thread before returning.
     */
    private synchronized void waitForMeta() {
	boolean interrupted = false;
	try {
	    while (!bindingReady) {
		try {
		    wait();
		} catch (InterruptedException ex) {
		    interrupted = true;
		}
	    }
	} finally {
	    if (interrupted) Thread.currentThread().interrupt();
	}
    }

    /**
     * Wait for meta-data and then close all streams.
     */
    @Override
    public void close() throws IOException {
	IOException res = null;
	waitForMeta();

	try {
	    in.close();
	} catch (IOException ex) {
	    if (res == null)
		res = ex;
	    else
		res.addSuppressed(ex);
	}

	try {
	    out.close();
	} catch (IOException ex) {
	    if (res == null)
		res = ex;
	    else
		res.addSuppressed(ex);
	}
    }

    @Override
    public InputStream getInputStream() throws IOException {
	waitForMeta();
	if (metaError != null) throw new IOException("input", metaError);
	return in;
    }

    @Override
    public OutputStream getOutputStream() {
	return out;
    }

    @Override
    public <T> T getAttributes(Class<T> type) {
	waitForMeta();
	if (metaError != null) return null;
	if (binding == null) return null;
	if (binding.types.contains(type)) return type.cast(binding.data);
	return null;
    }
}
