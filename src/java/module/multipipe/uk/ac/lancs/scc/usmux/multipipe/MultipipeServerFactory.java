/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.multipipe;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.ac.lancs.scc.jardeps.Service;
import uk.ac.lancs.scc.usmux.SessionServer;
import uk.ac.lancs.scc.usmux.SessionServerException;
import uk.ac.lancs.scc.usmux.SessionServerFactory;

/**
 * Creates servers based on a pair of control pipes, plus a pair of
 * pipes per session. The pattern for the configuration string is
 * 
 * <samp>{@value #PATTERN}</samp>,
 * 
 * whose first field identifies the upstream control pipe, and whose
 * second field identifies the downstream control pipe. The server
 * initially sends a single byte of any value on the downstream control
 * pipe to signal that it is ready to receive sessions. It closes the
 * downstream control pipe to signal that it will no longer receive
 * sessions.
 * 
 * <p>
 * For each session, the daemon will print two lines into upstream
 * control pipe, giving the names of two pipes created for the session,
 * the first being upstream from daemon to server. This class opens
 * these two files, then deletes them.
 * 
 * <p>
 * The upstream data pipe begins with two bytes for the length of the
 * session's meta-data, followed by that meta-data. Everything after
 * that is the upstream content. The downstream data pipe is used
 * exclusively for downstream content.
 * 
 * @author simpsons
 */
@Service(SessionServerFactory.class)
public final class MultipipeServerFactory extends SessionServerFactory {
    /**
     * @undocumented
     */
    public static final String PATTERN = "^multipipe:([^;]+);([^;]+)$";

    private static final Pattern configPattern = Pattern.compile(PATTERN);

    @Override
    public SessionServer createServer(String config, Properties props)
	    throws SessionServerException {
	Matcher matcher = configPattern.matcher(config);
	if (!matcher.matches()) return null;
	try {
	    File upstreamControlFile =
		new File(URLDecoder.decode(matcher.group(1), "UTF-8"));
	    File downstreamControlFile =
		new File(URLDecoder.decode(matcher.group(2), "UTF-8"));
	    MultipipeStreamServer result =
		new MultipipeStreamServer(upstreamControlFile,
					  downstreamControlFile);
	    return result;
	} catch (UnsupportedEncodingException ex) {
	    throw new AssertionError("unreachable");
	} catch (IOException ex) {
	    throw new SessionServerException("on open", ex);
	}
    }
}
