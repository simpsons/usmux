/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

/**
 * Identifies different types of messages in the multiplexing protocol.
 * 
 * <p>
 * The multiplexing protocol allows several ‘sessions’ to simultaneously
 * share a single physical connection. The peers of the connection are
 * referred to herein as the daemon and the server.
 * 
 * <p>
 * Each session has two streams. One flows upstream from daemon to
 * server, while the other flows downstream from server to daemon. Each
 * session consists of a series of messages, some travelling upstream
 * and some downstream. The session also begins with some meta-data of
 * arbitrary size, sent upstream.
 * 
 * <p>
 * Each session has two identifiers, one used by the daemon to identify
 * it, and one used by the server. Identifiers are 16-bit quantities,
 * and are exchanged at the start of each session.
 * 
 * <p>
 * It is assumed that each peer has a transmission buffer and a
 * reception buffer. The protocol imposes requirements on the sender of
 * stream data to ensure that no reception buffer overflows, and that no
 * session needs to be blocked because another session is temporarily
 * dormant.
 * 
 * <p>
 * Each message begins with a 2-byte message type, followed by a 2-byte
 * payload length (excluding the four header bytes). The payload format
 * depends on the message type.
 * 
 * <p>
 * The protocol begins with the server sending a RDY message, which may
 * include a 2-byte payload specifying the maximum size of meta-data
 * that a new session can send.
 * 
 * <p>
 * The daemon starts a new session by sending a NEW message, with two
 * bytes giving the session id, and any remaining payload being the
 * session's meta-data (upto the limit specified by the RDY message).
 * The server responds with ACK, giving the daemon's session id,
 * followed by its own. All subsequent messages pertaining to this
 * session have a payload prefixed with the server's id if it comes from
 * the daemon, or with the daemon's id if it comes from the server.
 * 
 * <p>
 * After ACK is transferred, each side can send a CTS message, which
 * includes a 4-byte length field after the session id. Each peer can
 * accumulate these received lengths, and must not send stream data
 * exceeding their sum. When a peer receives stream data, it can send
 * CTS with upto the received length, or remember it for later. CTS
 * allows the receiver of a stream to ensure that no data is transmitted
 * to it while its receiving buffer is full.
 * 
 * <p>
 * To send stream data for a session, the sender sends a STR message.
 * The payload begins with the receiver's session id, and concludes with
 * the stream data itself. The payload length is therefore two less than
 * the stream data length. The entire message content must be available
 * at the sender when it starts sending the message. The sender must not
 * send a total of stream data that exceeds the sum of lengths of
 * received CTS messages for session.
 * 
 * <p>
 * To indicate that the stream is exhausted, the sender must send EOS.
 * 
 * <p>
 * To indicate that a stream's data is no longer necessary, the receiver
 * of the stream can send STP. It may then discard any further stream
 * data for the session. The sender may use this to avoid transmitting
 * any further stream data unnecessarily.
 * 
 * @author simpsons
 */
public enum MessageType {
    /**
     * Identifies a message of unknown type. No message is to be sent
     * with this type. No peer may send this.
     */
    UNK(0),

    /**
     * Identifies a message with no purpose. No peer may send this.
     */
    NOP(1),

    /**
     * Identifies a RDY (ready) message, indicating that the server is
     * ready.
     */
    RDY(2),

    /**
     * Identifies a NEW (new session) message, indicating that the
     * client is attempting to open a new session, and providing
     * meta-data.
     */
    NEW(3),

    /**
     * Identifies an ACK (session acknowledge) message, indicating that
     * the server accepts the client's session.
     */
    ACK(4),

    /**
     * Identifies a CTS (clear to send) message, specifying how many
     * additional bytes the peer may send on a given session.
     */
    CTS(5),

    /**
     * Identifies a STR (stream data) message, specifying a session. The
     * payload is the stream data.
     */
    STR(6),

    /**
     * Identifies an EOS (end of file) message, identifying a session
     * that will be sending no more data.
     */
    EOS(7),

    /**
     * Identifies an STP (stop) message, identifying a session that need
     * not send more data.
     */
    STP(8);

    MessageType(int code) {
	this.code = (short) code;
    }

    private final short code;

    /**
     * Get the encoded value of this message type.
     * 
     * @return the encoded value of the message type
     */
    public short encode() {
	return code;
    }

    /**
     * Decode a message type.
     * 
     * @param value the encoded value of the message type
     * 
     * @return the message type, defaulting to {@link #UNK} if not
     * recognized
     */
    public static MessageType decode(int value) {
	for (MessageType r : values())
	    if (r.encode() == value) return r;
	return MessageType.UNK;
    }
}
