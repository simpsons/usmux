/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles traffic from the client upstream to the server.
 * 
 * @author simpsons
 */
final class UpstreamAgent extends InputStream implements StreamSink {
    private final StreamControl control;

    /**
     * Create an upstream agent.
     * 
     * @param control a means to control the incoming data
     */
    public UpstreamAgent(StreamControl control) {
	this.control = control;
	control.clear(data.remaining());

	/* Set the buffer for reading. */
	data.flip();
	assert data.remaining() == 0;
    }

    private ByteBuffer data = ByteBuffer.allocate(1024);

    /* Have we received EOS from the peer? */
    private boolean eosIn;
    private IOException error;

    /* Have we received close() from the user? */
    private boolean closed;

    @Override
    public synchronized int read(InputStream in, int max) throws IOException {
	if (eosIn) throw new IllegalStateException("EOS received");

	/*
	 * Make the buffer writable, read data directly from the stream
	 * into it, and make it readable again.
	 */
	int got;
	Utils.slide(data);
	try {
	    if (max > data.remaining())
		throw new IllegalArgumentException("offered " + max
		    + " out of bounds available " + data.remaining());
	    got =
		in.read(data.array(), data.position() + data.arrayOffset(),
			max);
	    if (got < 0) return got;
	    assert got == max;
	    data.position(data.position() + got);

	} finally {
	    data.flip();
	}

	if (logger.isLoggable(Level.FINEST)) {
	    StringWriter sw = new StringWriter();
	    PrintWriter out = new PrintWriter(sw);
	    try {
		out.printf("Last %d bytes in buffer%n", max);
		Utils.print(out, data, data.limit() - max, max);
	    } finally {
		out.flush();
		logger.finest(sw.toString());
	    }
	}

	notify();

	return got;
    }

    @Override
    public synchronized void consume(ByteBuffer from, int max) {
	if (eosIn) throw new IllegalStateException("EOS received");

	/*
	 * Make the buffer writable, move data into it, and make it
	 * readable again.
	 */
	int skip = 0;
	Utils.slide(data);
	try {
	    if (max > data.remaining()) {
		skip = max - data.remaining();
		max = data.remaining();
	    }
	    if (logger.isLoggable(Level.FINEST)) {
		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);
		try {
		    out.printf("Transferred payload bytes: %d%n", max);
		    Utils.print(out, from, from.position(), max);
		} finally {
		    out.flush();
		    logger.finest(sw.toString());
		}
	    }
	    int done = Utils.transfer(data, from, max);
	    assert done == max;
	} finally {
	    data.flip();
	}

	if (logger.isLoggable(Level.FINEST)) {
	    StringWriter sw = new StringWriter();
	    PrintWriter out = new PrintWriter(sw);
	    try {
		out.printf("Last %d bytes in buffer%n", max);
		Utils.print(out, data, data.limit() - max, max);
	    } finally {
		out.flush();
		logger.finest(sw.toString());
	    }
	}

	notify();

	/* Discard any remaining data. */
	if (skip > 0) {
	    logger.warning("Skipped excess bytes: " + skip);
	    assert skip <= from.remaining();
	    from.position(from.position() + skip);
	}
    }

    @Override
    public synchronized void terminate(IOException ex) {
	if (eosIn) return;
	eosIn = true;
	if (error == null) error = ex;
	notify();
    }

    /**
     * Wait until we have some data for the user, or we know we will get
     * no more, or we know the user doesn't really want any more.
     */
    private void waitForMore() throws EOFException {
	while (!closed && !eosIn && data.remaining() == 0) {
	    try {
		wait();
	    } catch (InterruptedException ex) {
		/* Try again. */
	    }
	}
	if (closed) throw new EOFException("stream closed");
    }

    @Override
    public int read() throws IOException {
	int rc;
	synchronized (this) {
	    waitForMore();
	    if (data.remaining() == 0 && eosIn) {
		if (error != null) throw new IOException(error);
		return -1;
	    }
	    rc = data.get() & 0xff;
	}
	control.clear(1);
	return rc;
    }

    @Override
    public int read(byte[] buf, int offset, int len) throws IOException {
	if (len < 1)
	    throw new IllegalArgumentException("non-positive length");
	synchronized (this) {
	    waitForMore();
	    if (data.remaining() == 0 && eosIn) {
		if (error != null) throw new IOException(error);
		return -1;
	    }
	    if (len > data.remaining()) len = data.remaining();
	    data.get(buf, offset, len);
	}
	control.clear(len);
	return len;
    }

    @Override
    public synchronized void close() throws IOException {
	if (closed) return;
	closed = true;
	if (!eosIn) control.stop();
    }

    private static final Logger logger = Logger
	.getLogger("uk.ac.lancs.scc.usmux.stream_pair_mux.upstream");
}
