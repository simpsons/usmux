/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.Buffer;
import java.nio.ByteBuffer;

final class Utils {
    private Utils() {}

    /**
     * Write the indicated portion of a buffer to a stream.
     * 
     * @param out the destination stream
     * 
     * @param buf the source buffer
     * 
     * @return the number of bytes written
     * 
     * @throws IOException if an I/O error occurs
     */
    static int write(OutputStream out, ByteBuffer buf) throws IOException {
	return Utils.write(out, buf, -1);
    }

    /**
     * Write the indicated portion of a buffer to a stream.
     * 
     * @param out the destination stream
     * 
     * @param buf the source buffer
     * 
     * @param amount the maximum number of bytes to write
     * 
     * @return the number of bytes written
     * 
     * @throws IOException if an I/O error occurs
     */
    static int write(OutputStream out, ByteBuffer buf, int amount)
	    throws IOException {
	if (amount < 0 || amount > buf.remaining()) amount = buf.remaining();
	out.write(buf.array(), buf.position() + buf.arrayOffset(), amount);
	buf.position(buf.position() + amount);
	return amount;
    }

    /**
     * Slide the contents of the buffer to its start, and set the buffer
     * to point at the remaining space. The buffer's position is set to
     * the amount of data moved. Its limit is set the capacity.
     * 
     * <p>
     * The buffer is effectively left in a state ready for writing, and
     * this method is provided as a dual of {@link Buffer#flip()}.
     * 
     * @param buf the buffer to modify
     * 
     * @return the supplied buffer
     */
    static <T extends Buffer> T slide(T buf) {
	System.arraycopy(buf.array(), buf.position() + buf.arrayOffset(),
			 buf.array(), buf.arrayOffset(), buf.remaining());
	buf.position(buf.remaining());
	buf.limit(buf.capacity());
	return buf;
    }

    /**
     * Move a limited number of bytes from one buffer to another.
     * 
     * @param to the destination buffer
     * 
     * @param from the source buffer
     * 
     * @param max the maximum number of bytes to send, or -1 if
     * unlimited
     * 
     * @return the number of bytes transferred
     */
    static <T extends Buffer> int transfer(T to, T from, int max) {
	if (max < 0 || max > to.remaining()) max = to.remaining();
	if (max > from.remaining()) max = from.remaining();
	System.arraycopy(from.array(), from.position() + from.arrayOffset(),
			 to.array(), to.position() + to.arrayOffset(), max);
	to.position(to.position() + max);
	from.position(from.position() + max);
	return max;
    }

    /**
     * Move as many bytes as possible from one buffer to another.
     * 
     * @param to the destination buffer
     * 
     * @param from the source buffer
     * 
     * @return the number of bytes transferred
     */
    static <T extends Buffer> int transfer(T to, T from) {
	return transfer(to, from, -1);
    }

    static void print(PrintWriter out, ByteBuffer buf, int start, int len) {
	final int end = start + len;
	for (int j = start; j < end; j += 16) {
	    final int lim = Math.min(j + 16, end);
	    for (int i = 0; i < 16; i++) {
		final int p = i + j;
		if (p < lim) {
		    int val = buf.get(p);
		    out.printf("%02X ", val & 0xff);
		} else {
		    out.printf(".. ");
		}
	    }
	    for (int i = 0; i < 16; i++) {
		final int p = i + j;
		if (p < lim) {
		    int val = buf.get(p);
		    if (Character.isISOControl(val))
			out.print('.');
		    else
			out.print((char) val);
		} else {
		    out.print(' ');
		}
	    }
	    out.println();
	}
    }
}
