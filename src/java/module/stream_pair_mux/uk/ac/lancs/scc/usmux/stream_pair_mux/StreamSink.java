/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

interface StreamSink {
    /**
     * Consume bytes from a given buffer. There should always be enough
     * space in the internal buffer to receive this, or else someone is
     * not using the protocol properly. Excess data will be discarded.
     * 
     * @param from the source buffer
     * 
     * @param max the maximum number of bytes to consume
     */
    void consume(ByteBuffer from, int max);

    /**
     * Consume bytes from a stream. There should always be enough space
     * in the internal buffer to receive this, or else someone is not
     * using the protocol properly. Excess data will be discarded.
     * 
     * @param in the source stream
     * 
     * @param max the maximum number of bytes to consume
     * 
     * @return the number of bytes read, or -1 if EOF is encountered
     */
    int read(InputStream in, int max) throws IOException;

    /**
     * Signal the end of the stream.
     * 
     * @param ex a reason for terminating the stream, or null if EOS
     */
    void terminate(IOException ex);
}
