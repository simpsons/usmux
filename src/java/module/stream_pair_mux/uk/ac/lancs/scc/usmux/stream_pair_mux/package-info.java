/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Supports Usmux sessions over a single pair of
 * streams.  This uses a multiplexing protocol (described under
 *
 * {@link uk.ac.lancs.scc.usmux.stream_pair_mux.MessageType})
 *
 * that ensures no party is blocked due to full buffers, i.e., no data
 * is transmitted unless the receiver has space for it.  Because this
 * protocol allows meta-data about the client to be transmitted as
 * opaque bytes, it uses the
 *
 * {@link uk.ac.lancs.scc.usmux.MetadataHandler}
 *
 * service to resolve these binary forms into Java structures.
 * Therefore, sessions supported by this protocol can have
 *
 * {@linkplain uk.ac.lancs.scc.usmux.Session#getAttributes(java.lang.Class) attributes}
 * 
 * of any type.
 * 
 * <p>
 * This package provides
 * 
 * {@link uk.ac.lancs.scc.usmux.stream_pair_mux.FilePairServerFactory}
 * 
 * as a service implementing
 * 
 * {@link uk.ac.lancs.scc.usmux.SessionServerFactory}.
 * 
 * @library usmux_server.jar
 */
package uk.ac.lancs.scc.usmux.stream_pair_mux;

