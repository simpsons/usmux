/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.ac.lancs.scc.usmux.MetadataBinding;
import uk.ac.lancs.scc.usmux.MetadataHandler;
import uk.ac.lancs.scc.usmux.Session;
import uk.ac.lancs.scc.usmux.SessionException;
import uk.ac.lancs.scc.usmux.SessionServer;

/**
 * Serves sessions multiplexed over a single connection. This class also
 * uses services implementing {@link MetadataHandler} to convert
 * meta-data bytes in the tail of a NEW message into structures
 * describing the domain-specific aspects of the session.
 * 
 * @author simpsons
 */
public class MultiplexedStreamServer implements SessionServer {
    /**
     * Provides the server with streams on demand. This allows the
     * server's own threads to execute the code that opens a file, which
     * might be significant when that file is a named pipe. Opening of
     * such a pipe could itself block until a peer has opened the other
     * end of the pipe.
     * 
     * @author simpsons
     */
    public interface StreamProvider {
	/**
	 * Open the input stream. This must be called only once.
	 * 
	 * @return the input stream
	 * 
	 * @throws IOException if an I/O error occurs
	 */
	InputStream openInput() throws IOException;

	/**
	 * Open the output stream. This must be called only once.
	 * 
	 * @return the output stream
	 * 
	 * @throws IOException if an I/O error occurs
	 */
	OutputStream openOutput() throws IOException;
    }

    private final StreamProvider streams;
    private InputStream in;
    private DataOutputStream out;

    /**
     * Holds incoming data. This buffer should normally be ready for
     * reading from.
     */
    private final ByteBuffer inBuf = ByteBuffer.allocate(1024);
    {
	inBuf.flip();
    }

    /**
     * The number of bytes the peer can send to us as meta data is our
     * input buffer size minus two bytes for the message type, two for
     * the length, and two for the peer sid.
     */
    private final int metaLimit = inBuf.capacity() - 6;

    /**
     * Maps our (server) session ids to downstream agents. Items added
     * by {@link #readAction()} when {@link MessageType#NEW} message
     * received.
     * 
     * Items removed by
     * 
     * {@link #writeAction()} when a {@link Transmitter} decides it's
     * not going to send any more messages other than
     * 
     * {@link MessageType#CTS}.
     * 
     * Read by {@link #readAction()} to get a free session id, or to
     * resolve an id of an incoming message.
     */
    private final Map<Integer, Transmitter> downstreamTransmitters =
	new ConcurrentHashMap<Integer, Transmitter>();

    /**
     * Maps our (server) session ids to upstream agents. Items added by
     * {@link #readAction()} when {@link MessageType#NEW} message
     * received. Items removed by {@link #readAction()} when
     * {@link MessageType#EOS} received.
     */
    private final Map<Integer, StreamSink> upstreamSinks =
	new ConcurrentHashMap<Integer, StreamSink>();

    /**
     * Holds downstream agents with data to transmit.
     */
    private final BlockingQueue<Transmitter> writeQueue =
	new LinkedBlockingQueue<Transmitter>();

    /**
     * Holds sessions yet to be exposed to users. The {@link #accept()}
     * call removes items from this queue. {@link #readAction()} adds to
     * it.
     */
    private final BlockingQueue<AgentPair> acceptQueue =
	new LinkedBlockingQueue<AgentPair>();

    /**
     * This thread simply calls {@link #readActivity()} once.
     */
    private final Thread readThread = new Thread() {
	@Override
	public void run() {
	    readActivity();
	}
    };

    /**
     * This thread simply calls {@link #writeActivity()} once.
     */
    private final Thread writeThread = new Thread() {
	@Override
	public void run() {
	    writeActivity();
	}
    };

    /**
     * Create a server from a given pair of streams.
     * 
     * @param in the input stream
     * 
     * @param out the output stream
     */
    public MultiplexedStreamServer(InputStream in, OutputStream out) {
	this(of(in, out));
    }

    /**
     * Create a server from a given pair of files.
     * 
     * @param in the input file
     * 
     * @param out the output file
     */
    public MultiplexedStreamServer(File in, File out) {
	this(of(in, out));
    }

    /**
     * Create a stream provider for existing streams.
     * 
     * @param in the input stream to be provided later
     * 
     * @param out the output stream to be provided later
     * 
     * @return the provider of the supplied streams
     */
    public static StreamProvider of(final InputStream in,
				    final OutputStream out) {
	return new StreamProvider() {
	    public InputStream openInput() {
		return in;
	    }

	    public OutputStream openOutput() {
		return out;
	    }
	};
    }

    /**
     * Create a stream provider for files.
     * 
     * @param in the file to be opened for input later
     * 
     * @param out the file to be opened for output later
     * 
     * @return the provider of the supplied streams
     */
    public static StreamProvider of(final File in, final File out) {
	return new StreamProvider() {
	    public InputStream openInput() throws FileNotFoundException {
		return new FileInputStream(in);
	    }

	    public OutputStream openOutput() throws FileNotFoundException {
		return new FileOutputStream(out);
	    }
	};
    }

    /**
     * Create a server from a stream provider.
     * 
     * @param streams the object optionally opening and providing
     * streams
     */
    public MultiplexedStreamServer(StreamProvider streams) {
	this.streams = streams;
    }

    /**
     * Set if the user of the server has closed it.
     */
    private volatile boolean closed;

    /**
     * Set if the upstream pipe has been closed by its peer.
     */
    private volatile boolean eosIn;

    /**
     * Records the fatal exception that terminated reading of the
     * upstream pipe.
     */
    private volatile IOException readFailure;

    /**
     * Records the fatal exception that terminated writing of the
     * downstream pipe.
     */
    private volatile IOException writeFailure;

    @Override
    public void close() throws SessionException {
	if (closed) return;
	closed = true;
	interruptUsers();
	readThread.interrupt();
	writeThread.interrupt();
    }

    /**
     * Keeps track of threads waiting for a session (i.e. blocked in an
     * {@link #accept()} call). Call {@link #interruptUsers()} to have
     * them all interrupted.
     */
    private final Collection<Thread> users = Collections
	.newSetFromMap(new ConcurrentHashMap<Thread, Boolean>());

    /**
     * Interrupt all threads blocked on an {@link #accept()} call.
     */
    private void interruptUsers() {
	for (Thread t : users)
	    t.interrupt();
    }

    /**
     * {@inheritDoc} The serialized meta-data of each new session is
     * passed to services implementing {@link MetadataHandler}, to
     * deserialize them into domain-specific structures describing the
     * session.
     */
    @Override
    public Session accept() throws SessionException {
	/*
	 * Record each thread waiting for a session, and then discard it
	 * afterwards.
	 */
	users.add(Thread.currentThread());
	try {
	    /*
	     * Keep trying to get a new session, until we know there can
	     * be no more.
	     */
	    while (!closed && !eosIn && readFailure == null
		&& writeFailure == null) {
		try {
		    return acceptQueue.take();
		} catch (InterruptedException ex) {
		    /* Try again. */
		}
	    }
	} finally {
	    users.remove(Thread.currentThread());
	}
	close();
	if (readFailure != null) throw new SessionException(readFailure);
	if (writeFailure != null) throw new SessionException(writeFailure);
	return null;
    }

    /**
     * Start reading and writing messages.
     */
    public void start() {
	readThread.start();
	writeThread.start();
    }

    /* Elements used privately by the read action */
    private int payload;
    private MessageType messageType = MessageType.UNK;
    private StreamSink currentSink;

    private void readAction() throws IOException {
	if (currentSink != null) {
	    /*
	     * We're in the middle of a STR message to one of our
	     * upstream agents. We should have no data in our own
	     * buffer.
	     */
	    assert payload > 0;
	    assert messageType == MessageType.STR;
	    assert inBuf.remaining() == 0;

	    if (eosIn) return;

	    /*
	     * Read directly into the agent's buffer, upto the limit of
	     * that buffer and the remaining payload.
	     */
	    upLogger.finest("Attempting read of " + payload + " bytes for "
		+ messageType + " to current sink");
	    int got = currentSink.read(in, payload);
	    if (got < 0) {
		eosIn = true;
		interruptUsers();
		return;
	    }
	    upLogger.finer("Read " + got + " bytes directly");
	    payload -= got;

	    /*
	     * If we've read the entire STR message, reset back to
	     * unknown.
	     */
	    if (payload == 0) {
		messageType = MessageType.UNK;
		currentSink = null;
	    } else {
		return;
	    }
	}

	/*
	 * Temporarily make the buffer suitable for writing into from
	 * the input stream.
	 */
	Utils.slide(inBuf);
	try {
	    if (eosIn) return;
	    upLogger.finest("Attempting read of " + inBuf.remaining()
		+ " bytes for " + messageType);
	    assert inBuf.remaining() > 0;

	    int got =
		in.read(inBuf.array(),
			inBuf.position() + inBuf.arrayOffset(),
			inBuf.remaining());
	    if (got < 0) {
		eosIn = true;
		interruptUsers();
		return;
	    }
	    if (upLogger.isLoggable(Level.FINER)) {
		StringWriter sw = new StringWriter();
		PrintWriter out = new PrintWriter(sw);
		try {
		    out.printf("Read %d bytes:%n", got);
		    Utils.print(out, inBuf, inBuf.position(), got);
		} finally {
		    out.flush();
		    upLogger.finer(sw.toString());
		}
	    }
	    inBuf.position(inBuf.position() + got);
	} finally {
	    inBuf.flip();
	}

	/*
	 * Consume as many bytes as possible, and interpret them as
	 * messages.
	 */
	next_message: for (;;) {
	    if (messageType == MessageType.UNK) {
		/*
		 * Go round again if we have insufficient bytes for a
		 * message header.
		 */
		if (inBuf.remaining() < 4) break;

		/*
		 * We have enough in the buffer to read the message type
		 * and payload length.
		 */
		assert inBuf.remaining() >= 4;
		messageType = MessageType.decode(inBuf.getShort() & 0xffff);
		payload = inBuf.getShort() & 0xffff;
		upLogger.finer("New message is " + messageType
		    + " with payload " + payload);
	    } else {
		upLogger.finer("Current message is " + messageType
		    + " with payload " + payload);
	    }

	    upLogger.finer("Buffer bytes: " + inBuf.remaining());
	    switch (messageType) {
	    case NEW:
		if (inBuf.remaining() + payload < 2) {
		    /*
		     * We'll never have enough to get the our sid.
		     */
		    messageType = MessageType.NOP;
		    continue;
		}

		if (inBuf.remaining() < payload) {
		    /* We haven't got the full message yet. */
		    break next_message;
		}
		/* We've got the full message. */

		{
		    /* Consume the client's sid. */
		    int clientSid = inBuf.getShort() & 0xffff;
		    payload -= 2;
		    upLogger.fine("Received NEW(" + clientSid + ")+"
			+ payload);

		    /*
		     * Create agents for the new session, and index
		     * them.
		     */
		    int ourSid = nextFreeSid();
		    assert out != null;
		    DownstreamAgent downstreamAgent =
			new DownstreamAgent(ourSid, clientSid, out,
					    writeQueue);
		    downstreamTransmitters.put(ourSid, downstreamAgent);
		    UpstreamAgent upstreamAgent =
			new UpstreamAgent(
					  downstreamAgent
					      .getUpstreamControl());
		    upstreamSinks.put(ourSid, upstreamAgent);

		    /*
		     * Pair them together to be accepted as a duplex
		     * session, and consume the remaining payload as
		     * meta-data.
		     */
		    AgentPair sess =
			new AgentPair(upstreamAgent,
				      proxyFor(downstreamAgent), inBuf,
				      payload);
		    payload = 0;
		    acceptQueue.add(sess);

		    /* We don't need any more. */
		    messageType = MessageType.UNK;
		    continue;
		}

	    case CTS:
		if (inBuf.remaining() + payload < 6) {
		    /*
		     * We'll never have enough to get the our sid and
		     * the clearance.
		     */
		    messageType = MessageType.NOP;
		    continue;
		}
		if (inBuf.remaining() < 6) {
		    /* We don't have enough to get our sid and quota. */
		    break next_message;
		}
		/* We've got the full message. */

		{
		    /* Read the our sid and the quota. */
		    int ourSid = inBuf.getShort() & 0xffff;
		    int quota = inBuf.getInt() & 0x7fffffff;
		    payload -= 6;
		    upLogger.fine("Received CTS(" + ourSid + ", " + quota
			+ ")");

		    /* We don't need any more. */
		    messageType = MessageType.NOP;

		    /* Find the controller for the downward stream. */
		    StreamControl ctrl = downstreamTransmitters.get(ourSid);
		    if (ctrl != null) ctrl.clear(quota);
		    continue;
		}

	    case STP:
		if (inBuf.remaining() + payload < 2) {
		    /* We'll never have enough to get our sid. */
		    messageType = MessageType.NOP;
		    continue;
		}

		if (inBuf.remaining() < 2) {
		    /* We don't have enough to get our sid. */
		    break next_message;
		}
		/* We've got the full message. */

		{
		    /* Read our sid. */
		    int ourSid = inBuf.getShort() & 0xffff;
		    upLogger.fine("Received STP(" + ourSid + ")");
		    payload -= 2;

		    /* We don't need any more. */
		    messageType = MessageType.NOP;

		    /* Find the controller for the downward stream. */
		    StreamControl ctrl = downstreamTransmitters.get(ourSid);
		    if (ctrl != null) ctrl.stop();
		    continue;
		}

	    case EOS:
		if (inBuf.remaining() + payload < 2) {
		    /* We'll never have enough to get our sid. */
		    messageType = MessageType.NOP;
		    continue;
		}
		if (inBuf.remaining() < 2) {
		    /* We don't have enough to get our sid. */
		    break next_message;
		}
		/* We've got the full message. */

		{
		    /* Read our sid. */
		    int ourSid = inBuf.getShort() & 0xffff;
		    upLogger.fine("Received EOS(" + ourSid + ")");
		    payload -= 2;

		    /* We don't need any more. */
		    messageType = MessageType.NOP;

		    /* Find the sink for the upward stream. */
		    StreamSink sink = upstreamSinks.remove(ourSid);
		    if (sink != null) sink.terminate(readFailure);
		    upLogger.finer("Remaining upstream sinks: "
			+ upstreamSinks.keySet().size());
		    continue;
		}

	    case STR:
		assert currentSink == null;
		if (inBuf.remaining() + payload < 2) {
		    /* We'll never have enough to get our sid. */
		    messageType = MessageType.NOP;
		    upLogger.warning("STR without sink; payload=" + payload);
		    continue;
		}

		if (inBuf.remaining() < 2) {
		    /* We don't have enough to get our sid. */
		    break next_message;
		}
		/* We've got the full header. */

		{
		    /* Read our sid. */
		    int ourSid = inBuf.getShort() & 0xffff;
		    payload -= 2;
		    upLogger.fine("Received STR(" + ourSid + ", " + payload
			+ ")");

		    /* Find the sink for the upward stream. */
		    currentSink = upstreamSinks.get(ourSid);
		    if (currentSink == null) {
			/* Discard the rest of this message. */
			messageType = MessageType.NOP;
			continue;
		    }

		    if (payload > 0) {
			/* Submit at most 'payload' bytes to the sink. */
			int amount = Math.min(inBuf.remaining(), payload);
			currentSink.consume(inBuf, amount);
			payload -= amount;
		    }

		    if (payload == 0) {
			messageType = MessageType.UNK;
			currentSink = null;
		    }
		    break next_message;
		}

	    default:
		messageType = MessageType.NOP;
		/* Fall through. */
	    case NOP:
		/* Discard the data, up to the remaining payload. */
		int amount = Math.min(inBuf.remaining(), payload);
		inBuf.position(inBuf.position() + amount);
		payload -= amount;
		assert payload >= 0;
		if (payload == 0) messageType = MessageType.UNK;
		continue;
	    }
	}
    }

    private void readActivity() {
	try {
	    in = streams.openInput();
	    synchronized (this) {
		while (out == null)
		    try {
			wait();
		    } catch (InterruptedException ex) {
			/*
			 * Just keep trying. TODO: Work out what error
			 * conditions would cause us to abort.
			 */
		    }
	    }
	    while (!closed && !eosIn)
		readAction();
	} catch (IOException ex) {
	    readFailure = ex;
	}
	interruptUsers();
	try {
	    in.close();
	} catch (IOException ex) {
	    upLogger.log(Level.WARNING, "Closing input", ex);
	}
    }

    private void writeAction() throws IOException, InterruptedException {
	Transmitter xmit = writeQueue.take();
	if (xmit.transmit()) {
	    downstreamTransmitters.remove(xmit.getSid());
	    downLogger.finer("Remaining downstream transmitters: "
		+ downstreamTransmitters.keySet().size());
	}
    }

    private void writeActivity() {
	try {
	    OutputStream cand = streams.openOutput();
	    synchronized (this) {
		out =
		    cand instanceof DataOutputStream
			? (DataOutputStream) cand
			: new DataOutputStream(cand);
		/*
		 * The reading thread must wait for the output stream to
		 * open.
		 */
		notifyAll();
	    }

	    /* Write out the initial RDY message. */
	    out.writeShort(MessageType.RDY.encode());
	    out.writeShort(2);
	    out.writeShort((short) metaLimit);
	    out.flush();
	    downLogger.fine("Sent RDY");

	    /* Now write out ACKs and user messages. */
	    while (!closed)
		try {
		    writeAction();
		} catch (InterruptedException ex) {
		    /* Go round again. */
		}
	} catch (IOException ex) {
	    writeFailure = ex;
	}
	interruptUsers();
	try {
	    out.close();
	} catch (IOException ex) {
	    downLogger.log(Level.WARNING, "Closing output", ex);
	}
    }

    private int nextSid;

    private int nextFreeSid() {
	assert Thread.currentThread() == readThread;
	for (;;) {
	    int result = nextSid++;
	    if (!upstreamSinks.containsKey(result)
		&& !downstreamTransmitters.containsKey(result))
		return result;
	}
    }

    /**
     * Combines upstream and downstream agents, and parses meta-data.
     * This is the type of objects exposed to users as sessions.
     * 
     * @author simpsons
     */
    private static class AgentPair implements Session {
	private final OutputStream out;
	private final InputStream in;
	private final MetadataBinding binding;

	public AgentPair(InputStream in, OutputStream out, ByteBuffer meta,
			 int metaLen) {
	    this.out = out;
	    this.in = in;
	    byte[] rawMeta = new byte[metaLen];
	    assert meta.remaining() >= metaLen;
	    meta.get(rawMeta);

	    ServiceLoader<MetadataHandler> service =
		ServiceLoader.load(MetadataHandler.class);
	    MetadataBinding binding = null;
	    for (MetadataHandler handler : service) {
		binding = handler.convert(rawMeta);
		if (binding != null) break;
	    }
	    this.binding = binding;
	}

	@Override
	public void close() throws IOException {
	    try {
		out.close();
	    } finally {
		in.close();
	    }
	}

	@Override
	public InputStream getInputStream() {
	    return in;
	}

	@Override
	public OutputStream getOutputStream() {
	    return out;
	}

	@Override
	public <T> T getAttributes(Class<T> type) {
	    if (binding.types.contains(type)) return type.cast(binding.data);
	    return null;
	}
    }

    /**
     * This object is {@link Runnable}, and when invoked, it closes the
     * supplied agent.
     * 
     * @summary A weak reference for a downstream agent
     * 
     * @author simpsons
     */
    private class StreamReference extends WeakReference<Object>
	    implements Runnable {
	private final DownstreamAgent orig;

	public StreamReference(OutputStream base, DownstreamAgent orig) {
	    super(base, refQueue);
	    this.orig = orig;
	}

	@Override
	public void run() {
	    try {
		downLogger.finest("Auto-closing " + orig.getSid());
		orig.close();
	    } catch (IOException ex) {
		// Don't care.
	    }
	}
    }

    /**
     * Records objects that must be properly discarded when they become
     * inaccessible. References added to this queue must implement
     * {@link Runnable}, and are executed when removed from the queue.
     */
    private static final ReferenceQueue<Object> refQueue =
	new ReferenceQueue<Object>();

    /**
     * Keeps processing discarded sessions as they become unreachable.
     * Stop when there can be no more additions to the queue.
     */
    private static Thread referenceThread = new Thread() {
	@Override
	public void run() {
	    for (;;) {
		try {
		    Runnable action = (Runnable) refQueue.remove();
		    action.run();
		} catch (InterruptedException ex) {
		    ;
		}
	    }
	}
    };

    /**
     * Make the reference thread a daemon so that it doesn't keep the
     * JVM running when everything else has finished.
     */
    static {
	referenceThread.setName("Usmux reaper");
	referenceThread.setDaemon(true);
	referenceThread.start();
    }

    /**
     * Wrap the output stream interface of a downstream agent to be
     * given to the user, so we can detect when the user has discarded
     * it.
     * 
     * @param base the downstream agent which has not been exposed to
     * the user
     * 
     * @return the wrapper for the supplied agent, which should be given
     * to the user
     */
    private OutputStream proxyFor(DownstreamAgent base) {
	OutputStream out = new FilterOutputStream(base);
	new StreamReference(out, base);
	return out;
    }

    @SuppressWarnings("unused")
    private final Logger logger = Logger
	.getLogger("uk.ac.lancs.scc.usmux.stream_pair_mux");

    private final Logger upLogger = Logger
	.getLogger("uk.ac.lancs.scc.usmux.stream_pair_mux.upstream");

    private final Logger downLogger = Logger
	.getLogger("uk.ac.lancs.scc.usmux.stream_pair_mux.downstream");
}
