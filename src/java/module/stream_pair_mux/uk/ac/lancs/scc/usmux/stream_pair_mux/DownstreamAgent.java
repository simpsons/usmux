/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.stream_pair_mux;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.logging.Logger;

/**
 * Handles traffic from the server downstream to the client. The
 * {@link StreamControl} of this class controls the downward stream to
 * the peer.
 * 
 * @author simpsons
 */
final class DownstreamAgent extends OutputStream implements Transmitter {
    private final DataOutputStream out;
    private final Collection<? super Transmitter> queue;
    private final int ownSid, remoteSid;

    /**
     * Create an object.
     * 
     * @param ownSid our own session id
     * 
     * @param remoteSid the remote session id
     * 
     * @param out the destination for messages
     * 
     * @param queue the queue to add this object to when ready to send
     */
    public DownstreamAgent(int ownSid, int remoteSid, OutputStream out,
			   Collection<? super Transmitter> queue) {
	this.out =
	    out instanceof DataOutputStream ? (DataOutputStream) out
		: new DataOutputStream(out);
	this.queue = queue;
	this.ownSid = ownSid;
	this.remoteSid = remoteSid;
	queue.add(this);
    }

    /**
     * Stores the outgoing stream. It is normally to be kept ready for
     * writing.
     */
    private ByteBuffer data = ByteBuffer.allocate(1024);

    /* This is the number of bytes we are cleared to send. */
    private int cleared;

    /* This is the number of bytes we must offer to clear. */
    private int offered;

    /*
     * When the user calls close() on us, we set eosIn. When we send an
     * EOS message, we set eosOut. That records that we won't generate
     * any more messages, other than CTS.
     */
    private boolean eosIn, eosOut;

    /*
     * We might be asked to send an STP message to tell the peer that we
     * don't bother with any more data.
     */
    private boolean stopUpstream;

    /* We might be told not to send any more downstream by the peer. */
    private boolean stopDownstream;

    /* This records an error that we must throw to the user, if set. */
    private IOException error;

    /* This records whether we have sent the ACK message. */
    private boolean acknowledged;

    @Override
    public synchronized void clear(int amount) {
	data.flip();
	try {
	    cleared += amount;
	    if (cleared > 0 && data.remaining() > 0) {
		/* We have something to say. */
		queue.add(this);
	    }
	} finally {
	    Utils.slide(data);
	}
    }

    @Override
    public synchronized void stop() {
	if (stopDownstream) return;
	stopDownstream = true;
	notify();
    }

    private synchronized void clearPeer(int amount) {
	offered += amount;
	if (offered > 0) queue.add(this);
    }

    private synchronized void stopPeer() {
	if (stopUpstream) return;
	stopUpstream = true;
	queue.add(this);
    }

    @Override
    public synchronized void write(byte[] buf, int offset, int len)
	    throws IOException {
	while (len > 0) {
	    if (error != null) throw error;
	    if (eosIn) throw new EOFException();

	    /*
	     * If we've been told to stop sending data, we just consume
	     * it as the user provides it. TODO: Throw some sort of
	     * exception?
	     */
	    if (stopDownstream) return;

	    int amount = data.remaining() < len ? data.remaining() : len;
	    if (amount == 0) {
		try {
		    wait();
		} catch (InterruptedException ex) {
		    if (error != null) error = new InterruptedIOException();
		}
		continue;
	    }
	    data.put(buf, offset, amount);
	    len -= amount;
	    queue.add(this);
	}
    }

    @Override
    public synchronized void write(int arg0) throws IOException {
	for (;;) {
	    if (error != null) throw error;
	    if (eosIn) throw new EOFException();

	    /*
	     * If we've been told to stop sending data, we just consume
	     * it as the user provides it. TODO: Throw some sort of
	     * exception?
	     */
	    if (stopDownstream) return;

	    if (!data.hasRemaining()) {
		try {
		    wait();
		} catch (InterruptedException ex) {
		    if (error != null) error = new InterruptedIOException();
		}
		continue;
	    }
	    data.put((byte) arg0);
	    queue.add(this);
	    break;
	}
    }

    @Override
    public synchronized void close() throws IOException {
	/* The user has signalled that it will send no more data. */
	if (eosIn) return;
	eosIn = true;
	queue.add(this);
    }

    @Override
    public synchronized boolean transmit() throws IOException {
	/*
	 * Make the data buffer ready for reading, and ensure it is
	 * ready for writing by the time we return.
	 */
	data.flip();
	try {
	    /*
	     * Check twice whether we have a message to send. In the
	     * first case, send the message. In the second case, add
	     * ourselves back into the queue.
	     */
	    boolean sent = false;
	    for (;;) {
		/* Check if we've any data to send immediately. */
		if (!acknowledged) {
		    if (sent) {
			queue.add(this);
			return false;
		    }
		    sent = true;

		    /*
		     * Write an ACK message. It must be the first thing
		     * we send for any given session, as it passes our
		     * sid to the daemon.
		     */
		    out.writeShort(MessageType.ACK.encode());
		    out.writeShort((short) 4);
		    out.writeShort((short) remoteSid);
		    out.writeShort((short) ownSid);
		    logger
			.fine("Sent ACK(" + remoteSid + ", " + ownSid + ")");
		    acknowledged = true;
		} else if (offered > 0) {
		    if (sent) {
			queue.add(this);
			return false;
		    }
		    sent = true;

		    int max = offered;
		    out.writeShort(MessageType.CTS.encode());
		    out.writeShort((short) 6);
		    out.writeShort((short) remoteSid);
		    out.writeInt(max);
		    logger.fine("Sent CTS(" + remoteSid + ", " + max + ")");
		    offered -= max;
		} else if (!stopDownstream && data.remaining() > 0
		    && cleared > 0) {
		    if (sent) {
			queue.add(this);
			return false;
		    }
		    sent = true;

		    /*
		     * Work out how much we can send. We can't send more
		     * than we've got, more than we're cleared to send,
		     * or more than the maximum payload in a message
		     * (minus 2 for the session id).
		     */
		    int outgoing = data.remaining();
		    if (outgoing > cleared) outgoing = cleared;
		    if (outgoing > 0xfffd) outgoing = 0xfffd;
		    cleared -= outgoing;

		    /* Write a STR header. */
		    out.writeShort(MessageType.STR.encode());
		    out.writeShort((short) (2 + outgoing));
		    out.writeShort((short) remoteSid);
		    logger.fine("Sent STR(" + remoteSid + ", " + outgoing
			+ ")");

		    /* Write the STR payload. */
		    Utils.write(out, data, outgoing);
		    logger.fine("Sent " + outgoing + " bytes");
		    notify();
		} else if (stopUpstream) {
		    if (sent) {
			queue.add(this);
			return false;
		    }
		    sent = true;

		    /* Write an STP message. */
		    out.writeShort(MessageType.STP.encode());
		    out.writeShort((short) 2);
		    out.writeShort((short) remoteSid);
		    logger.fine("Sent STP(" + remoteSid + ")");
		    stopUpstream = false;
		} else if (!stopDownstream && eosIn && data.remaining() == 0
		    && !eosOut) {
		    if (sent) {
			queue.add(this);
			return false;
		    }
		    sent = true;

		    /* Write an EOS message. */
		    out.writeShort(MessageType.EOS.encode());
		    out.writeShort((short) 2);
		    out.writeShort((short) remoteSid);
		    logger.fine("Sent EOS(" + remoteSid + ")");
		    eosOut = true;
		} else {
		    /*
		     * We've nothing left to send for now. Indicate
		     * whether we will generate anything other than
		     * CTS/STP from now on.
		     */
		    return eosOut || stopDownstream;
		}

		/*
		 * Having just sent something, go round again to see if
		 * there's more.
		 */
	    }
	} finally {
	    Utils.slide(data);
	}
    }

    public StreamControl getUpstreamControl() {
	return upstreamControl;
    }

    private final StreamControl upstreamControl = new StreamControl() {
	@Override
	public void clear(int amount) {
	    clearPeer(amount);
	}

	@Override
	public void stop() {
	    stopPeer();
	}
    };

    @Override
    public synchronized void abort(IOException ex) {
	if (error != null) return;
	error = ex;
	notify();
    }

    private static final Logger logger = Logger
	.getLogger("uk.ac.lancs.scc.usmux.stream_pair_mux.downstream");

    @Override
    public int getSid() {
	return ownSid;
    }
}
