package uk.ac.lancs.scc.usmux;

/**
 * Indicates an error in establishing a session or a session server.
 * 
 * @author simpsons
 */
public class SessionServerException extends SessionException {
    /**
     * Create an exception with no detail message or cause.
     */
    public SessionServerException() {}

    /**
     * Create an exception with a detail message and cause.
     * 
     * @param message the detail message
     * 
     * @param cause the cause
     */
    public SessionServerException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * Create an exception with a detail message but no cause.
     * 
     * @param message the detail message
     */
    public SessionServerException(String message) {
	super(message);
    }

    /**
     * Create an exception with a cause but no detail message.
     * 
     * @param cause the cause
     */
    public SessionServerException(Throwable cause) {
	super(cause);
    }

    private static final long serialVersionUID = 1L;

}
