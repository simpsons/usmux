package uk.ac.lancs.scc.usmux;

/**
 * Indicates an error in establishing a session or a session server.
 * 
 * @author simpsons
 */
public class SessionException extends Exception {
    /**
     * Create an exception with no detail message or cause.
     */
    public SessionException() {}

    /**
     * Create an exception with a detail message and cause.
     * 
     * @param message the detail message
     * 
     * @param cause the cause
     */
    public SessionException(String message, Throwable cause) {
	super(message, cause);
    }

    /**
     * Create an exception with a detail message but no cause.
     * 
     * @param message the detail message
     */
    public SessionException(String message) {
	super(message);
    }

    /**
     * Create an exception with a cause but no detail message.
     * 
     * @param cause the cause
     */
    public SessionException(Throwable cause) {
	super(cause);
    }

    private static final long serialVersionUID = 1L;

}
