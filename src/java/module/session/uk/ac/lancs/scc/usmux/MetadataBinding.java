/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * Binds meta-data to a set of class types.
 * 
 * @author simpsons
 */
public final class MetadataBinding {
    /**
     * Create a binding.
     * 
     * @param data the meta-data
     * 
     * @param types the types to bind to
     * 
     * @return the new binding
     * 
     * @throws IllegalArgumentException if the meta-data is not an
     * instance of all the specified types
     */
    public static MetadataBinding of(Object data, Class<?>... types) {
	return new MetadataBinding(data, Arrays.asList(types));
    }

    /**
     * Create a binding.
     * 
     * @param data the meta-data
     * 
     * @param types the types to bind to
     * 
     * @return the new binding
     * 
     * @throws IllegalArgumentException if the meta-data is not an
     * instance of all the specified types
     */
    public static MetadataBinding of(Object data,
				     Collection<? extends Class<?>> types) {
	return new MetadataBinding(data, types);
    }

    /**
     * The meta-data
     */
    public final Object data;

    /**
     * An unmodifiable collection of types
     */
    public final Collection<Class<?>> types;

    private MetadataBinding(Object data, Collection<? extends Class<?>> types) {
	for (Class<?> type : types)
	    if (type.isInstance(type))
		throw new IllegalArgumentException(data + " is not a " + type);
	this.data = data;
	this.types =
	    Collections
		.unmodifiableCollection(new HashSet<Class<?>>(types));
    }
}
