/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux;

import java.util.Properties;
import java.util.ServiceLoader;

/**
 * Creates servers from a string configuration.
 * 
 * <p>
 * Configuration strings should begin with a scheme and a colon.
 * Everything after the colon is specific to the scheme. Any special
 * characters should be percent-encoded (as if by
 * 
 * {@link java.net.URLEncoder#encode(String,String)}.
 * 
 * @author simpsons
 */
public abstract class SessionServerFactory {
    /**
     * Create a server.
     * 
     * @param config the configuration
     * 
     * @param props application properties
     * 
     * @throws SessionServerException if the configuration string was
     * recognized, but there was an error in constructing the server
     * 
     * @return the created server, or null if not recognised
     */
    public abstract SessionServer createServer(String config, Properties props)
	    throws SessionServerException;

    private static final ServiceLoader<SessionServerFactory> factories =
	ServiceLoader.load(SessionServerFactory.class);

    /**
     * Create a server from a configuration, by seeking
     * {@link SessionServerFactory} implementations in the classpath.
     * 
     * @param config the configuration string
     * 
     * @param props application properties
     * 
     * @return the created server
     * 
     * @throws SessionServerException if the configuration string was
     * recognized, but there was an error in constructing the server
     * 
     * @throws IllegalArgumentException if no factory recognised the
     * configuration string
     */
    public static SessionServer makeServer(String config, Properties props)
	    throws SessionServerException {
	for (SessionServerFactory factory : factories) {
	    SessionServer result = factory.createServer(config, props);
	    if (result != null) return result;
	}
	throw new IllegalArgumentException("Unrecognised configuration "
	    + config);
    }

    /**
     * Create a server from a configuration, by seeking
     * {@link SessionServerFactory} implementations in the classpath,
     * and using system properties as the application properties.
     * 
     * This method calls
     * 
     * {@link System#getProperties()},
     * 
     * and passes its first argument and the obtained properties to
     * 
     * {@link #makeServer(String, Properties)}.
     * 
     * @param config the configuration string
     * 
     * @return the created server
     * 
     * @throws IllegalArgumentException if no factory recognised the
     * configuration string
     * 
     * @throws SessionServerException if the configuration string was
     * recognized, but there was an error in constructing the server
     */
    public static SessionServer makeServer(String config)
	    throws SessionServerException {
	return makeServer(config, System.getProperties());
    }
}
