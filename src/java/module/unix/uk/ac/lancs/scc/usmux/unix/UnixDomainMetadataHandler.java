/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.unix;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

import uk.ac.lancs.scc.jardeps.Service;
import uk.ac.lancs.scc.usmux.MetadataBinding;
import uk.ac.lancs.scc.usmux.MetadataHandler;

/**
 * Recognizes meta-data for a multiplexed Unix-domain socket. The data
 * must begin with the 32-bit quantity 0x554E4958, and be followed by
 * 16-bit pid, uid, and gid.
 * 
 * @author simpsons
 */
@Service(MetadataHandler.class)
public final class UnixDomainMetadataHandler implements MetadataHandler {
    /**
     * @summary The initial four bytes of meta-data that this class
     * recognizes
     */
    public static final int MAGIC = 0x554E4958;

    @Override
    public MetadataBinding convert(byte[] from) {
	try {
	    ByteBuffer buf = ByteBuffer.wrap(from);
	    if (buf.getInt() != MAGIC) return null;
	    final int pid = buf.getShort() & 0xffff;
	    final int uid = buf.getShort() & 0xffff;
	    final int gid = buf.getShort() & 0xffff;

	    UnixDomainSocketAttributes data =
		new UnixDomainSocketAttributes() {
		    @Override
		    public int getUserId() {
			return uid;
		    }

		    @Override
		    public int getProcessId() {
			return pid;
		    }

		    @Override
		    public int getGroupId() {
			return gid;
		    }
		};
	    return MetadataBinding.of(data, UnixDomainSocketAttributes.class);
	} catch (BufferUnderflowException ex) {
	    return null;
	}
    }

}
