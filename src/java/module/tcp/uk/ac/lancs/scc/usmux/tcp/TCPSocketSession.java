package uk.ac.lancs.scc.usmux.tcp;

import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

import uk.ac.lancs.scc.usmux.Session;

class TCPSocketSession implements Session {
    private final Socket socket;

    private final OutputStream out;
    private final InputStream in;

    private boolean outClosed = false;
    private boolean inClosed = false;

    public TCPSocketSession(Socket socket) throws IOException {
	this.socket = socket;
	out = new FilterOutputStream(socket.getOutputStream()) {
	    @Override
	    public void close() throws IOException {
		closeOut();
	    }
	};
	in = new FilterInputStream(socket.getInputStream()) {
	    @Override
	    public void close() throws IOException {
		closeIn();
	    }
	};
    }

    private synchronized void closeOut() throws IOException {
	if (outClosed) return;
	outClosed = true;
	testClosed();
    }

    private synchronized void closeIn() throws IOException {
	if (inClosed) return;
	inClosed = true;
	testClosed();
    }

    private void testClosed() throws IOException {
	if (outClosed && inClosed) socket.close();
    }

    @Override
    public synchronized void close() throws IOException {
	if (inClosed && outClosed) return;
	inClosed = outClosed = true;
	socket.close();
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
	return out;
    }

    @Override
    public InputStream getInputStream() throws IOException {
	return in;
    }

    @Override
    public <T> T getAttributes(Class<T> type) {
	if (type != InetSocketAddress.class) return null;
	return type.cast(socket.getRemoteSocketAddress());
    }
}
