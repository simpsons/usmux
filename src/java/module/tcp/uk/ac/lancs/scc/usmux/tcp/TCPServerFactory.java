/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.tcp;

import java.io.File;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.ac.lancs.scc.jardeps.Service;
import uk.ac.lancs.scc.usmux.SessionServer;
import uk.ac.lancs.scc.usmux.SessionServerException;
import uk.ac.lancs.scc.usmux.SessionServerFactory;

/**
 * Creates multiplexing servers on a TCP server socket. The server can
 * be configured with a list of IP address it may accept connections
 * from. The first connection must come from 127.0.0.1 or the IP address
 * to which the socket it bound. Closing this connection signals the
 * server's readiness.
 * 
 * <p>
 * The configuration string has the regular-expression format of:
 * 
 * <pre>
 * {@value #PATTERN}
 * </pre>
 * 
 * <p>
 * The first parameter is the host name for the server to bind to. The
 * second is the port number. Subsequent parameters are IP addresses
 * which are acceptable to receive connections from; if none are
 * specified, all hosts are acceptable.
 * 
 * <p>
 * This class instantiates {@link TCPSocketServer} to manage sessions
 * over a TCP server socket.
 * 
 * @author simpsons
 */
@Service(SessionServerFactory.class)
public final class TCPServerFactory extends SessionServerFactory {
    /**
     * @undocumented
     */
    public static final String PATTERN = "^tcp:(/[^;]+);([^;]+);([0-9]+)(;[^;]+)*$";

    private static final Pattern configPattern = Pattern.compile(PATTERN);

    @Override
    public SessionServer createServer(String config, Properties props)
	    throws SessionServerException {
	Matcher m = configPattern.matcher(config);
	if (!m.matches()) return null;
	try {
	    File signalPipeName = new File(m.group(1));
	    InetAddress bindHost = InetAddress.getByName(m.group(2));
	    int bindPort = Integer.parseInt(m.group(3));
	    final ServerSocket serverSocket =
		new ServerSocket(bindPort, 5, bindHost);

	    /* Determine the set of permitted client addresses. */
	    String listParts = m.group(4);
	    final Collection<InetAddress> allowedHosts;
	    if (listParts == null) {
		allowedHosts = new AbstractCollection<InetAddress>() {
		    @Override
		    public boolean contains(Object obj) {
			return true;
		    }

		    @Override
		    public Iterator<InetAddress> iterator() {
			throw new UnsupportedOperationException();
		    }

		    @Override
		    public int size() {
			throw new UnsupportedOperationException();
		    }
		};
	    } else {
		allowedHosts = new HashSet<InetAddress>();
		for (String part : listParts.substring(1).split(";")) {
		    InetAddress host = InetAddress.getByName(part);
		    allowedHosts.add(host);
		}
	    }

	    TCPSocketServer server =
		new TCPSocketServer(signalPipeName, serverSocket, allowedHosts);
	    server.start();
	    return server;
	} catch (RuntimeException ex) {
	    throw ex;
	} catch (Exception ex) {
	    throw new SessionServerException(ex);
	}
    }
}
