package uk.ac.lancs.scc.usmux.tcp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collection;
import java.util.logging.Logger;

import uk.ac.lancs.scc.usmux.Session;
import uk.ac.lancs.scc.usmux.SessionException;
import uk.ac.lancs.scc.usmux.SessionServer;

/**
 * Creates sessions out of accepted TCP connections. The user of this
 * class should call {@link #start()} when ready to accept connections.
 * 
 * @author simpsons
 */
public class TCPSocketServer implements SessionServer {
    private final OutputStream signal;
    private final ServerSocket serverSocket;
    private final Collection<InetAddress> allowedHosts;

    /**
     * Create a server to accept TCP connections as sessions.
     * 
     * @param pipe the file to which a single byte should be written to
     * indicate readiness, and which should be closed to indicate
     * termination
     * 
     * @param serverSocket the socket on which to accept new connections
     * 
     * @param allowedHosts the set of IP addresses from which
     * connections shalled be allowed. Only the
     * {@link Collection#contains(Object)} method needs to be
     * implemented on this object.
     * 
     * @throws FileNotFoundException if the signalling pipe was not
     * found
     */
    public TCPSocketServer(File pipe, ServerSocket serverSocket,
			   Collection<InetAddress> allowedHosts) throws FileNotFoundException {
	try {
	    this.signal = new FileOutputStream(pipe);
	} finally {
	    pipe.delete();
	}
	this.serverSocket = serverSocket;
	this.allowedHosts = allowedHosts;
    }

    /**
     * Signal to the Usmux daemon that the server is ready. This happens
     * by accepting the first connection that comes from either
     * 127.0.0.1 or the address to which the server socket is bound.
     * Calls to {@link #accept()} will block until this connection has
     * been accepted.
     */
    public void start() {
	try {
	    signal.write('a');
	} catch (IOException ex) {
	    startFailure = ex;
	}
    }

    private IOException startFailure;

    @Override
    public Session accept() throws SessionException {
	if (startFailure != null)
	    throw new SessionException("start", startFailure);
	Socket socket;
	try {
	    for (;;) {
		socket = serverSocket.accept();
		if (allowedHosts.contains(socket.getInetAddress())) {
		    logger.fine(serverSocket.getLocalSocketAddress() +
			" accepted connection from " + socket.getInetAddress());
		    return new TCPSocketSession(socket);
		} else {
		    logger.warning(serverSocket.getLocalSocketAddress() +
			" rejected connection from " + socket.getInetAddress());
		    socket.close();
		    continue;
		}
	    }
	} catch (IOException ex) {
	    throw new SessionException("accepting", ex);
	}
    }

    @Override
    public void close() throws SessionException {
	try {
	    signal.close();
	} catch (IOException ex) {
	    throw new SessionException("close", ex);
	}
    }

    private static final Logger logger =
	Logger.getLogger("uk.ac.lancs.scc.usmux.tcp.server");
}
