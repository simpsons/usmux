/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.demo;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import uk.ac.lancs.scc.usmux.Session;
import uk.ac.lancs.scc.usmux.unix.UnixDomainSocketAttributes;

final class EchoThread extends Thread {
    private final boolean reverse, rot13;
    private final Session sess;
    private final int id = nextId.getAndIncrement();
    private static AtomicInteger nextId = new AtomicInteger();

    public EchoThread(Session sess, boolean reverse, boolean rot13) {
	this.reverse = reverse;
	this.rot13 = rot13;
	this.sess = sess;
	logger.info("New session: id=" + id + (this.reverse ? " reverse" : "") +
	    (this.rot13 ? " rot13" : ""));
	UnixDomainSocketAttributes attrs =
	    sess.getAttributes(UnixDomainSocketAttributes.class);
	if (attrs != null) {
	    logger.fine("Process id: " + attrs.getProcessId());
	    logger.fine("User id: " + attrs.getUserId());
	    logger.fine("Group id: " + attrs.getGroupId());
	}
    }

    public void run() {
	try {
	    InputStream in = sess.getInputStream();
	    try {
		OutputStream out = sess.getOutputStream();
		try {
		    byte[] buf = new byte[1024];
		    int len;

		    while ((len = in.read(buf)) > 0) {
			if (logger.isLoggable(Level.FINER)) {
			    StringWriter sw = new StringWriter();
			    PrintWriter str = new PrintWriter(sw);
			    try {
				str.printf("Session %d read %d bytes", id, len);
				for (int i = 0; i < len; i++) {
				    str.printf("%s%02X",
					       (i % 16 == 0) ? "\n  " : " ",
					       buf[i]);
				}
			    } finally {
				str.flush();
				logger.finer(sw.toString());
			    }
			}
			if (reverse) {
			    for (int i = 0; i < (len - 1) / 2; i++) {
				byte b = buf[i];
				buf[i] = buf[len - i - 2];
				buf[len - i - 2] = b;
			    }
			}
			if (rot13) {
			    for (int i = 0; i < len; i++) {
				if (buf[i] >= 65 && buf[i] <= 90) {
				    buf[i] -= 65;
				    buf[i] += 13;
				    buf[i] %= 26;
				    buf[i] += 65;
				} else if (buf[i] >= 97 && buf[i] <= 122) {
				    buf[i] -= 97;
				    buf[i] += 13;
				    buf[i] %= 26;
				    buf[i] += 97;
				}
			    }
			}
			out.write(buf, 0, len);
		    }
		    logger.info("Session " + id + " closed");
		} finally {
		    out.close();
		}
	    } finally {
		in.close();
	    }
	} catch (IOException ex) {
	    logger.log(Level.SEVERE, "Session " + id, ex);
	}
    }

    private static final Logger logger =
	Logger.getLogger("uk.ac.lancs.scc.usmux.demo.echo");
}
