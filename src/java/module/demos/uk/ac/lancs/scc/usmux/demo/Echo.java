/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.ac.lancs.scc.usmux.demo;

import java.util.ArrayList;
import java.util.List;

import uk.ac.lancs.scc.usmux.Session;
import uk.ac.lancs.scc.usmux.SessionException;
import uk.ac.lancs.scc.usmux.SessionServer;
import uk.ac.lancs.scc.usmux.SessionServerFactory;

/**
 * A simple server that echos back its clients' streams.
 * 
 * @author simpsons
 */
public final class Echo {
    /**
     * Create and run a server to echo back clients' streams.
     * 
     * @param args The first argument must be the server configuration
     * string.
     * @throws SessionException
     */
    public static void main(String[] args) throws SessionException {
	List<Thread> servers = new ArrayList<Thread>();
	boolean reverse = false, rot13 = false;
	for (String arg : args) {
	    if ("-r".equals(arg)) {
		reverse = !reverse;
	    } else if ("-13".equals(arg)) {
		rot13 = !rot13;
	    } else {
		System.err.printf("Starting %s%sserver on %s%n",
				  reverse ? "reverse " : "",
				  rot13 ? "rot13 " : "", arg);
		final SessionServer server =
		    SessionServerFactory.makeServer(arg);
		final boolean myReverse = reverse;
		final boolean myRot13 = rot13;
		Thread t = new Thread() {
		    public void run() {
			try {
			    server.start();
			    Session conn;
			    try {
				while ((conn = server.accept()) != null) {
				    new EchoThread(conn, myReverse, myRot13)
					    .start();
				}
			    } catch (SessionException ex) {
				System.err.println(ex);
			    } finally {
				server.close();
			    }
			} catch (SessionException ex) {
			    System.err.println(ex);
			}
		    }
		};
		servers.add(t);
	    }
	}
	for (Thread t : servers)
	    t.start();
    }
}
