// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stddef.h>
#include <stdbool.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include <react/condtype.h>
#include <react/conds.h>
#include <ddslib/dllist.h>

#include "mux.h"
#include "mux_proto.h"

#define BASE_VERBOSITY 2

extern int verbosity;

static void hex_block(FILE *out, const char *pre,
		      const struct buf *buf, size_t lim)
{
  if (lim > buf_rem(buf))
    lim = buf_rem(buf);
  const unsigned char *base = buf_base(buf);

  for (size_t j = 0; j < lim; j += 16) {
    fprintf(out, "%s", pre);

    const size_t nj = j + 16;
    size_t ulim = (nj < lim ? nj : lim) + 16 - nj;

    /* Print hex values in groups of 16. */
    for (size_t i = 0; i < 16 ; i++)
      if (i < ulim)
	fprintf(out, "%02X ", base[i + j]);
      else
	fprintf(out, ".. ");

    /* Print printable characters in groups of 16, on the same line. */
    for (size_t i = 0; i < 16 ; i++)
      if (i < ulim) {
	int c = base[i + j];
	if (isgraph(c))
	  fprintf(out, "%c", c);
	else
	  putc('.', out);
      } else
	putc(' ', out);

    putc('\n', out);
  }

  if (lim < buf_rem(buf))
    fprintf(out, "%s...\n", pre);
}

struct mux_session_tag {
  struct mux_core_tag *owner;
  dllist_elem(struct mux_session_tag) in_all, in_writing, in_changed;
  _Bool writing, connected, acked, changed;
  mux_sid server_sid, our_sid;
  struct buf meta;

  void *user;
  struct mux_dir_tag {
    /* For 'up', this is the function to call the client to get more
       data from it.  For 'down', this is the function to call the
       client to submit data to it. */
    mux_xfer_func func;

    /* For 'up', this tells us that a prior recv(client)/up.func()
       returned BLOCK, so we can't call it again until told to do so
       by recvokay().  For 'down', this tells us that a prior
       send(client)/down.func() returned BLOCK, so we can't call it
       again until told to do so by sendokay(). */
    _Bool blocked;

    /* For 'up', this tells us that we have received an STP message,
       and do not need to send any more data upstream; it will be
       discarded anyway.  For 'down', it tells us that we must send an
       STP message upstream; we set it when our client tells us that
       it is no longer accepting data. */
    _Bool stopped;

    /* For 'up', 'eof' tells us that we received EOF from the client
       when reading from it, and 'sent_eof' tells us that we have
       already sent an EOF message to the server.  For 'down', 'eof'
       tells us that we received an EOF message from the server, and
       'eof_sent' tells us that we have passed EOF on to the
       client. */
    _Bool eof, sent_eof;

    /* For 'up', this is how much the server has cleared us to send by
       us receiving CTS messages.  For 'down', this is how much we
       have yet to send upstream in CTS messages. */
    uint_least32_t cleared;

    /* For 'up', this is the first error when calling
       recv(client)/up.func().  For 'down', this is the first error
       when calling send(client)/down.func(). */
    int error;

    struct buf buf;
  } down, up;
};

static void display_dir_status(const char *nm, struct mux_dir_tag *dir)
{
  fprintf(stderr, " %s(%" PRIuLEAST32, nm, dir->cleared);
  if (dir->blocked)
    fprintf(stderr, " BLOCKED");
  if (dir->stopped)
    fprintf(stderr, " STOPPED");
  if (dir->eof)
    fprintf(stderr, " EOF");
  if (dir->sent_eof)
    fprintf(stderr, " SENT_EOF");
  if (dir->error != 0)
    fprintf(stderr, " %d(%s)", dir->error, strerror(dir->error));
  putc(')', stderr);
}

static void display_pipe_status(mux_core *me)
{
  fprintf(stderr, "  Pipes:");

  if (me->down.eof)
    fprintf(stderr, " DOWN.EOF");
  if (me->down.error != 0)
    fprintf(stderr, " DOWN:%d", me->down.error);
  if (react_isprimed(me->down.event))
    fprintf(stderr, " DOWN");

  if (me->up.error != 0)
    fprintf(stderr, " UP:%d", me->up.error);
  if (react_isprimed(me->up.event))
    fprintf(stderr, " UP");

  fprintf(stderr, "\n");

  fprintf(stderr, "  Sessions:\n");
  for (struct mux_session_tag *sess = dllist_first(&me->all_sessions);
       sess != NULL; sess = dllist_next(in_all, sess)) {
    fprintf(stderr, "  %p %u<->%u", (void *) sess,
	    (unsigned) sess->our_sid, (unsigned) sess->server_sid);
    display_dir_status("up", &sess->up);
    display_dir_status("down", &sess->down);
    putc('\n', stderr);
  }
}

static inline _Bool server_okay(mux_core *me)
{
  return me->down.error == 0 && !me->down.eof && me->up.error == 0;
}

static inline _Bool client_okay(struct mux_session_tag *sess)
{
  return sess->down.error == 0 && sess->up.error == 0;
}

static inline _Bool never_to_send_down_again(struct mux_session_tag *sess)
{
  /* If a previous call to the client failed, we can't send anything
     else. */
  if (!client_okay(sess))
    return true;

  /* If we haven't sent EOF to the client, we're not done. */
  if (!sess->down.sent_eof)
    return false;

  return true;
}

static inline _Bool something_to_send_down_now(struct mux_session_tag *sess)
{
  if (never_to_send_down_again(sess)) return false;

  /* If the server has died, we have to tell the client. */
  if (!server_okay(sess->owner))
    return true;

  /* Do we have data to send? */
  if (buf_rem(&sess->down.buf) > 0)
    return true;

  /* Have we reached the end of the stream? */
  if (sess->down.eof && !sess->down.sent_eof)
    return true;

  return false;
}

static inline _Bool something_to_send_up(struct mux_session_tag *sess)
{
  mux_core *me = sess->owner;

  /* Have we yet to send the NEW message? */
  if (!sess->connected) return true;

  /* Have we sent all of our meta-data? */
  if (buf_rem(&sess->meta) > 0) return true;

  /* We can't send any other message until we know what the server's
     sid is. */
  if (!sess->acked) return false;

  /* Are we in the process of sending a STR message? */
  if (sess->writing &&
      sess == dllist_first(&me->writing_sessions) &&
      me->up.payload > 0)
    return true;

  /* Have we an outstanding STP message to send? */
  if (sess->down.stopped) return true;

  /* Do we have an outstanding CTS message to send?  TODO: Only send
     CTS if it is at least half the buffer size. */
  if (sess->down.cleared > 0) return true;

  /* If we've been told by the server to stop, we can't send EOF, or
     start a new STR.  IOW, we can't say any more about the upward
     stream. */
  if (sess->up.stopped) return false;

  /* Do we have data to send, and are cleared to send it? */
  if (sess->up.cleared > 0 && buf_rem(&sess->up.buf) > 0) return true;

  /* Do we have an outstanding EOF message to send? */
  if (buf_rem(&sess->up.buf) == 0 &&
      sess->up.eof && !sess->up.sent_eof) return true;

  return false;
}

static void destroy_session(struct mux_session_tag *sess)
{
  assert(sess);

  mux_core *me = sess->owner;
  if (verbosity > BASE_VERBOSITY + 1) {
    fprintf(stderr, "  -- %p %p\n", (void *) sess, sess->user);
  }

  /* Remove the session from the hashtable. */
  htab_const key;
  key.unsigned_integer = sess->our_sid;
  htab_del(me->chptrs, key);

  /* Remove the session from the writing list.  If it's the head, and
     we have a payload to send, arse. */
  if (sess->writing) {
    assert(!me->down.eof && me->up.error == 0 &&
	   (sess != dllist_first(&me->writing_sessions) ||
	    me->up.payload == 0));
    dllist_unlink(&me->writing_sessions, in_writing, sess);
  }

  if (sess->changed) {
    dllist_unlink(&me->changed_sessions, in_changed, sess);
  }

  if (me->down.sess == sess) {
    /* We're receiving data from the server on this session. */
    me->down.code = mux_MNOP;
    me->down.sess = NULL;
  }

  /* Remove the session from the main list. */
  dllist_unlink(&me->all_sessions, in_all, sess);

  /* Tell the user not to use this session again. */
  assert(sess->up.func);
  sess->up.func(sess->user, NULL, NULL);
  sess->up.func = NULL;
  sess->down.func = NULL;

  free(sess);
}

static void check_session_end(struct mux_session_tag *sess)
{
  mux_core *me = sess->owner;

  /* We can't be destroyed if we've still got something to write to the
     server. */
  if (sess->writing) {
    assert(something_to_send_up(sess));
    return;
  }

  /* We can't be destroyed if we've still got something to send to the
     client. */
  if (!never_to_send_down_again(sess))
    return;

  destroy_session(sess);
}

static void stop_session_writing(struct mux_session_tag *sess)
{
  assert(sess->writing);
  mux_core *me = sess->owner;
  assert(dllist_first(&me->writing_sessions) == sess);
  dllist_unlink(&me->writing_sessions, in_writing, sess);
  sess->writing = false;
}

static void change_session(mux_session sess)
{
  if (sess->changed) return;
  sess->changed = true;
  mux_core *me = sess->owner;
  dllist_append(&me->changed_sessions, in_changed, sess);
}

static void change_all_sessions(mux_core *me)
{
  for (struct mux_session_tag *sess = dllist_first(&me->all_sessions);
       sess != NULL; sess = dllist_next(in_all, sess))
    change_session(sess);
}

static void prepare_session_data(struct mux_session_tag *sess)
{
  mux_core *me = sess->owner;

  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p):\n", __func__, (void *) sess);
  }

  /* This session must be the head of the list. */
  assert(sess->writing);
  assert(sess == dllist_first(&me->writing_sessions));

  /* There must be no header or payload already outstanding. */
  assert(buf_rem(&me->up.buf) == 0);
  assert(me->up.payload == 0);

  if (!sess->connected) {
    /* We haven't sent the NEW message yet for this session. */

    /* Set up the buffer to open a new session, specifying our sid. */
    assert(buf_rem(&me->up.buf) == 0);
    buf_clear(&me->up.buf);
    buf_putu16(&me->up.buf, mux_MNEW);
    assert(buf_rem(&sess->meta) <= 0xfffdu);
    buf_putu16(&me->up.buf, 2 + buf_rem(&sess->meta));
    buf_putu16(&me->up.buf, sess->our_sid);
    buf_flip(&me->up.buf);
    assert(me->up.payload == 0);

    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> NEW(%u) %p %p\n", (unsigned) sess->our_sid,
	      (void *) sess, sess->user);
    }

    /* Record that we have sent the NEW message. */
    sess->connected = true;

    /* Move this session to the back of the writing queue. */
    if (buf_rem(&sess->meta) == 0) {
      stop_session_writing(sess);
      change_session(sess);
    }
    assert((buf_rem(&sess->meta) > 0) == (sess->writing));
  } else if (buf_rem(&sess->meta) > 0) {
    /* We're still sending meta-data. */
    buf_slide(&me->up.buf);
    size_t done = buf_move(&me->up.buf, &sess->meta, -1);
    buf_flip(&me->up.buf);
    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> NEW(%u) %p %p payload %zu + %zu\n",
	      (unsigned) sess->our_sid, (void *) sess, sess->user,
	      done, buf_rem(&sess->meta));
    }

    if (buf_rem(&sess->meta) == 0) {
      /* Move this session to the back of the writing queue. */
      stop_session_writing(sess);
      change_session(sess);
    }
    assert((buf_rem(&sess->meta) == 0) == (!sess->writing));
  } else if (sess->acked && !sess->up.stopped &&
	     sess->up.cleared > 0 &&
	     buf_rem(&sess->up.buf) > 0) {
    /* We have data to send, and we are cleared to send some of
       it. */

    /* How much can we send?  It's the minimum of how much we've
       got, how much we're cleared to send, and the message payload
       limit minus 2. */
    size_t amount = buf_rem(&sess->up.buf);
    if (amount > sess->up.cleared)
      amount = sess->up.cleared;
    if (amount > 0xfffdu)
      amount = 0xfffdu;
    assert(amount > 0);

    /* Set up the buffer to send a STR message, followed by an
       acceptable payload. */
    assert(buf_rem(&me->up.buf) == 0);
    buf_clear(&me->up.buf);
    buf_putu16(&me->up.buf, mux_MSTR);
    buf_putu16(&me->up.buf, 2u + amount);
    buf_putu16(&me->up.buf, sess->server_sid);
    buf_flip(&me->up.buf);
    me->up.payload = amount;

    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> STR(%u) + %zu %p %p\n",
	      (unsigned) sess->server_sid, amount,
	      (void *) sess, sess->user);
    }

    /* Record that we're no longer cleared to send this amount. */
    sess->up.cleared -= amount;

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  %" PRIuLEAST32 " remaining to be cleared\n",
	      sess->up.cleared);
    }

    /* This session is left at the head of the writing queue. */
    assert(dllist_first(&me->writing_sessions) == sess);
    change_session(sess);
  } else if (sess->acked && !sess->up.stopped &&
	     buf_rem(&sess->up.buf) == 0 &&
	     sess->up.eof && !sess->up.sent_eof) {
    /* We've sent all the data down, and we've received EOF from the
       client, but we haven't terminated the stream to the
       server, nor have we received an STP from the server. */

    assert(buf_rem(&sess->up.buf) == 0);

    /* Set up the buffer to send an EOF message. */
    assert(buf_rem(&me->up.buf) == 0);
    buf_clear(&me->up.buf);
    buf_putu16(&me->up.buf, mux_MEOS);
    buf_putu16(&me->up.buf, 2);
    buf_putu16(&me->up.buf, sess->server_sid);
    buf_flip(&me->up.buf);
    assert(me->up.payload == 0);

    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> EOF(%u) %p %p\n", (unsigned) sess->server_sid,
	      (void *) sess, sess->user);
    }

    /* Record that we've sent the EOF message. */
    sess->up.sent_eof = true;

    /* Move this session to the back of the writing queue. */
    stop_session_writing(sess);
    change_session(sess);
  } else if (sess->acked && sess->down.stopped) {
    /* Set up the buffer to send an STP message. */
    assert(buf_rem(&me->up.buf) == 0);
    buf_clear(&me->up.buf);
    buf_putu16(&me->up.buf, mux_MSTP);
    buf_putu16(&me->up.buf, 2);
    buf_putu16(&me->up.buf, sess->server_sid);
    buf_flip(&me->up.buf);
    assert(me->up.payload == 0);

    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> STP(%u) %p %p\n", (unsigned) sess->server_sid,
	      (void *) sess, sess->user);
    }

    /* Record that we've sent the STP message. */
    sess->down.stopped = false;

    /* Move this session to the back of the writing queue. */
    stop_session_writing(sess);
    change_session(sess);
  } else if (sess->acked && sess->down.cleared > 0) { 
    /* Work out how much to offer. */
    size_t offer = sess->down.cleared;
    if (offer > 0x7fffffffu)
      offer = 0x7fffffffu;

    /* Set up the buffer to send a CTS message. */
    assert(buf_rem(&me->up.buf) == 0);
    buf_clear(&me->up.buf);
    buf_putu16(&me->up.buf, mux_MCTS);
    buf_putu16(&me->up.buf, 6);
    buf_putu16(&me->up.buf, sess->server_sid);
    buf_putu32(&me->up.buf, offer);
    buf_flip(&me->up.buf);
    assert(me->up.payload == 0);

    if (verbosity > BASE_VERBOSITY + 0) {
      fprintf(stderr, "  -> CTS(%u, %zu) %p %p\n",
	      (unsigned) sess->server_sid, offer,
	      (void *) sess, sess->user);
    }

    /* Record that we have cleared the server to send the offered
       number of bytes. */
    sess->down.cleared -= offer;

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  %" PRIuLEAST32 " remaining to be cleared\n",
	      sess->down.cleared);
    }

    /* Move this session to the back of the writing queue. */
    stop_session_writing(sess);
    change_session(sess);
  } else {
    /* What is the session doing here, if not to generate data? */
    fprintf(stderr, "%p/%p:", (void *) sess, sess->user);
    if (sess->connected) fprintf(stderr, " connected");
    if (sess->acked) fprintf(stderr, " acked");
    fprintf(stderr, " meta=%zu", buf_rem(&sess->meta));
    putc('\n', stderr);
    assert(false);
  }

  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "  prepared %zu header bytes:\n", buf_rem(&me->up.buf));
    hex_block(stderr, "  ", &me->up.buf, -1);
    if (me->up.payload > 0) {
      fprintf(stderr, "  prepared %zu STR payload bytes:\n", me->up.payload);
      hex_block(stderr, "  ", &sess->up.buf, me->up.payload);
    }
  }
}

static int prime_up_write(mux_core *me)
{
  react_fdcond cond = {
    .handle = me->up.fd,
    .mode = react_MOUT,
  };
  return react_prime(me->up.event, react_CFD, &cond);
}

static int prime_down_read(mux_core *me)
{
  react_fdcond cond = {
    .handle = me->down.fd,
    .mode = react_MIN,
  };
  return react_prime(me->down.event, react_CFD, &cond);
}

/* Check whether a session is ready to write something up to the
   server.  Put it on the writing queue if it is; take it off if
   not. */
static void check_session_for_send_up(struct mux_session_tag *sess)
{
  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p)\n", __func__, (void *) sess);
  }
  mux_core *me = sess->owner;

  if (server_okay(me)) {
    /* Append the session to the writing queue if it has something to
       say. */
    if (something_to_send_up(sess)) {
      /* Ensure the session is in the writing queue. */
      if (!sess->writing) {
	dllist_append(&me->writing_sessions, in_writing, sess);
	sess->writing = true;
      }
    } else {
      /* Make sure this session is out of the writing queue. */
      if (sess->writing) {
	dllist_unlink(&me->writing_sessions, in_writing, sess);
	sess->writing = false;
      }
    }
  } else {
    /* The server is down.  Ensure that this session is not writing to
       it any more. */
    if (sess->writing) {
      dllist_unlink(&me->writing_sessions, in_writing, sess);
      sess->writing = false;
    }
  }

  if (server_okay(me)) {
    /* Do we already have some data prepared for writing? */
    if (buf_rem(&me->up.buf) == 0 && me->up.payload == 0) {
      /* No.  Can we get some? */
      struct mux_session_tag *sess = dllist_first(&me->writing_sessions);
      if (sess) {
	/* Get the first session in the queue to prepare some data. */
	prepare_session_data(sess);
      }
    }

    if (buf_rem(&me->up.buf) > 0 || me->up.payload > 0) {
      /* We have something to write, so be sure to ask when that will be
	 possible. */
      if (!react_isprimed(me->up.event))
	prime_up_write(me);
    } else {
      /* We have nothing to write, so no need to wait until we can
	 write. */
      react_cancel(me->up.event);
    }
  } else {
    react_cancel(me->up.event);
  }
}

/* Check whether a session is ready to write something down to
   the client. */
static void check_session_for_send_down(struct mux_session_tag *sess)
{
  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p)\n", __func__, (void *) sess);
  }
  mux_core *me = sess->owner;

  if (!server_okay(me)) {
    /* There's something wrong with the server, so we can't really do
       anything but terminate the stream to the client. */
    sess->down.eof = true;
  }

  if (!client_okay(sess)) {
    /* A previous write to the client failed.  Make sure everything
       stops. */
  } else if (buf_rem(&sess->down.buf) > 0) {
    /* We have data to send. */
    if (!sess->down.blocked) {
      /* We've not been told not to send data to the client, so let's
	 try it. */
      mux_rcode res =
	sess->down.func(sess->user, &sess->down.buf, &sess->down.error);
      if (res == mux_BLOCK) {
	/* We've just been told not to send data for now.  Record
	   this, so we don't try again until we're cleared. */
	sess->down.blocked = true;
      } else if (res == mux_ERROR) {
	/* We've been told not to send data ever again.  Record that
	   we must tell the server to stop sending data. */
	sess->down.stopped = true;
	if (verbosity > BASE_VERBOSITY + 2) {
	  fprintf(stderr, "  %p failed: %d (%s)\n",
		  (void *) sess, sess->down.error, strerror(sess->down.error));
	}
      } else {
	assert(res != mux_EOF);
	assert(res > 0);

	/* Record that the server is permitted to send some more
	   bytes on this session. */
	if (verbosity > BASE_VERBOSITY + 2) {
	  fprintf(stderr, "  %p sent %zu bytes down to %p\n",
		  (void *) sess, (size_t) res, sess->user);
	}
	sess->down.cleared += res;
	assert(sess->down.cleared > 0);
	change_session(sess);
      }
    }
  } else if (sess->down.eof && !sess->down.sent_eof) {
    /* We have no data to send, have received EOF from the server, and
       we need to tell the client about EOF. */

    sess->down.func(sess->user, NULL, NULL);
    sess->down.sent_eof = true;
  }
}

static void check_session_for_recv_up(mux_session sess)
{
  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p)\n", __func__, (void *) sess);
  }
  mux_core *me = sess->owner;

  /* Work out if we need to discard data.  We only do this if the
     server has told us to stop, and we are not currently sending a
     payload. */
  bool discard = sess->up.stopped &&
    (sess != dllist_first(&me->writing_sessions) ||
     me->up.payload == 0);

  if (client_okay(sess) && !sess->up.blocked) {
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  upstream buffer (%zu bytes):\n",
	      buf_rem(&sess->up.buf));
      hex_block(stderr, "  ", &sess->up.buf, -1);
    }

    /* Prepare the buffer for writing, and to find out how much we can
       write. */
    buf_slide(&sess->up.buf);

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  available space: %zu\n", buf_rem(&sess->up.buf));
    }

    /* Keep reading from the user until we run out of space, the user
       runs out of data (for now), or the user reports EOF/error. */
    while (buf_rem(&sess->up.buf) > 0 &&
	   !sess->up.blocked &&
	   !sess->up.eof &&
	   sess->up.error == 0) {
      /* We have space to read from the client. */
      mux_rcode rc = sess->up.func(sess->user, &sess->up.buf, &sess->up.error);

      if (rc == mux_EOF || rc == mux_ERROR) {
	if (rc == mux_ERROR) {
	  if (verbosity > BASE_VERBOSITY + 1) {
	    fprintf(stderr, "  error: %d %s\n",
		    sess->up.error, strerror(sess->up.error));
	  }
	} else {
	  assert(rc == mux_EOF);
	  if (verbosity > BASE_VERBOSITY + 1) {
	    fprintf(stderr, "  EOF\n");
	  }
	}

	/* Either way, that's the end of the upward stream. */
	sess->up.eof = true;
      } else if (rc == mux_BLOCK) {
	if (verbosity > BASE_VERBOSITY + 2) {
	  fprintf(stderr, "  BLOCK\n");
	}

	/* We can't get data again until recvokay is called. */
	sess->up.blocked = true;
      } else {
	/* Some data was read, so nothing else to do.  (The buffer was
	   already updated by the call.) */
	if (verbosity > BASE_VERBOSITY + 2) {
	  fprintf(stderr, "  read %zu\n", (size_t) rc);
	}
      }
    }

    /* If the server has expressed to receive no more data, we can
       discard what we have. */
    if (discard) {
      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded due to stream stop\n");
      }
      buf_clear(&sess->up.buf);
    }

    /* Put the buffer back into read mode. */
    buf_flip(&sess->up.buf);

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  upstream buffer (%zu bytes):\n",
	      buf_rem(&sess->up.buf));
      hex_block(stderr, "  ", &sess->up.buf, -1);
    }
  }
}

static void flush_changed_session(mux_session sess)
{
  mux_core *me = sess->owner;
  assert(sess->changed);
  dllist_unlink(&me->changed_sessions, in_changed, sess);
  sess->changed = false;

  check_session_for_recv_up(sess);
  check_session_for_send_up(sess);
  check_session_for_send_down(sess);
  check_session_end(sess);
}

static void flush_changed_sessions(mux_core *me)
{
  for (struct mux_session_tag *sess = dllist_first(&me->changed_sessions);
       sess != NULL; sess = dllist_first(&me->changed_sessions)) {
    assert(sess->changed);
    flush_changed_session(sess);
  }
}

static void on_server_ready_for_data(void *vp)
{
  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p):\n", __func__, vp);
  }
  mux_core *me = vp;

  assert(me->up.error == 0);
  if (buf_rem(&me->up.buf) > 0) {
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  %zu header bytes:\n", buf_rem(&me->up.buf));
      hex_block(stderr, "  ", &me->up.buf, -1);
    }
    /* We have a header to send.  Try to write it to the server. */
    ssize_t got = buf_write(me->up.fd, &me->up.buf, -1);
    if (got < 0) {
      me->up.error = errno;
      if (verbosity > BASE_VERBOSITY + 1) {
	fprintf(stderr, "  error: %d %s\n", errno, strerror(errno));
      }
      change_all_sessions(me);
      goto out;
    }
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  sent %zd\n", got);
    }
  } else if (me->up.payload > 0) {
    struct mux_session_tag *sess = dllist_first(&me->writing_sessions);
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  sess %p\n", (void *) sess);
    }
    /* We shouldn't be here unless one of our sessions has something
       to say. */
    assert(sess);
    assert(sess->writing);

    if (verbosity > BASE_VERBOSITY + 1) {
      fprintf(stderr, "  %zu STR payload bytes:\n", me->up.payload);
      hex_block(stderr, "  ", &sess->up.buf, me->up.payload);
    }

    /* There is still some payload of a STR message to send. */
    ssize_t got = buf_write(me->up.fd, &sess->up.buf, me->up.payload);
    if (got < 0) {
      me->up.error = errno;
      if (verbosity > BASE_VERBOSITY + 1) {
	fprintf(stderr, "  error: %d %s\n", errno, strerror(errno));
      }
      change_all_sessions(me);
      goto out;
    }
    assert(me->up.payload >= (size_t) got);
    me->up.payload -= got;

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  sent %zd; remaining %zu\n", got, me->up.payload);
    }

    if (me->up.payload == 0) {
      /* We've completed the message.  Take the session off the
	 queue. */
      stop_session_writing(sess);
    }
    change_session(sess);
  }

  /* Check whether we've finished with the current job. */
  if (buf_rem(&me->up.buf) == 0 && me->up.payload == 0) {
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  message complete\n");
    }

    /* See if we've got more work to do.  Check the next session on
       the list. */
    struct mux_session_tag *sess = dllist_first(&me->writing_sessions);
    if (sess)
      change_session(sess);
  }

  /* If we haven't had an I/O error with the server, and we've got
     either a header or stream payload to deliver, schedule the write
     event. */
  if (!react_isprimed(me->up.event) && server_okay(me) &&
      (buf_rem(&me->up.buf) > 0 || me->up.payload > 0)) {
    /* Reschedule the write event. */
    prime_up_write(me);
  }

out:
  flush_changed_sessions(me);
  if (verbosity > BASE_VERBOSITY + 0) {
    display_pipe_status(me);
  }
}

/* Identify the current session, or return true if not found. */
static _Bool session_not_found(mux_core *me)
{
  // If we've already got it, no problem!
  if (me->down.sess) return false;

  // Find the relevant session.
  htab_const key;
  key.unsigned_integer = me->down.sid;
  htab_obj res;
  if (htab_get(me->chptrs, key, &res)) {
    me->down.sess = res.pointer;
    assert(me->down.sess->our_sid == me->down.sid);
    return false;
  }
  assert(me->down.sess == NULL);
  return true;
}

/* Try to consume signalling data from the server, and return true if
 * there is more to do. */
static bool consume_signalling_from_server(mux_core *me)
{
  /* There's nothing more to do if we've run out of data. */
  if (buf_rem(&me->down.buf) == 0)
    return false;

  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "  incoming header:\n");
    hex_block(stderr, "  ", &me->down.buf, -1);
  }

  /* Do we already know what message type this is?  Can we find
     out now if not? */
  if (me->down.code == mux_MUNK) {
    if (buf_rem(&me->down.buf) >= 4) {
      /* Decode the message type and length. */
      me->down.code = buf_getu16(&me->down.buf);
      me->down.payload = buf_getu16(&me->down.buf);

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  Started to receive %s + %zu\n",
		mux_getmsgname(me->down.code), me->down.payload);
	fprintf(stderr, "  %zu bytes now in header:\n",
		buf_rem(&me->down.buf));
	hex_block(stderr, "  ", &me->down.buf, -1);
      }
    } else {
      /* We need more data. */
      return false;
    }
  } else {
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  decoding %s with payload %zu\n",
	      mux_getmsgname(me->down.code), me->down.payload);
    }
  }

  /* Act on the message type, if complete. */
  switch (me->down.code) {
  case mux_MNOP: {
    /* We're discarding all the data. */
    size_t got = buf_rem(&me->down.buf);
    size_t todo = me->down.payload;
    if (got >= todo) {
      buf_skip(&me->down.buf, todo);
      me->down.payload = 0;
    } else {
      assert(got < todo);
      me->down.payload -= got;
      buf_skip(&me->down.buf, got);
      assert(buf_rem(&me->down.buf) == 0);
    }
  } break;

  case mux_MRDY:
    if (buf_rem(&me->down.buf) >= 2) {
      /* Record that the server is ready. */
      me->meta_lim = buf_getu16(&me->down.buf);
      me->down.payload -= 2;
      me->ready = true;

      if (verbosity > BASE_VERBOSITY + 0) {
	fprintf(stderr, "  <- RDY(%zu)\n", me->meta_lim);
      }

      /* We don't need any more data. */
      me->down.code = mux_MNOP;
    } else if (me->down.payload < 2) {
      /* There is insufficient data in the payload to get the
       * meta limit. */
      me->meta_lim = 0;
      me->ready = true;

      if (verbosity > BASE_VERBOSITY + 0) {
	fprintf(stderr, "  <- RDY(0) (assumed limit)\n");
      }

      /* We don't need any more data. */
      me->down.code = mux_MNOP;
    } else {
      /* We need more data. */
      return false;
    }
    break;

  case mux_MACK:
    /* The server acknowledges a session, echoing the client's
       session id, and providing its own. */
    if (buf_rem(&me->down.buf) >= 4) {
      me->down.sid = buf_getu16(&me->down.buf);
      mux_sid server_sid = buf_getu16(&me->down.buf);
      me->down.payload -= 4;

      /* We don't need any more data. */
      me->down.code = mux_MNOP;

      assert(!me->down.sess);
      if (session_not_found(me)) {
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- ACK(%u, %u) ??\n",
		  (unsigned) me->down.sid, (unsigned) server_sid);
	}
      } else {
	mux_session sess = me->down.sess;
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- ACK(%u, %u) %p %p\n",
		  (unsigned) me->down.sid, (unsigned) server_sid,
		  (void *) sess, sess->user);
	}
	sess->server_sid = server_sid;
	sess->acked = true;
	change_session(sess);
      }
    } else if (me->down.payload < 4) {
      /* There is insufficient data in the payload to make any
	 use of this message. */
      me->down.code = mux_MNOP;

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded; insufficient payload\n");
      }
    } else {
      /* We need more data. */
      return false;
    }
    break;

  case mux_MCTS:
    /* The server is offering to accept another n bytes on its
       input stream for a given session. */
    if (buf_rem(&me->down.buf) >= 6) {
      /* Read the session id and the number of bytes. */
      me->down.sid = buf_getu16(&me->down.buf);
      uint_least32_t amount = buf_getu32(&me->down.buf);
      me->down.payload -= 6;

      /* We don't need any more data. */
      me->down.code = mux_MNOP;

      if (session_not_found(me)) {
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- CTS(%u, %" PRIuLEAST32 ") ??\n",
		  (unsigned) me->down.sid, amount);
	}
      } else {
	mux_session sess = me->down.sess;
	sess->up.cleared += amount;
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- CTS(%u, %" PRIuLEAST32 ") %p %p: now %"
		  PRIuLEAST32 "\n",
		  (unsigned) me->down.sid, amount,
		  (void *) sess, sess->user, sess->up.cleared);
	}
	change_session(sess);
      }
    } else if (me->down.payload < 6) {
      /* There is insufficient data in the payload to make any
	 use of this message. */
      me->down.code = mux_MNOP;

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded; insufficient payload\n");
      }
    } else {
      /* We need more data. */
      return false;
    }
    break;

  case mux_MEOS:
    if (buf_rem(&me->down.buf) >= 2) {
      /* Read the session id. */
      me->down.sid = buf_getu16(&me->down.buf);
      me->down.payload -= 2;

      /* We don't need any more data. */
      me->down.code = mux_MNOP;

      if (session_not_found(me)) {
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- EOF(%u) ??\n", (unsigned) me->down.sid);
	}
      } else {
	mux_session sess = me->down.sess;
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- EOF(%u) %p %p\n", (unsigned) me->down.sid,
		  (void *) sess, sess->user);
	}

	/* Mark the server's output stream as ended, and inform
	   the session about it. */
	sess->down.eof = true;
	change_session(sess);
      }
    } else if (me->down.payload < 2) {
      /* There is insufficient data in the payload to make any
	 use of this message. */
      me->down.code = mux_MNOP;

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded; insufficient payload\n");
      }
    } else {
      /* We need more data. */
      return false;
    }
    break;

  case mux_MSTP:
    if (buf_rem(&me->down.buf) >= 2) {
      /* Read the session id. */
      me->down.sid = buf_getu16(&me->down.buf);
      me->down.payload -= 2;

      /* We don't need any more data. */
      me->down.code = mux_MNOP;

      if (session_not_found(me)) {
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- STP(%u) ??\n", (unsigned) me->down.sid);
	}
	me->down.code = mux_MNOP;
      } else {
	mux_session sess = me->down.sess;
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- STP(%u) %p %p\n", (unsigned) me->down.sid,
		  (void *) sess, sess->user);
	}
	/* Mark the server's input stream as not expecting more
	   data, and inform the session about it. */
	me->down.sess->up.stopped = true;
	change_session(me->down.sess);
      }
    } else if (me->down.payload < 2) {
      /* There is insufficient data in the payload to make any
	 use of this message. */
      me->down.code = mux_MNOP;

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded; insufficient payload\n");
      }
    } else {
      /* We need more data. */
      return false;
    }
    break;

  case mux_MSTR:
    if (buf_rem(&me->down.buf) >= 2) {
      /* Read the session id. */
      me->down.sid = buf_getu16(&me->down.buf);
      me->down.payload -= 2;

      /* Find the session. */
      if (session_not_found(me)) {
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- STR(%u)+%zu ??\n",
		  (unsigned) me->down.sid, me->down.payload);
	}
	me->down.code = mux_MNOP;
      } else if (me->down.payload == 0) {
	/* It's an empty STR message.  We don't need it any
	   more. */
	mux_session sess = me->down.sess;
	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- STR(%u) empty %p %p\n",
		  (unsigned) me->down.sid, (void *) sess, sess->user);
	}
	me->down.code = mux_MNOP;
      } else {
	assert(me->down.payload > 0);

	/* We have some of the STR payload already. */
	mux_session sess = me->down.sess;
	assert(sess);

	if (verbosity > BASE_VERBOSITY + 0) {
	  fprintf(stderr, "  <- STR(%u)+%zu %p %p\n",
		  (unsigned) me->down.sid, me->down.payload,
		  (void *) sess, sess->user);
	}

	if (buf_rem(&me->down.buf) > 0) {
	  /* Some of the payload is in the signalling buffer.  Move as
	     much as we can of the remaining bytes into the session's
	     buffer. */
	  buf_slide(&sess->down.buf);
	  size_t done =
	    buf_move(&sess->down.buf, &me->down.buf, me->down.payload);
	  me->down.payload -= done;
	  buf_flip(&sess->down.buf);

	  if (verbosity > BASE_VERBOSITY + 0) {
	    fprintf(stderr, "  <- STR(%u)... %zu + %zu %p %p !\n",
		    (unsigned) me->down.sid, done, me->down.payload,
		    (void *) sess, sess->user);
	  }

	  /* Tell the session that it has bytes from the server
	     to send to the client. */
	  change_session(sess);
	}

	/* By this stage, we've either exhausted all data in the
	 * signalling buffer (but there might be more of the STR to
	 * follow), or we've exhausted the STR message (so we might
	 * need to go round the loop again). */
	assert(buf_rem(&me->down.buf) == 0 || me->down.payload == 0);
      }
    } else if (me->down.payload < 2) {
      /* There is insufficient data in the payload to make any
	 use of this message. */
      me->down.code = mux_MNOP;

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  discarded; insufficient payload\n");
      }
    } else {
      /* We need more data. */
      return false;
    }
    break;
  }
  
  return true;
}

static void clear_after_payload(mux_core *me)
{
  /* If we know the last message type, and we've read the full
     payload, then be ready for a new message aimed at a potentially
     different session. */
  if (me->down.code != mux_MUNK && me->down.payload == 0) {
    me->down.sess = NULL;
    me->down.code = mux_MUNK;
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  incoming message complete\n");
    }
  }
}

// Buffers should be in reading mode on entry and exit.
static void on_data_from_server(void *vp)
{
  if (verbosity > BASE_VERBOSITY + 2) {
    fprintf(stderr, "\n%s(%p):\n", __func__, vp);
  }
  mux_core *me = vp;

  if (me->down.sess) {
    mux_session sess = me->down.sess;
    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  sess %p\n", (void *) sess);
    }

    /* We're reading a payload delivered to a session. */
    assert(me->down.code == mux_MSTR);

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  adding upto %zu bytes to %zu payload:\n",
	      me->down.payload, buf_rem(&sess->down.buf));
      hex_block(stderr, "  ", &sess->down.buf, -1);
    }

    /* Read as much as we can into the current session's buffer, upto
       the outstanding payload length. */
    buf_slide(&sess->down.buf);
    unsigned char *base = buf_base(&sess->down.buf);
    buf_limit(&sess->down.buf, me->down.payload);
    assert(buf_rem(&sess->down.buf) > 0);
    ssize_t got = buf_read(me->down.fd, &sess->down.buf, -1);
    buf_flip(&sess->down.buf);

    /* Detect and record EOF/error. */
    if (got <= 0) {
      me->down.eof = true;
      me->down.error = got == 0 ? 0 : errno;

      if (me->down.error != 0) {
	if (verbosity > BASE_VERBOSITY + 1) {
	  fprintf(stderr, "  error: %d %s\n",
		  me->down.error, strerror(me->down.error));
	}
      }

      /* Give all sessions a chance to do something with this
	 problem. */
      change_all_sessions(me);
    } else {
      /* We did not get an error.  Record that some of the payload is
       * accounted for. */
      assert((size_t) got <= me->down.payload);
      me->down.payload -= got;

      if (verbosity > BASE_VERBOSITY + 0) {
	{
	  struct buf tmp = {
	    .base = base,
	    .pos = 0,
	    .lim = got,
	    .max = got,
	  };
	  fprintf(stderr, "  <- STR(%u)... %zd + %zu %p %p\n", 
		  (unsigned) sess->our_sid, got, me->down.payload,
		  (void *) sess, sess->user);
	  hex_block(stderr, "  ", &tmp, -1);
	}
      }

      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  received %zd; now %zu payload bytes:\n",
		got, buf_rem(&sess->down.buf));
	hex_block(stderr, "  ", &sess->down.buf, -1);
	fprintf(stderr, "  %zu remaining\n", me->down.payload);
      }

      /* Give the session an opportunity to do something with it. */
      assert(buf_rem(&sess->down.buf) > 0);
      change_session(sess);
    }

    /* If we know the last message type, and we've read the full
       payload, then be ready for a new message aimed at a potentially
       different session. */
    assert(me->down.code == mux_MSTR);
    if (me->down.payload == 0) {
      me->down.sess = NULL;
      me->down.code = mux_MUNK;
      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  incoming STR message complete\n");
      }
    }
  } else {
    /* We're reading a header. */

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  adding to %zu header:\n",
	      buf_rem(&me->down.buf));
      hex_block(stderr, "  ", &me->down.buf, -1);
    }
    /* We're reading into our signalling buffer. */
    buf_slide(&me->down.buf);

    if (verbosity > BASE_VERBOSITY + 2) {
      fprintf(stderr, "  permitting %zu bytes\n", buf_rem(&me->down.buf));
    }
    assert(buf_rem(&me->down.buf) > 0);
    ssize_t got = buf_read(me->down.fd, &me->down.buf, -1);
    buf_flip(&me->down.buf);

    /* Detect and record EOF/error. */
    if (got <= 0) {
      me->down.eof = true;
      me->down.error = got == 0 ? 0 : errno;

      if (me->down.error != 0) {
	if (verbosity > BASE_VERBOSITY + 1) {
	  fprintf(stderr, "  error: %d %s\n",
		  me->down.error, strerror(me->down.error));
	}
      }

      /* Give all sessions a chance to do something with this
	 problem. */
      change_all_sessions(me);
    } else {
      /* Consume as much of the signalling buffer as we can. */
      while (consume_signalling_from_server(me))
	clear_after_payload(me);
      clear_after_payload(me);
      if (verbosity > BASE_VERBOSITY + 2) {
	fprintf(stderr, "  exhausted signalling buffer\n");
      }
    }
  }

  if (server_okay(me) && !me->down.eof) {
    /* We've had no errors from the server, nor has it closed its
       output stream.  Reschedule the read event. */
    prime_down_read(me);
  }

  flush_changed_sessions(me);
  if (verbosity > BASE_VERBOSITY + 0) {
    display_pipe_status(me);
  }
}

static size_t sid_hash(void *ign, htab_const key)
{
  return key.unsigned_integer;
}

static int sid_cmp(void *ign, htab_const sought, htab_const extant)
{
  return sought.unsigned_integer != extant.unsigned_integer;
}

void mux_term(mux_core *me)
{
  struct mux_session_tag *sess;
  while ((sess = dllist_first(&me->all_sessions)))
    destroy_session(sess);
  htab_close(me->chptrs);
  me->chptrs = NULL;

  if (me->init_event != react_ERROR)
    react_close(me->init_event);
  me->init_event = react_ERROR;

  if (me->down.event != react_ERROR)
    react_close(me->down.event);
  me->down.event = react_ERROR;

  if (me->up.event != react_ERROR)
    react_close(me->up.event);
  me->up.event = react_ERROR;
}

static int prime_init_action(mux_core *me)
{
  return react_prime(me->init_event, react_CIDLE, NULL);
}

static int ensure_init_action(mux_core *me)
{
  if (react_isprimed(me->init_event))
    return 0;
  return prime_init_action(me);
}

static void init_action(void *vp)
{
  mux_core *me = vp;

  flush_changed_sessions(me);
  if (verbosity > BASE_VERBOSITY + 0) {
    display_pipe_status(me);
  }
}

int mux_init(mux_core *me, react_core core, int in_fd, int out_fd)
{
  if (in_fd < 0) return -1;
  if (out_fd < 0) return -1;

  /* Set everything to safe values first. */
  me->chptrs = NULL;
  me->ready = false;
  me->next_free_sid = 20;

  dllist_init(&me->all_sessions);
  dllist_init(&me->writing_sessions);
  dllist_init(&me->changed_sessions);
  me->core = core;
  me->init_event = react_ERROR;

  me->inbufsize = 1024;
  me->outbufsize = 1024;

  me->up.event = react_ERROR;
  me->up.fd = out_fd;
  me->up.error = 0;
  me->up.payload = 0;
  buf_init(&me->up.buf, me->up.space, sizeof me->up.space);
  buf_flip(&me->up.buf);

  me->down.event = react_ERROR;
  me->down.fd = in_fd;
  me->down.eof = false;
  me->down.error = 0;
  me->down.code = mux_MUNK;
  me->down.sess = NULL;
  me->down.payload = 0;
  buf_init(&me->down.buf, me->down.space, sizeof me->down.space);
  buf_flip(&me->down.buf);

  /* Create a hashtable mapping session ids to session structures. */
  me->chptrs = htab_open(31, me, &sid_hash, &sid_cmp, NULL, NULL, NULL, NULL);
  if (!me->chptrs)
    goto failure;

  /* Create an initializing event. */
  {
    me->init_event = react_open(me->core);
    if (me->init_event == react_ERROR)
      goto failure;
    react_direct(me->init_event, &init_action, me);
  }

  /* Create and prime an event to read from the server. */
  {
    me->down.event = react_open(me->core);
    if (me->down.event == react_ERROR)
      goto failure;
    react_direct(me->down.event, &on_data_from_server, me);
    if (prime_down_read(me) < 0)
      goto failure;
  }

  /* Create an event to write to the server, but don't prime it. */
  {
    me->up.event = react_open(me->core);
    if (me->up.event == react_ERROR)
      goto failure;
    react_direct(me->up.event, &on_server_ready_for_data, me);
#if 0
    if (prime_up_write(me) < 0)
      goto failure;
#endif
  }

  return 0;

failure:
  mux_term(me);
  return -1;
}

mux_session mux_accept(mux_core *me, void *user,
		       mux_xfer_func send, mux_xfer_func recv,
		       const void *meta, size_t metalen)
{
  if (!mux_working(me)) return NULL;

  if (!mux_ready(me)) return NULL;

  if (metalen > me->meta_lim) return NULL;

  const size_t outbufsize = me->outbufsize;
  const size_t inbufsize = me->inbufsize;
  mux_session sess = malloc(sizeof *sess + outbufsize + inbufsize);
  if (!sess) return NULL;
  void *outbufstart = sess + 1;
  void *inbufstart = (char *) outbufstart + outbufsize;

  *sess = (struct mux_session_tag) {
    .owner = me,
    .writing = false,
    .connected = false,
    .acked = false,
    .changed = true,
    .user = user,
    .up = {
      .func = recv,
      .blocked = false,
      .stopped = false,
      .eof = false,
      .sent_eof = false,
      .cleared = 0,
    },
    .down = {
      .func = send,
      .blocked = false,
      .stopped = false,
      .eof = false,
      .sent_eof = false,
      .cleared = outbufsize,
    },
  };

  buf_init(&sess->meta, (void *) meta, metalen);

  /* Choose a free session id. */
  htab_const key;
  do {
    key.unsigned_integer = (me->next_free_sid++ & 0xffffu);
    me->next_free_sid &= 0xffffu;
  } while (htab_tst(me->chptrs, key));

  if (ensure_init_action(me) < 0) {
    free(sess);
    return NULL;
  }

  /* Map the session id to our structure. */
  sess->our_sid = key.unsigned_integer;
  htab_const val;
  val.pointer = sess;
  if (!htab_put(me->chptrs, key, val)) {
    free(sess);
    return NULL;
  }

  buf_init(&sess->down.buf, inbufstart, inbufsize);
  buf_flip(&sess->down.buf);
  buf_init(&sess->up.buf, outbufstart, outbufsize);
  buf_flip(&sess->up.buf);

  dllist_append(&me->all_sessions, in_all, sess);
  assert(sess->changed);
  dllist_append(&me->changed_sessions, in_changed, sess);

  if (verbosity > BASE_VERBOSITY + 1) {
    fprintf(stderr, "  ++ %p %p\n", (void *) sess, user);
  }
  return sess;
}

void mux_sendokay(mux_session sess)
{
  sess->down.blocked = false;
  change_session(sess);
  mux_core *me = sess->owner;
  flush_changed_sessions(me);
  if (verbosity > BASE_VERBOSITY + 0) {
    display_pipe_status(me);
  }
}

void mux_recvokay(mux_session sess)
{
  sess->up.blocked = false;
  change_session(sess);
  mux_core *me = sess->owner;
  flush_changed_sessions(me);
  if (verbosity > BASE_VERBOSITY + 0) {
    display_pipe_status(me);
  }
}

_Bool mux_working(mux_core *me)
{
  if (dllist_first(&me->all_sessions)) return true;
  return server_okay(me);
}

void mux_dump(mux_core *me, FILE *out)
{
  fprintf(out, "mux core @ %p:%s\n", (void *) me, me->ready ? " ready" : "");
  fprintf(out, "  upstream: fd=%d, error=%d/%s\n",
	  me->up.fd, me->up.error, strerror(me->up.error));
  fprintf(out, "    event:%s%s\n",
	  react_isprimed(me->up.event) ? " primed" : "",
	  react_istriggered(me->up.event) ? " triggered" : "");
  fprintf(out, "    payload=%zu; buf:\n", me->up.payload);
  hex_block(out, "    ", &me->up.buf, -1);
  fprintf(out, "  downstream: fd=%d%s, error=%d/%s\n", me->down.fd,
	  me->down.eof ? ", eof" : "", me->down.error,
	  strerror(me->down.error));
  fprintf(out, "    event:%s%s\n",
	  react_isprimed(me->down.event) ? " primed" : "",
	  react_istriggered(me->down.event) ? " triggered" : "");
  fprintf(out, "    message: %u/%s (sid %u)\n", (unsigned) me->down.code,
	  mux_getmsgname(me->down.code), (unsigned) me->down.sid);
  fprintf(out, "    payload=%zu; buf:\n", me->down.payload);
  hex_block(out, "    ", &me->down.buf, -1);
  fprintf(out, "  sessions:\n");
  for (mux_session sess = dllist_first(&me->all_sessions);
       sess != NULL; sess = dllist_next(in_all, sess)) {
    fprintf(out, "    %p%s\n", (void *) sess,
	    sess == me->down.sess ? " current downstream" : "");
    fprintf(out, "      flags:%s%s%s%s\n",
	    sess->connected ? " connected" : "",
	    sess->acked ? " acked" : "",
	    sess->writing ? " writing" : "",
	    sess->changed ? " changed" : "");
    fprintf(out, "      our sid: %u; server sid: %u\n",
	    (unsigned) sess->our_sid, (unsigned) sess->server_sid);
    display_dir_status("     up  ", &sess->up);
    putc('\n', out);
    display_dir_status("     down", &sess->down);
    putc('\n', out);
  }
}


/* Inline definitions */
extern _Bool mux_ready(mux_core *me);
extern size_t mux_metalim(mux_core *me);
extern void mux_setbufsize(mux_core *me, size_t sz);
