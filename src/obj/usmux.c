#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/select.h>
#include <sys/wait.h>

#include <unistd.h>
#include <fcntl.h>

#include <react/core.h>
#include <react/event.h>
#include <react/signal.h>

#include "buf.h"
#include "options.h"
#include "command.h"
#include "multipiper.h"

static volatile sig_atomic_t process_told_to_die;

static void on_death_instr(int sn)
{
  process_told_to_die = 1;
}

static volatile sig_atomic_t process_interrupted;

static void on_interrupt(int sn)
{
  process_interrupted = 1;
}

static volatile sig_atomic_t wait_for_child;

static void on_child_death(int sn)
{
  wait_for_child = 1;
}

int main(int argc, const char *const *argv)
{
  /* Declare things needed to tidy up on error.  Initialize them to
     safe values. */
  char tmpdirname[FILENAME_MAX];
  bool gottmpdir = false;
  struct method *methods = NULL;
  struct options opts;
  options_init(&opts);
  int exit_code = EXIT_FAILURE;
  react_core_t eventcore = react_COREERROR;
  react_t intr_event = react_ERROR;
  int newargc;
  char **newargs = NULL;
  pid_t server_pid = -1;

  /* Parse the command line. */
  if (options_parse(&opts, argv[0], argc - 1, argv + 1))
    goto tidy_up;
  if (opts.cmd_start < 0 || opts.actions.size == 0 || opts.no_exec) {
    options_usage(stderr, argv[0]);
    goto exit_okay;
  }

  /* We passed only part of the command line for parsing, so adjust
     the command start accordingly. */
  opts.cmd_start++;

  /* Disable SIGPIPE.  What's it for anyway? */
  {
    struct sigaction action;
    action.sa_handler = SIG_IGN;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    if (sigaction(SIGPIPE, &action, NULL) < 0) {
      fprintf(stderr, "%s: disabling SIGPIPE: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  /* Block SIGCHLD, which indicates the death of the server
     process, plus other signals. */
  {
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGCHLD);
    sigaddset(&sigset, SIGINT);
    sigaddset(&sigset, SIGTERM);
    if (sigprocmask(SIG_BLOCK, &sigset, NULL) < 0) {
      fprintf(stderr, "%s: blocking SIGCHLD/SIGINT/SIGTERM: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  /* Create a temporary directory. */
  if (snprintf(tmpdirname, sizeof tmpdirname,
	       "%s/usmux-XXXXXX", opts.tmproot) < 0) {
    fprintf(stderr, "%s: could name temporary directory: %s/usmux-XXXXXX",
	    argv[0], opts.tmproot);
    goto tidy_up;
  }
  if (mkdtemp(tmpdirname) == NULL) {
    fprintf(stderr, "%s: could not create temporary directory %s: %s\n",
	    argv[0], tmpdirname, strerror(errno));
    goto tidy_up;
  }
  gottmpdir = true;

  /* Create a reactor. */
  eventcore = react_opencore(0);
  if (eventcore == react_COREERROR) {
    fprintf(stderr, "%s: failed creation of event core: %s\n",
	    argv[0], strerror(errno));
    goto tidy_up;
  }

  /* Create and prime an event to prevent the core from returning
     EINTR. */
  intr_event = react_open(eventcore);
  if (intr_event == react_ERROR) {
    fprintf(stderr, "%s: failed creation of interrupt event: %s\n",
	    argv[0], strerror(errno));
    goto tidy_up;
  }
  if (react_prime_intr(intr_event) < 0) {
    fprintf(stderr, "%s: failed to prime interrupt event: %s\n",
	    argv[0], strerror(errno));
    goto tidy_up;
  }

  /* Create a method for each action. */
  methods = malloc(sizeof *methods * opts.actions.size);
  if (methods == NULL) {
    fprintf(stderr, "%s: failed allocation of %u rendezvous: %s\n",
	    argv[0], opts.actions.size, strerror(errno));
    goto tidy_up;
  } else {
    struct utils utils = {
      .core = eventcore,
      .tmpdir = tmpdirname,
    };
    for (unsigned i = 0; i < opts.actions.size; i++) {
      const action_config *conf = &opts.actions.base[i];
      int rc = (*conf->constr)(&methods[i], &utils, &opts.shared, &conf->spec);
      if (rc < 0) {
	fprintf(stderr, "%s: can't create rendezvous %u: %s\n",
		argv[0], i, strerror(errno));
	remove(tmpdirname);
	return EXIT_FAILURE;
      }
    }
  }

  /* Build up the server command. */
  {
    newargc = argc - opts.cmd_start;
    if (opts.shared.verbosity > 2) {
      for (int i = 0; i < newargc; i++)
	fprintf(stderr, "orig arg[%d] = %s\n", i, argv[i + opts.cmd_start]);
    }

    unsigned used = 0;
    newargs = make_new_args(newargc, argv + opts.cmd_start,
			    methods, opts.actions.size, &used);

    if (!newargs) {
      fprintf(stderr, "%s: could not build command: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }

    if (opts.shared.verbosity > 1) {
      for (int i = 0; i < newargc; i++)
	fprintf(stderr, "arg[%d] = %s\n", i, newargs[i]);
    }

    if (!used) {
      fprintf(stderr, "%s: insufficient %%s\n", argv[0]);
      goto tidy_up;
    }
  }

  /* Set a signal handler to detect the server dying. */
  {
    struct sigaction action;
    action.sa_handler = &on_child_death;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    if (sigaction(SIGCHLD, &action, NULL) < 0) {
      fprintf(stderr, "%s: preparing for SIGCHLD: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  /* Set a signal handler to tell us when the user wants the server to
     stop. */
  {
    struct sigaction action;
    action.sa_handler = &on_interrupt;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    if (sigaction(SIGINT, &action, NULL) < 0) {
      fprintf(stderr, "%s: preparing for SIGINT: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  /* Set a signal handler to tell us when someone kills the server. */
  {
    struct sigaction action;
    action.sa_handler = &on_death_instr;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    if (sigaction(SIGTERM, &action, NULL) < 0) {
      fprintf(stderr, "%s: preparing for SIGTERM: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  /* Fork to execute the server. */
  {
    server_pid = fork();
    if (server_pid == 0) {
      /* We are the server process. */

      /* Detach from streams copied from the command process. */
      for (unsigned i = 0; i < opts.actions.size; i++)
	method_serverdetach(&methods[i]);

      /* Detach from standard streams. */
      if (opts.close_streams) {
	fclose(stdout);
	fclose(stderr);
	fclose(stdin);
      }

      int rc = exec_server(opts.cmdname, newargs);
      assert(rc == -1);
      if (!opts.close_streams)
	fprintf(stderr, "%s: could not execute server: %s\n",
		argv[0], strerror(errno));
      /* Just abort.  We are in the server process, which is not
	 responsible for tidying up resources. */
      abort();
    }
    /* We are still in the command process. */

    if (server_pid == -1) {
      /* Execution of the server process failed. */
      fprintf(stderr, "%s: could not fork server: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
    /* Execution of the server process succeeded. */

    if (opts.shared.verbosity >= 0)
      fprintf(stderr, "Child process forked (pid %lu)\n",
	      (unsigned long) server_pid);
  }
  /* We are in the command process, with the daemon still to fork. */

  /* Tell all methods to report when they have received an
     acknowledgement from the server, then wait for those reports. */
  unsigned waiting_methods = opts.actions.size;
  bool wait_error = false;
  for (unsigned i = 0; i < opts.actions.size; i++)
    method_await(&methods[i], &waiting_methods, &wait_error);
  if (opts.shared.verbosity >= 1)
    fprintf(stderr, "Awaiting confirmation from %u methods\n", waiting_methods);
  while (waiting_methods > 0 && !wait_error) {
    if (opts.shared.verbosity >= 2)
      fprintf(stderr, "Remaining: %u\n", waiting_methods);
    wait_for_child = 0;
    process_told_to_die = 0;
    process_interrupted = 0;
    if (react_yield(eventcore) < 0) {
      assert(errno != EINTR);
      fprintf(stderr, "%s: reactor failure waiting on readiness: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }

    /* Pass some signals on to server. */
    if (process_told_to_die)
      kill(server_pid, SIGTERM);
    if (process_interrupted)
      kill(server_pid, SIGINT);

    if (wait_for_child) {
      pid_t child = wait(&exit_code);
      if (WIFEXITED(exit_code)) {
	if (opts.shared.verbosity >= 0)
	  fprintf(stderr, "Exit code %d for pid %d\n",
		  WEXITSTATUS(exit_code), (int) child);
      } else if (WIFSIGNALED(exit_code)) {
	if (opts.shared.verbosity >= 0)
	  fprintf(stderr, "Signal %d (%s) for pid %d\n",
		  WTERMSIG(exit_code),
		  strsignal(WTERMSIG(exit_code)), (int) child);
      }
      assert(child == server_pid);
      goto tidy_up;
    }

    if (opts.shared.verbosity >= 2)
      fprintf(stderr, "Yielded\n");
  }
  if (wait_error)
    goto tidy_up;

  if (opts.shared.verbosity > 1)
    fprintf(stderr, "Server ready...\n");

  if (opts.pidfile != NULL) {
    FILE *fp = fopen(opts.pidfile, "w");
    if (fp) {
      fprintf(fp, "%u\n", (unsigned) server_pid);
      fclose(fp);
    } else {
      fprintf(stderr, "%s: warning: could not open pidfile %s: %s\n",
	      argv[0], opts.pidfile, strerror(errno));
    }
  }

  /* Unless disabled, fork into the background. */
  pid_t daemon_pid;
  if (opts.await_end) {
    daemon_pid = 0;
    if (opts.shared.verbosity >= 1)
      fprintf(stderr, "Remaining in foreground...\n");
  } else {
    if (opts.shared.verbosity >= 1)
      fprintf(stderr, "Forking into background...\n");
    daemon_pid = fork();
    if (daemon_pid < 0) {
      fprintf(stderr, "%s: could not fork daemon: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }
  }

  if (daemon_pid > 0) {
    /* We are still the command process. */
    if (opts.shared.verbosity >= 0)
      fprintf(stderr, "Process away...\n");
    /* We are the command process, and the daemon process is running.
       The daemon process is responsible for tidying up, so we just
       exit normally. */
    return EXIT_SUCCESS;
  }
  /* We are in the daemon process. */

  if (!opts.await_end) {
    /* Close streams. */
    close(0);
    close(1);
    close(2);
  }

  /* TODO: Defer listening until this point? */

  /* Start all methods. */
  for (unsigned i = 0; i < opts.actions.size; i++)
    method_activate(&methods[i]);
  unsigned running = opts.actions.size;
  while (true) {
    for (unsigned i = 0; i < opts.actions.size; i++) {
      if (method_isnull(&methods[i])) continue;

      int rc = method_test(&methods[i]);
      if (rc == 0) continue;
      if (rc > 0)
	fprintf(stderr, "%s: rdvz %u reports: %d (%s)\n",
		argv[0], i, rc, strerror(rc));
      else
	fprintf(stderr, "%s: rdvz %u failing\n", argv[0], i);
      method_destroy(&methods[i]);
      running--;
    }
    if (running == 0) break;

    wait_for_child = 0;
    process_told_to_die = 0;
    process_interrupted = 0;
    if (react_yield(eventcore) < 0) {
      assert(errno != EINTR);
      fprintf(stderr, "%s: reactor failure: %s\n",
	      argv[0], strerror(errno));
      goto tidy_up;
    }

    /* Pass some signals on to server. */
    if (process_told_to_die)
      kill(server_pid, SIGTERM);
    if (process_interrupted)
      kill(server_pid, SIGINT);

    if (wait_for_child) {
      pid_t child = wait(&exit_code);
      if (WIFEXITED(exit_code)) {
	fprintf(stderr, "Exit code %d for pid %d\n",
		WEXITSTATUS(exit_code), (int) child);
	if (WEXITSTATUS(exit_code) >= 128)
	  fprintf(stderr, "Signal %d (%s)\n",
		  WEXITSTATUS(exit_code) - 128,
		  strsignal(WEXITSTATUS(exit_code) - 128));
      } else if (WIFSIGNALED(exit_code)) {
	fprintf(stderr, "Signal %d (%s) for pid %d\n",
		WTERMSIG(exit_code),
		strsignal(WTERMSIG(exit_code)), (int) child);
      }
      assert(child == server_pid);
      break;
    }
  }

  if (opts.shared.verbosity > 0)
    fprintf(stderr, "Daemon terminating with %d\n", exit_code);
  goto tidy_up;

 exit_okay:
  exit_code = EXIT_SUCCESS;
 tidy_up:
  if (opts.pidfile != NULL)
    remove(opts.pidfile);
  if (server_pid != -1)
    kill(server_pid, SIGTERM);
  if (methods != NULL) {
    for (unsigned i = 0; i < opts.actions.size; i++)
      method_destroy(&methods[i]);
    free(methods);
  }
  if (newargs) {
    for (int i = 0; i < newargc; i++)
      free(newargs[i]);
    free(newargs);
  }
  if (gottmpdir)
    remove(tmpdirname);
  if (intr_event != react_ERROR)
    react_close(intr_event);
  if (eventcore != react_COREERROR)
    react_closecore(eventcore);
  return exit_code;
}
