#ifndef command_included
#define command_included

#include <stdbool.h>

#include "method.h"

char **make_new_args(int argc, const char *const *argv,
		     const struct method *meth, unsigned nmeth,
		     unsigned *used);

int exec_server(const char *cmdname, char **argv);

#endif
