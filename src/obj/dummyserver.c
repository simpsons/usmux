// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "mux_types.h"
#include "mux_proto.h"
#include "buf.h"

int main(int argc, const char *const *argv)
{
  char out_space[1024], in_space[1024];
  struct buf outbuf, inbuf;

  buf_init(&outbuf, out_space, sizeof out_space);
  buf_flip(&outbuf);
  buf_init(&inbuf, in_space, sizeof in_space);
  buf_flip(&inbuf);

  size_t payload = 0;
  for ( ; ; ) {
    if ((payload == 0 && buf_rem(&inbuf) < 4) || buf_rem(&inbuf) == 0) {
      buf_slide(&inbuf);
      ssize_t got = buf_read(0, &inbuf, -1);
      buf_flip(&inbuf);
      
      if (got < 0) {
	perror("buf_recv");
	break;
      } else if (got == 0) {
	fprintf(stderr, "End of input\n");
	break;
      } else {
	fprintf(stderr, "recv() = %lu\n", (unsigned long) got);
      }
    }

    if (payload > 0) {
      size_t max = payload;
      if (max > buf_rem(&inbuf))
	max = buf_rem(&inbuf);

      unsigned char *ptr = buf_base(&inbuf);
      for (size_t i = 0; i < max; i++) {
	if (i % 16 == 0)
	  fprintf(stderr, "\nPayload:");
	fprintf(stderr, " %02x", ptr[i]);
      }
      buf_skip(&inbuf, max);
      payload -= max;
      fprintf(stderr, "\n  Remaining = %zu\n", payload);
    } else if (buf_rem(&inbuf) >= 4) {
      mux_msgtype type = buf_getu16(&inbuf);
      payload = buf_getu16(&inbuf);

      const char *name = mux_getmsgname(type);
      fprintf(stderr, "Received %s %zu bytes\n",
	      name ? name : "unknown", payload);
    }
  }
  return EXIT_SUCCESS;
}
