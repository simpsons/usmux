// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "buf.h"

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

extern void buf_init(struct buf *b, void *base, size_t max);
extern void buf_clear(struct buf *b);
extern void buf_flip(struct buf *b);
extern void buf_rewind(struct buf *b);
extern size_t buf_rem(const struct buf *b);
extern void *buf_base(const struct buf *b);

static inline void advance(struct buf *b, size_t n)
{
  assert(b->pos <= b->lim);
  assert(n <= b->lim - b->pos);
  b->pos += n;
}

ssize_t buf_send(int fd, struct buf *b, size_t max, int flags)
{
  if (max > buf_rem(b))
    max = buf_rem(b);
  ssize_t done = send(fd, buf_base(b), max, flags);
  if (done > 0) advance(b, done);
  return done;
}

ssize_t buf_write(int fd, struct buf *b, size_t max)
{
  if (max > buf_rem(b))
    max = buf_rem(b);
  ssize_t done = write(fd, buf_base(b), max);
  if (done > 0) advance(b, done);
  return done;
}

ssize_t buf_recv(int fd, struct buf *b, size_t max, int flags)
{
  if (max > buf_rem(b))
    max = buf_rem(b);
  ssize_t done = recv(fd, buf_base(b), max, flags);
  if (done > 0) advance(b, done);
  return done;
}

ssize_t buf_read(int fd, struct buf *b, size_t max)
{
  if (max > buf_rem(b))
    max = buf_rem(b);
  ssize_t done = read(fd, buf_base(b), max);
  fflush(stderr);
  if (done > 0) advance(b, done);
  return done;
}

void buf_slide(struct buf *b)
{
  memmove(b->base, buf_base(b), buf_rem(b));
  b->pos = b->lim - b->pos;
  b->lim = b->max;
}


uint_fast16_t buf_getu16(struct buf *b)
{
  assert(buf_rem(b) >= 2);
  unsigned char *ptr = buf_base(b);
  uint_fast16_t res = 0;
  res |= *ptr++;
  res <<= 8;
  res |= *ptr++;
  advance(b, 2);
  return res;
}

uint_fast32_t buf_getu32(struct buf *b)
{
  assert(buf_rem(b) >= 4);
  unsigned char *ptr = buf_base(b);
  uint_fast16_t res = 0;
  res |= *ptr++;
  res <<= 8;
  res |= *ptr++;
  res <<= 8;
  res |= *ptr++;
  res <<= 8;
  res |= *ptr++;
  advance(b, 4);
  return res;
}

void buf_putu16(struct buf *b, uint_fast16_t val)
{
  assert(buf_rem(b) >= 2);
  unsigned char *ptr = buf_base(b);
  *ptr++ = (val >> 8) & 0xffu;
  *ptr++ = val & 0xffu;
  advance(b, 2);
}

void buf_putu32(struct buf *b, uint_fast32_t val)
{
  assert(buf_rem(b) >= 4);
  unsigned char *ptr = buf_base(b);
  *ptr++ = (val >> 24) & 0xffu;
  *ptr++ = (val >> 16) & 0xffu;
  *ptr++ = (val >> 8) & 0xffu;
  *ptr++ = val & 0xffu;
  advance(b, 4);
}

void buf_skip(struct buf *b, size_t n)
{
  assert(buf_rem(b) >= n);
  advance(b, n);
}

void buf_limit(struct buf *b, size_t n)
{
  size_t rem = buf_rem(b);
  if (rem > n) {
    size_t new_lim = b->pos + n;;
    assert(new_lim < b->lim);
    b->lim = new_lim;
  }
}

size_t buf_move(struct buf *to, struct buf *from, size_t max)
{
  if (max > buf_rem(to))
    max = buf_rem(to);
  if (max > buf_rem(from))
    max = buf_rem(from);
  memcpy(buf_base(to), buf_base(from), max);
  advance(to, max);
  advance(from, max);
  return max;
}
