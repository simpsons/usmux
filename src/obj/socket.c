#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "options.h"

#if 0
int options_makesocket(struct options *opts)
{
  int sock = socket(PF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("socket");
    return -1;
  }
  if (opts->verbosity > 2) {
    fprintf(stderr, "PF_UNIX socket is %d\n", sock);
  }

  struct sockaddr_un addr;
  memset(&addr, 0, sizeof addr);
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, opts->sockname, sizeof addr.sun_path);

  if (bind(sock, (struct sockaddr *) &addr, sizeof addr) < 0) {
    perror("bind");
    close(sock);
    return -1;
  }

  if (opts->verbosity > 1) {
    fprintf(stderr, "Socket mode %04o\n", opts->sockmode);
  }
  if (chmod(opts->sockname, opts->sockmode) < 0) {
    fprintf(stderr, "%s: chmod %o %s\n", strerror(errno),
	    opts->sockmode, opts->sockname);
    close(sock);
    remove(opts->sockname);
    return -1;
  }

  if (listen(sock, 5) < 0) {
    perror("listen");
    close(sock);
    remove(opts->sockname);
    return -1;
  }

  return sock;
}
#endif
