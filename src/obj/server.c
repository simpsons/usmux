#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include "command.h"

int exec_server(const char *cmdname, char **argv)
{
  char tmpcmdname[128];
  if (cmdname) {
    const char *temp = argv[0];
    strncpy(tmpcmdname, cmdname, sizeof tmpcmdname);
    tmpcmdname[sizeof tmpcmdname - 1] = '\0';
    argv[0] = tmpcmdname;
    cmdname = temp;
  } else {
    cmdname = argv[0];
  }

  return execvp(cmdname, argv);
}
