#include <stddef.h>
#include <errno.h>

#include "method.h"

void method_await(const struct method *p, unsigned *remp, _Bool *flag)
{
  if (p->vtab && p->vtab->await)
    (*p->vtab->await)(p->ctxt, remp, flag);
}

void method_activate(const struct method *p)
{
  if (p->vtab && p->vtab->activate)
    (*p->vtab->activate)(p->ctxt);
}

void method_destroy(struct method *p)
{
  if (p->vtab && p->vtab->destroy)
    (*p->vtab->destroy)(p->ctxt);
  p->vtab = NULL;
  p->ctxt = NULL;
}

int method_isnull(const struct method *p)
{
  return p && p->vtab;
}

int method_test(const struct method *p)
{
  if (p->vtab && p->vtab->test)
    return (*p->vtab->test)(p->ctxt);
  errno = EINVAL;
  return -1;
}

int method_writecode(const struct method *p, char *buf, size_t len)
{
  if (p->vtab && p->vtab->writecode)
    return (*p->vtab->writecode)(p->ctxt, buf, len);
  return 0;
}

void method_serverdetach(const struct method *p)
{
  if (p->vtab && p->vtab->serverdetach)
    (*p->vtab->serverdetach)(p->ctxt);
}
