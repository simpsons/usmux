#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <assert.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include <arpa/inet.h>

#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>

#include <react/types.h>
#include <react/event.h>
#include <react/fd.h>

#include "tcpsock.h"

static unsigned nextid;

struct object {
  unsigned id;
  int verbosity;
  react_core_t core;
  struct addrinfo *addr;
  struct addrinfo **acl;
  unsigned acllen;
  char downname[FILENAME_MAX];
  bool down_made;
  int downfd;
  react_t down_event;
  unsigned *remp;
  bool *failure;
  int term;
};

static void safeclose(int *fp)
{
  close(*fp);
  *fp = -1;
}

static void safecloseevent(react_t *p)
{
  react_close(*p);
  *p = react_ERROR;
}

static int prime_down(struct object *obj)
{
  assert(obj->downfd >= 0);
  assert(obj->down_event != react_ERROR);
  return react_prime_fdin(obj->down_event, obj->downfd);
}

static void on_down(void *vp)
{
  struct object *obj = vp;

  char buf[1];
  int rc = read(obj->downfd, buf, 1);
  if (rc == 0) {
    /* The server has closed the session server. */
    obj->term = -1;
    if (obj->failure)
      *obj->failure = true;
  } else if (rc < 0) {
    obj->term = errno;
    if (obj->failure)
      *obj->failure = true;
  } else {
    if (obj->remp) {
      (*obj->remp)--;
      obj->remp = NULL;
      obj->failure = NULL;
    } else if (prime_down(obj) < 0) {
      obj->term = errno;
    }
  }
}

static void await(void *vp, unsigned *remp, bool *fail)
{
  struct object *obj = vp;
  obj->remp = remp;
  obj->failure = fail;

  if (prime_down(obj) < 0) {
    *obj->failure = true;
    return;
  }
}

static int test(void *vp)
{
  struct object *obj = vp;

  return obj->term;
}

static int writecode(void *vp, char *buf, size_t len)
{
  struct object *obj = vp;
  char tmp[100];
  unsigned port;
  void *host;
  switch (obj->addr->ai_family) {
  case AF_INET: {
    struct sockaddr_in *p = (void *) obj->addr->ai_addr;
    port = ntohs(p->sin_port);
    host = &p->sin_addr;
  } break;

  case AF_INET6: {
    struct sockaddr_in6 *p = (void *) obj->addr->ai_addr;
    port = ntohs(p->sin6_port);
    host = &p->sin6_addr;
  } break;

  default:
    return -1;
  }

  int rc;
  int done = 0;
  rc = snprintf(buf, len, "tcp:%s;%s;%u",
		obj->downname,
		inet_ntop(obj->addr->ai_family, host, tmp, sizeof tmp),
		port);
  if (rc < 0) return rc;
  done += rc;

  for (unsigned i = 0; i < obj->acllen; i++) {
    if (obj->acl[i] == NULL) continue;
    switch (obj->acl[i]->ai_family) {
    case AF_INET: {
      struct sockaddr_in *p = (void *) obj->acl[i]->ai_addr;
      host = &p->sin_addr;
    } break;

    case AF_INET6: {
      struct sockaddr_in6 *p = (void *) obj->acl[i]->ai_addr;
      host = &p->sin6_addr;
    } break;

    default:
      continue;
    }
    inet_ntop(obj->acl[i]->ai_family, host, tmp, sizeof tmp);
    rc = snprintf(buf ? buf + done : NULL, buf ? len - done : 0, ";%s", tmp);
    if (rc < 0) return rc;
    done += rc;
  }

  return done;
}

static void serverdetach(void *vp)
{
  struct object *obj = vp;
  react_cancel(obj->down_event);
  safeclose(&obj->downfd);
}

static void activate(void *vp)
{
  struct object *obj = vp;
  obj->remp = NULL;
  obj->failure = NULL;

  if (prime_down(obj) < 0) {
    obj->term = errno;
    return;
  }
}

static void destroy(void *vp)
{
  struct object *obj = vp;

  if (obj->acl) {
    for (unsigned i = 0; i < obj->acllen; i++)
      freeaddrinfo(obj->acl[i]);
    free(obj->acl), obj->acl = NULL;
  }

  safecloseevent(&obj->down_event);
  safeclose(&obj->downfd);

  if (obj->down_made) {
    assert(*obj->downname);
    remove(obj->downname);
  }
  if (obj->addr)
    freeaddrinfo(obj->addr);
  free(obj);
}

static const struct method_suite vtab = {
  .await = &await,
  .test = &test,
  .activate = &activate,
  .destroy = &destroy,
  .writecode = &writecode,
  .serverdetach = &serverdetach,
};

static const struct addrinfo hints = {
  .ai_socktype = SOCK_STREAM,
  .ai_protocol = IPPROTO_TCP,
};

int tcpsock_constr(struct method *meth,
		   const struct utils *utils,
		   const shared_opts *shared,
		   const void *config)
{
  const struct tcpsock_conf *conf = config;
  struct object *obj = malloc(sizeof *obj);
  if (obj == NULL) goto failure;
  static const struct object null = {
    .core = react_COREERROR,
    .downfd = -1,
    .down_event = react_ERROR,
  };
  *obj = null;

  obj->acl = malloc(sizeof *obj->acl * obj->acllen);
  if (obj->acl == NULL)
    goto dealloc;
  for (unsigned i = 0; i < conf->acllen; i++)
    obj->acl[i] = NULL;
  obj->acllen = conf->acllen;
  for (unsigned i = 0; i < conf->acllen; i++)
    if (getaddrinfo(conf->acl[i], NULL, &hints, &obj->acl[i]) < 0)
      goto dealloc;

  obj->core = utils->core;
  obj->verbosity = shared->verbosity;
  obj->id = nextid++;
  obj->down_event = react_open(obj->core);
  if (obj->down_event == react_ERROR)
    goto dealloc;
  react_direct(obj->down_event, &on_down, obj);
  if (getaddrinfo(conf->node, conf->svc, &hints, &obj->addr) < 0)
    goto dealloc;
  switch (obj->addr->ai_family) {
  case AF_INET: {
    assert(obj->addr->ai_addrlen == sizeof(struct sockaddr_in));
    const struct sockaddr_in *p = (void *) obj->addr->ai_addr;
    unsigned port = ntohs(p->sin_port);
    fprintf(stderr, "IPv4 port=%u\n", port);
    break;
  }

  case AF_INET6: {
    assert(obj->addr->ai_addrlen == sizeof(struct sockaddr_in6));
    const struct sockaddr_in6 *p = (void *) obj->addr->ai_addr;
    unsigned port = ntohs(p->sin6_port);
    fprintf(stderr, "IPv6 port=%u\n", port);
    break;
  }

  default:
    fprintf(stderr, "unknown af\n");
    goto dealloc;
  }


  if (snprintf(obj->downname, sizeof obj->downname,
	       "%s/tcp%u-cdown", utils->tmpdir, obj->id) < 0)
    goto dealloc;
  if (mkfifo(obj->downname, 0600) < 0)
    goto dealloc;
  obj->down_made = true;
  obj->downfd = open(obj->downname, O_RDONLY | O_NONBLOCK);
  if (obj->downfd < 0) goto failure;

  meth->vtab = &vtab;
  meth->ctxt = obj;
  return 0;

 dealloc:
  assert(obj != NULL);
  destroy(obj);
 failure:
  meth->ctxt = NULL;
  meth->vtab = NULL;
  return -1;
}
