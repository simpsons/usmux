// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef included_mux_h
#define included_mux_h

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <ddslib/dllist.h>
#include <ddslib/htab.h>
#include <react/core.h>
#include <react/event.h>

#include "buf.h"
#include "mux_types.h"

typedef struct mux_session_tag *mux_session;

typedef struct mux_core_tag {
  react_core core;
  htab chptrs;
  _Bool ready;
  mux_sid next_free_sid;
  react_event init_event;
  size_t meta_lim;

  size_t inbufsize, outbufsize;

  struct {
    int fd;
    int error; // error calling send/write(server)
    react_event event;
    size_t payload;
    struct buf buf;
    char space[512];
  } up;

  struct {
    int fd;
    _Bool eof;
    int error; // error calling recv/read(server)
    struct buf buf;
    mux_msgtype code;
    mux_sid sid;
    struct mux_session_tag *sess;
    size_t payload;
    react_event event;
    char space[12];
  } down;

  dllist_hdr(struct mux_session_tag) all_sessions, writing_sessions,
    changed_sessions;
} mux_core;

int mux_init(mux_core *, react_core, int in_fd, int out_fd);
void mux_term(mux_core *);

_Bool mux_working(mux_core *);

/*
 * Create a new session multiplexed over the file descriptors.
 *
 * The mux will call send/recv(user) when it wants to send/receive.
 * If send/recv(user) returns 0 (mux_BLOCK), it will not attempt to
 * call send/receive again until sendokay is called.  If
 * send/recv(user) returns mux_ERROR, an error code will be reported
 * in *ecp.  If recv(user) returns mux_EOF, no more data will be
 * delivered.  send(user, NULL, NULL) will be called when the session
 * is terminated.
 */
mux_session mux_accept(mux_core *, void *user,
		       mux_xfer_func send, mux_xfer_func recv,
		       const void *meta, size_t metalen);
void mux_sendokay(mux_session);
void mux_recvokay(mux_session);

inline _Bool mux_ready(mux_core *me) { return me->ready; }
inline size_t mux_metalim(mux_core *me) { return me->meta_lim; }
inline void mux_setbufsize(mux_core *me, size_t sz) {
  me->inbufsize = me->outbufsize = sz;
}

void mux_dump(mux_core *, FILE *);

#endif
