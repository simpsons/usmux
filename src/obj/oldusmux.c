// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>
#include <errno.h>
#include <assert.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/un.h>

#include <react/core.h>
#include <react/condtype.h>
#include <react/conds.h>
#include <ddslib/dllist.h>

#include "mux.h"

static void hex_block(FILE *out, const struct buf *buf, size_t lim)
{
  if (lim > buf_rem(buf))
    lim = buf_rem(buf);
  const unsigned char *base = buf_base(buf);

  for (size_t i = 0; i < lim; i++)
    fprintf(out, "%s%02X", i % 16 ? " " : "\n   ", base[i]);
  if (lim < buf_rem(buf))
    fprintf(out, "...");
  putc('\n', out);
}

struct acceptor {
  mux_core *mux;
  react_core core;
  react_event event;
  int sock;
  dllist_hdr(struct session) sessions;
  dllist_hdr(struct session) deleted_sessions;
};

struct session {
  struct acceptor *owner;
  dllist_elem(struct session) others;
  react_event read_event;
  react_event write_event;
  bool read_ready, write_ready;
  bool read_wanted, write_wanted;
  bool deleted;
  int sock;
  mux_session mux;
#ifdef _GNU_SOURCE
  struct buf meta;
  unsigned char metadata[10];
#endif
};

int verbosity = 0;

static volatile sig_atomic_t child_death, termed;

static int exit_code = EXIT_SUCCESS;
static _Bool server_reaped;
static mux_core muxcore;

static void flush_deleted_sessions(struct acceptor *me)
{
  for (struct session *sess = dllist_first(&me->deleted_sessions);
       sess != NULL; sess = dllist_first(&me->deleted_sessions)) {
    assert(sess->deleted);
    dllist_unlink(&me->deleted_sessions, others, sess);
    free(sess);
  }
}

static int prime_accept(struct acceptor *me)
{
  react_fdcond cond = {
    .handle = me->sock,
    .mode = react_MIN,
  };
  return react_prime(me->event, react_CFD, &cond);
}

static int prime_read(struct session *sess)
{
  react_fdcond cond = {
    .handle = sess->sock,
    .mode = react_MIN,
  };
  return react_prime(sess->read_event, react_CFD, &cond);
}

static int prime_write(struct session *sess)
{
  react_fdcond cond = {
    .handle = sess->sock,
    .mode = react_MOUT,
  };
  return react_prime(sess->write_event, react_CFD, &cond);
}

static int ensure_read(struct session *sess)
{
  if (sess->deleted) return 0;
  if (sess->read_ready) return 0;

  if (react_isprimed(sess->read_event)) return 0;

  /* Do nothing if we're due to be invoked. */
  if (react_istriggered(sess->read_event)) return 0;

  int rc;
  if ((rc = prime_read(sess)) < 0) {
    perror("prime_read");
    termed = 1;
    exit_code = EXIT_FAILURE;
  }
  return rc;
}

/* Ensure that we either know that we can write to the client, or that
 * we're asking whether we can write to the client. */
static int ensure_write(struct session *sess)
{
  if (sess->deleted) return 0;
  if (sess->write_ready) return 0;

  if (react_isprimed(sess->write_event)) return 0;

  /* Do nothing if we're due to be invoked. */
  if (react_istriggered(sess->write_event)) return 0;

  int rc;
  if ((rc = prime_write(sess)) < 0) {
    perror("prime_write");
    termed = 1;
    exit_code = EXIT_FAILURE;
  }
  return rc;
}

static void on_readable(void *vp)
{
  struct session *sess = vp;
  assert(!sess->deleted);
  struct acceptor *me = sess->owner;

  if (verbosity > 4) {
    fprintf(stderr, "\n%s(%p)%s\n", __func__, vp,
	    sess->read_wanted ? " wanted" : "");
  }

  assert(!sess->read_ready);

  sess->read_ready = true;
  if (sess->read_wanted) {
    assert(sess->read_ready);
    sess->read_wanted = false;
    mux_recvokay(sess->mux);
  }
  if (ensure_read(sess))
    return;
}

static void on_writable(void *vp)
{
  struct session *sess = vp;
  assert(!sess->deleted);
  struct acceptor *me = sess->owner;

  if (verbosity > 4) {
    fprintf(stderr, "\n%s(%p)%s\n", __func__, vp,
	    sess->write_wanted ? " wanted" : "");
  }

  assert(!sess->write_ready);

  sess->write_ready = true;
  if (sess->write_wanted) {
    assert(sess->write_ready);
    sess->write_wanted = false;
    mux_sendokay(sess->mux);
  }
  if (ensure_write(sess))
    return;
}

static mux_rcode try_recv(void *vp, struct buf *buf, int *ecp)
{
  if (verbosity > 4) {
    fprintf(stderr, "\n%s(%p)\n", __func__, vp);
  }

  struct session *sess = vp;
  assert(!sess->deleted);
  struct acceptor *me = sess->owner;

  if (buf == NULL) {
    if (verbosity > 4) {
      fprintf(stderr, "Old session %p\n", (void *) sess);
    }
    if (!sess->deleted) {
      dllist_unlink(&me->sessions, others, sess);
      react_close(sess->read_event), sess->read_event = react_ERROR;
      react_close(sess->write_event), sess->write_event = react_ERROR;
      close(sess->sock), sess->sock = -1;
      sess->deleted = true;
      dllist_append(&me->deleted_sessions, others, sess);
    }
    assert(sess->deleted);
    return mux_BLOCK;
  }

  ssize_t rc = mux_BLOCK;
  if (sess->read_ready) {
    rc = buf_recv(sess->sock, buf, -1, 0);
    sess->read_ready = false;
    if (rc < 0) {
      *ecp = errno;
      if (verbosity > 4)
	fprintf(stderr, "  read error %s\n", strerror(errno));
      return mux_ERROR;
    }
    if (rc == 0) {
      if (verbosity > 4)
	fprintf(stderr, "  EOF\n");
      return mux_EOF;
    }
    if (verbosity > 5)
      fprintf(stderr, "  got %zd bytes\n", rc);
  } else {
    if (verbosity > 5)
      fprintf(stderr, "  banked for later\n");
    sess->read_wanted = true;
  }
  if (ensure_read(sess))
    return mux_ERROR;
  return rc;
}

static mux_rcode try_send(void *vp, struct buf *buf, int *ecp)
{
  if (verbosity > 4) {
    fprintf(stderr, "\n%s(%p)\n", __func__, vp);
  }

  struct session *sess = vp;
  assert(!sess->deleted);
  struct acceptor *me = sess->owner;

  if (buf == NULL) {
    shutdown(sess->sock, SHUT_WR);
    return mux_EOF;
  }

  ssize_t rc = mux_BLOCK;
  if (sess->write_ready) {
    if (verbosity > 5) {
      fprintf(stderr, "Sending to client:");
      hex_block(stderr, buf, -1);
    }
    rc = buf_send(sess->sock, buf, -1, MSG_NOSIGNAL);
    sess->write_ready = false;
    if (rc < 0) {
      *ecp = errno;
      return mux_ERROR;
    }
    assert(rc != 0);
  } else {
    sess->write_wanted = true;
  }
  if (ensure_write(sess))
    return mux_ERROR;
  return rc;
}

static void on_acceptable(void *vp)
{
  if (verbosity > 4)
    fprintf(stderr, "Accepting...\n");

  struct acceptor *me = vp;
  struct sockaddr addr;
  socklen_t addrlen = sizeof addr;
  int cli = accept(me->sock, &addr, &addrlen);
  if (cli < 0) {
    if (verbosity > 4)
      perror("accept");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }
  struct session *sess = malloc(sizeof *sess);
  if (!sess) {
    if (verbosity > 4)
      fprintf(stderr, "session creation failed\n");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }
  if (verbosity > 4)
    fprintf(stderr, "New session %p\n", (void *) sess);
  sess->owner = me;
  dllist_append(&me->sessions, others, sess);
  sess->sock = cli;
  sess->read_ready = sess->write_ready = false;
  sess->read_wanted = sess->write_wanted = false;
  sess->deleted = false;

  void *meta = NULL;
  size_t metalen = 0;
#ifdef _GNU_SOURCE
  struct ucred creds;
  socklen_t creds_len = sizeof creds;
  if (getsockopt(cli, SOL_SOCKET, SO_PEERCRED, &creds, &creds_len) < 0) {
    perror("getsockopt(SO_PEERCRED)");
  } else {
    buf_init(&sess->meta, sess->metadata, sizeof sess->metadata);
    buf_putu32(&sess->meta, 0x554E4958u);
    buf_putu16(&sess->meta, creds.pid);
    buf_putu16(&sess->meta, creds.uid);
    buf_putu16(&sess->meta, creds.gid);
    buf_flip(&sess->meta);
  }
  meta = buf_base(&sess->meta);
  metalen = buf_rem(&sess->meta);
#endif

  sess->read_event = react_open(me->core);
  if (sess->read_event == react_ERROR) {
    if (verbosity > 4)
      fprintf(stderr, "no memory\n");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }
  react_direct(sess->read_event, &on_readable, sess);
  sess->write_event = react_open(me->core);
  if (sess->write_event == react_ERROR) {
    if (verbosity > 4)
      fprintf(stderr, "no memory\n");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }
  react_direct(sess->write_event, &on_writable, sess);

  sess->mux = mux_accept(me->mux, sess, &try_send, &try_recv, meta, metalen);
  if (!sess->mux) {
    if (verbosity > 4)
      fprintf(stderr, "no memory\n");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }

  if (ensure_read(sess))
    return;

  if (ensure_write(sess))
    return;

  if (prime_accept(me) < 0) {
    if (verbosity > 4)
      perror("prime_accept");
    termed = 1;
    exit_code = EXIT_FAILURE;
    return;
  }
}

static void on_broken_pipe(int sig)
{
  signal(SIGPIPE, &on_broken_pipe);
}

static void on_child_death(int sig)
{
  child_death = 1;
  fprintf(stderr, "Detected child death (%d, %s)\n", sig, strsignal(sig));
}

static void on_term(int sig)
{
  termed = 1;
  fprintf(stderr, "Detected termination (%d, %s)\n", sig, strsignal(sig));
}

static char *apply_args(const char *from,
			const char *inname, const char *outname,
			bool *named)
{
  size_t codelen = snprintf(NULL, 0, "pipes:%s;%s", inname, outname);

#if 0
  fprintf(stderr, "orig: %s\n", from);
  fprintf(stderr, "inname: %s\n", inname);
  fprintf(stderr, "outname: %s\n", outname);
  fprintf(stderr, "replacement: pipes:%s;%s (len=%zu) \n",
	  inname, outname, codelen);
#endif

  size_t len = 0;
  for (const char *ptr = from; *ptr; ptr++) {
    if (ptr[0] == '%') {
      if (ptr[1] != 's') {
	len++;
	if (ptr[1])
	  ptr++;
	continue;
      }
      ptr++;
      len += codelen;
    } else {
      len++;
    }
  }

  char *res = malloc(len + 1);
  char *out = res;
  if (!res) return NULL;

  for (const char *ptr = from; *ptr; ptr++) {
    if (ptr[0] == '%') {
      if (ptr[1] != 's') {
	*out++ = *++ptr;
	continue;
      }

      size_t done =
	snprintf(out, len + 1 - (out - res), "pipes:%s;%s", inname, outname);
      assert(done == codelen);
      out += done;
      ptr++;
      *named = true;
    } else {
      *out++ = *ptr;
    }
  }
  *out = '\0';
  return res;
}

char **make_new_args(int argc, const char *const *argv,
		     const char *inname, const char *outname,
		     bool *named)
{
  char **newargs = malloc(sizeof *newargs * (argc + 1));
  if (!newargs) return NULL;
  for (int argi = 0; argi < argc; argi++) {
    newargs[argi] = apply_args(argv[argi], inname, outname, named);
    if (!newargs[argi]) {
      for (int i = 0; i < argi; i++)
	free(newargs[i]);
      free(newargs);
      return NULL;
    }
  }
  newargs[argc] = NULL;
  return newargs;
}

int main(int argc, const char *const *argv)
{
  char inname[L_tmpnam + 20], outname[L_tmpnam + 20];
  const char *sockname = "/tmp/usmux.sock";
  strncpy(inname, tempnam(NULL, "usmux"), sizeof inname);
  strncpy(outname, tempnam(NULL, "usmux"), sizeof outname);
  const char *cmdname = NULL;

  mode_t sockmode = 0600;
  bool await_end = false;
  bool close_streams = true;
  bool no_exec = false;
  size_t bufsize = 1024;

  /* Process command line. */
  int cmd_start = -1;
  for (int argi = 1; argi < argc; argi++) {
    if (argv[argi][0] == '-') {
      if (!strcmp(argv[argi] + 1, "B")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s unixsockname\n", argv[0], argv[argi]);
	  exit(EXIT_FAILURE);
	}
	sockname = argv[++argi];
      } else if (!strcmp(argv[argi] + 1, "d")) {
	await_end = true;
	close_streams = false;
      } else if (!strcmp(argv[argi] + 1, "v")) {
	verbosity++;
      } else if (!strcmp(argv[argi] + 1, "o")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s filemode\n", argv[0], argv[argi]);
	  exit(EXIT_FAILURE);
	}
	sockmode = strtol(argv[++argi], NULL, 8);
      } else if (!strcmp(argv[argi] + 1, "n")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s cmdname\n", argv[0], argv[argi]);
	  exit(EXIT_FAILURE);
	}
	cmdname = argv[++argi];
      } else if (!strcmp(argv[argi] + 1, "z")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s buffer-size\n", argv[0], argv[argi]);
	  exit(EXIT_FAILURE);
	}
	char *end;
	bufsize = strtoull(argv[++argi], &end, 0);
	if (end) {
	  switch (*end) {
	  case 'k':
	  case 'K':
	    bufsize *= 1024;
	    break;

	  case 'm':
	  case 'M':
	    bufsize *= 1024LL * 1024;
	    break;

	  case 'g':
	  case 'G':
	    bufsize *= 1024LL * 1024 * 1024;
	    break;

	  case 't':
	  case 'T':
	    bufsize *= 1024LL * 1024 * 1024 * 1024;
	    break;
	  }
	}
      } else if (!strcmp(argv[argi] + 1, "-")) {
	cmd_start = argi + 1;
	no_exec = false;
	break;
      } else {
	fprintf(stderr, "%s: unknown switch %s\n", argv[0], argv[argi]);
	exit(EXIT_FAILURE);
      }
    } else if (argv[argi][0] == '+') {
      if (!strcmp(argv[argi] + 1, "v")) {
	verbosity--;
      } else if (!strcmp(argv[argi] + 1, "-")) {
	no_exec = true;
      } else {
	fprintf(stderr, "%s: unknown switch %s\n", argv[0], argv[argi]);
	exit(EXIT_FAILURE);
      }
    }
  }

  if (cmd_start < 0 && !no_exec) {
    fprintf(stderr, "usage: %s [options] -- command\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  /* Build up the server command. */
  int newargc;
  char **newargs = NULL;
  if (!no_exec) {
    newargc = argc - cmd_start;
    if (verbosity > 2) {
      for (int i = 0; i < newargc; i++)
	fprintf(stderr, "orig arg[%d] = %s\n", i, argv[i + cmd_start]);
    }

    bool named = false;
    newargs = make_new_args(newargc, argv + cmd_start,
			    inname, outname, &named);

    if (!newargs) {
      fprintf(stderr, "%s: out of memory\n", argv[0]);
      exit(EXIT_FAILURE);
    }

    if (verbosity > 1) {
      for (int i = 0; i < newargc; i++)
	fprintf(stderr, "arg[%d] = %s\n", i, newargs[i]);
    }

    if (!named) {
      fprintf(stderr, "%s: %%s missing from command\n", argv[0]);
      exit(EXIT_FAILURE);
    }
  }

  /* Create a reactor. */
  react_core eventcore = react_opencore(1);
  if (eventcore == react_COREERROR) {
    perror("eventcore");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGPIPE, &on_broken_pipe) == SIG_ERR) {
    perror("signal PIPE");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGINT, &on_term) == SIG_ERR) {
    perror("signal INT");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGTERM, &on_term) == SIG_ERR) {
    perror("signal TERM");
    exit(EXIT_FAILURE);
  }

  /* Create a UNIX-domain socket.  This also serves as our test to see
   * if the server is already running. */
  int sock = socket(PF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }
  if (verbosity > 2) {
    fprintf(stderr, "PF_UNIX socket is %d\n", sock);
  }

  struct sockaddr_un addr;
  memset(&addr, 0, sizeof addr);
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, sockname, sizeof addr.sun_path);

  if (bind(sock, (struct sockaddr *) &addr, sizeof addr) < 0) {
    perror("bind");
    exit(EXIT_FAILURE);
  }

  if (verbosity > 1) {
    fprintf(stderr, "Socket mode %04o\n", sockmode);
  }
  if (chmod(sockname, sockmode) < 0) {
    perror("fchmod");
    exit(EXIT_FAILURE);
  }

  if (listen(sock, 5) < 0) {
    perror("listen");
    exit(EXIT_FAILURE);
  }

  /* Create named pipes to talk to the server. */
  if (mkfifo(inname, 0600) < 0) {
    perror("mkfifo in");
    remove(sockname);
    exit(EXIT_FAILURE);
  }
  if (mkfifo(outname, 0600) < 0) {
    perror("mkfifo out");
    remove(inname);
    remove(sockname);
    exit(EXIT_FAILURE);
  }

  if (no_exec && verbosity > 0) {
    fprintf(stderr, "pipes:%s;%s\n", inname, outname);
  }

  /* Prepare to reap the server process. */
  if (signal(SIGCHLD, &on_child_death) == SIG_ERR) {
    perror("signal CHLD");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }

  /* Fork to execute the server. */
  pid_t server_pid;

  if (!no_exec) {
    server_pid = fork();
    if (server_pid == 0) {
      /* We are the server process. */

      /* Detach from standard streams. */
      close(sock);
      if (close_streams) {
	fclose(stdout);
	fclose(stderr);
	fclose(stdin);
      }

      /* Execute the server. */ 
      char tmpcmdname[128];
      if (cmdname) {
	const char *temp = newargs[0];
	strncpy(tmpcmdname, cmdname, sizeof tmpcmdname);
	tmpcmdname[sizeof tmpcmdname - 1] = '\0';
	newargs[0] = tmpcmdname;
	cmdname = temp;
      } else {
	cmdname = newargs[0];
      }
      if (execvp(cmdname, newargs) < 0)
	exit(EXIT_FAILURE);
    }
    /* We are the command process. */

    if (server_pid == -1) {
      perror("fork server");
      remove(sockname);
      remove(inname);
      remove(outname);
      exit(EXIT_FAILURE);
    }
    if (verbosity >= 0)
      fprintf(stderr, "Child process forked (pid %lu)\n",
	      (unsigned long) server_pid);
  }

  /* Open the two named pipes. */

  /* We must open our input stream non-blocking, or we block until the
   * peer opens the fifo. */
  int fin = open(outname, O_RDONLY | O_NONBLOCK);
  if (fin < 0)
    perror("open(in)");
  if (verbosity > 2)
    fprintf(stderr, "input fd = %d on %s\n", fin, outname);

  /* We must open our output stream non-blocking and for both read and
   * write, otherwise we block until the peer opens the fifo, or we
   * fail immediately with ENXIO. */
  int fout = open(inname, O_RDWR | O_NONBLOCK);
  /* TODO: Allow the write fd to be supplied to the mux after the mux
   * has been initialized. */
  if (fout < 0)
    perror("open(out)");

  if (fin < 0 || fout < 0) {
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }
  if (verbosity > 1)
    fprintf(stderr, "Streams ready\n");

  /* Create a multiplexer over the pipes. */
  if (mux_init(&muxcore, eventcore, fin, fout) < 0) {
    perror("mux_init");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }
  if (bufsize < 256) {
    const size_t newbufsize = 256;
    if (verbosity >= 0)
      fprintf(stderr, "warning: setting buffer size from %zu to %zu\n",
	      bufsize, newbufsize);
    bufsize = newbufsize;
  }
  if (verbosity > 0)
    fprintf(stderr, "Buffer size %zu\n", bufsize);
  mux_setbufsize(&muxcore, bufsize);

  /* Process events. */
  if (verbosity >= 0)
    fprintf(stderr, "Awaiting RDY signal...\n");
  while (termed == 0 && !mux_ready(&muxcore)) {
    if (child_death) {
      child_death = 0;

      if (verbosity > 1)
	fprintf(stderr, "Reaping child before RDY...\n");
      pid_t child = wait(&exit_code);
      if (verbosity > 0)
	fprintf(stderr, "Exit code %d (%s) for pid %d\n"
		"Expected pid: %d\n", exit_code,
		(exit_code > 128 ?
		 strsignal(exit_code - 128) : "app-specific"), (int) child,
		(int) server_pid);
      assert(child == server_pid);
      remove(sockname);
      remove(inname);
      remove(outname);
      exit(exit_code);
    }

    if (verbosity > 0)
      mux_dump(&muxcore, stderr);
    if (react_yield(eventcore) < 0) {
      if (errno == EAGAIN)
	exit(EXIT_SUCCESS);
      if (errno == EINTR)
	continue;
      perror("react_yield");
      remove(sockname);
      remove(inname);
      remove(outname);
      exit(EXIT_FAILURE);
    }
  }

  pid_t daemon_pid;
  if (await_end) {
    daemon_pid = 0;
  } else {
    pid_t daemon_pid = fork();
    if (daemon_pid < 0) {
      perror("fork daemon");
      exit(EXIT_FAILURE);
    }
  }

  if (daemon_pid > 0) {
    /* We are still the command process. */
    if (verbosity >= 0)
      fprintf(stderr, "Process away...\n");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_SUCCESS);
  }

  /* Re-affirm that we want to handle these signals. */
  if (signal(SIGPIPE, &on_broken_pipe) == SIG_ERR) {
    perror("signal PIPE");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }

  if (signal(SIGINT, &on_term) == SIG_ERR) {
    perror("signal INT");
    exit(EXIT_FAILURE);
  }

  if (signal(SIGTERM, &on_term) == SIG_ERR) {
    perror("signal TERM");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }

  /* Create a socket handler over the UNIX-domain socket. */
  struct acceptor acceptor = {
    .mux = &muxcore,
    .core = eventcore,
    .event = react_open(eventcore),
    .sock = sock,
  };
  dllist_init(&acceptor.sessions);
  dllist_init(&acceptor.deleted_sessions);
  if (acceptor.event == react_ERROR) {
    fprintf(stderr, "%s: no memory for accept event\n", argv[0]);
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }
  react_direct(acceptor.event, &on_acceptable, &acceptor);
  if (prime_accept(&acceptor) < 0) {
    perror("prime_accept");
    remove(sockname);
    remove(inname);
    remove(outname);
    exit(EXIT_FAILURE);
  }

  assert(daemon_pid == 0);
  /* We are the daemon process. */

  /* Detach from standard streams. */
  if (close_streams) {
    fclose(stdout);
    fclose(stderr);
    fclose(stdin);
  }

  while (termed == 0 && child_death == 0 && mux_working(&muxcore)) {
    if (verbosity > 4)
      fprintf(stderr, "Yielding...\n");
    if (verbosity > 0)
      mux_dump(&muxcore, stderr);
    if (react_yield(eventcore) < 0) {
      if (errno == EAGAIN)
	break;
      if (errno == EINTR)
	continue;
      perror("react_yield");
      exit_code = EXIT_FAILURE;
      break;
    }

    flush_deleted_sessions(&acceptor);
  }

  if (!no_exec && !server_reaped) {
    pid_t child = wait(&exit_code);
    if (child != -1) {
      assert(child == server_pid);
      server_reaped = true;
    }
  }

  remove(sockname);
  remove(inname);
  remove(outname);

  if (!no_exec)
    kill(server_pid, SIGTERM);
  exit(exit_code);
}
