#ifndef tcpsock_included
#define tcpsock_included

#include "method.h"

struct tcpsock_conf {
  char node[100];
  char svc[30];
  const char **acl;
  unsigned acllen;
};

method_constr tcpsock_constr;

#endif
