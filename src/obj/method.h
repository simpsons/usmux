#ifndef method_included
#define method_included

#include <react/types.h>

#include "shared.h"

struct method_suite {
  /* Await acknowledgement from the server.  Decrement *remp when
     received. */
  void (*await)(void *, unsigned *remp, _Bool *);

  /* Detect failure.  Return zero if okay, or an errno value
     otherwise.  Negative for termination of server. */
  int (*test)(void *);

  /* Write the configuration string into a buffer.  If buffer is null,
     just return required length. */
  int (*writecode)(void *, char *, size_t);

  /* As the child process about to execute the server, close your
     handles, which are merely duplicates of the command/daemon
     process. */
  void (*serverdetach)(void *);

  /* Start receiving connections and processing them through the
     server. */
  void (*activate)(void *);

  /* Destroy the object. */
  void (*destroy)(void *);
};

struct method {
  const struct method_suite *vtab;
  void *ctxt;
};

/* A constructor creates an object (storing its address in mp->ctxt)
   using *utils, *shared and *config as configuration, and sets
   mp->vtab to list the operations that can be performed on the
   object.  *config provides implementation-specific arguments,
   *shared provides shared configuration options set by the user, and
   *utils provides shared resources created by the main program. */
typedef int method_constr(struct method *mp,
			  const struct utils *utils,
			  const shared_opts *shared,
			  const void *config);

void method_await(const struct method *, unsigned *remp, _Bool *);
void method_activate(const struct method *);
void method_destroy(struct method *);
int method_test(const struct method *);
int method_writecode(const struct method *, char *, size_t);
void method_serverdetach(const struct method *);
int method_isnull(const struct method *);

#endif
