#ifndef options_included
#define options_included

#include <stddef.h>
#include <stdbool.h>

#include <sys/types.h>

#include "shared.h"
#include "multipiper.h"
#include "tcpsock.h"

typedef struct {
  method_constr *constr;
  union {
    struct multipiper_conf multipiper;
    struct tcpsock_conf tcpsock;
  } spec;
} action_config;

struct options {
  /* Global shared options */
  shared_opts shared;

  /* Global options */
  const char *tmproot;
  const char *cmdname;
  const char *pidfile;
  int cmd_start;
  bool await_end;
  bool close_streams;
  bool no_exec;

  struct {
    action_config *base;
    unsigned cap, size;
  } actions;

  /* Options applying to next udsock */
  mode_t sockmode;
  size_t bufsize;

  struct {
    const char **base;
    unsigned cap, size;
  } ipacl;
};

void options_init(struct options *);

int options_parse(struct options *, const char *prog,
		  int argc, const char *const *argv);

void options_usage(FILE *out, const char *prog);

#endif
