#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "command.h"

static char *apply_args(const char *from,
			const struct method *meth,
			unsigned rem,
			unsigned *index)
{
  size_t myidx = *index;

  /* Compute length of expanded argument. */
  size_t len = 0;
  for (const char *ptr = from; *ptr; ptr++) {
    if (ptr[0] == '%') {
      if (ptr[1] != 's') {
	len++;
	if (ptr[1])
	  ptr++;
	continue;
      }
      ptr++;
      if (myidx < rem) {
	int done = method_writecode(&meth[myidx++], NULL, 0);
	if (done < 0) return NULL;
	len += done;
      } else {
	/* There are too many "%s"es, not enough methods. */
	return NULL;
      }
    } else {
      len++;
    }
  }
  len++;

  char *const res = malloc(len);
  if (!res) return NULL;
  char *const end = res + len;
  char *out = res;

  for (const char *ptr = from; *ptr; ptr++) {
    if (out == end) goto failure;
    if (ptr[0] == '%') {
      if (ptr[1] != 's') {
	*out++ = *++ptr;
	continue;
      }

      assert(*index < rem);
      int done = method_writecode(&meth[(*index)++], out, end - out);
      if (done < 0) goto failure;
      if (done >= end - out) goto failure;
      out += done;
      ptr++;
    } else {
      *out++ = *ptr;
    }
  }
  assert(out < end);
  *out = '\0';
  return res;

 failure:
  free(res);
  return NULL;
}

char **make_new_args(int argc, const char *const *argv,
		     const struct method *meth, unsigned nmeth,
		     unsigned *used)
{
  char **newargs = malloc(sizeof *newargs * (argc + 1));
  if (!newargs) return NULL;
  unsigned index = 0;
  for (int argi = 0; argi < argc; argi++) {
    newargs[argi] = apply_args(argv[argi], meth, nmeth, &index);
    if (!newargs[argi]) {
      for (int i = 0; i < argi; i++)
	free(newargs[i]);
      free(newargs);
      return NULL;
    }
  }
  newargs[argc] = NULL;
  *used = index;
  return newargs;
}
