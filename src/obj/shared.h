#ifndef shared_included
#define shared_included

#include <react/types.h>

typedef struct {
  int verbosity;
  const char *progname;
} shared_opts;

struct utils {
  react_core_t core;
  const char *tmpdir;
};

#endif
