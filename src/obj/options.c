#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include "options.h"

#define DEFAULT_SOCK_PERMS 0600
#define DEFAULT_TEMP_DIR "/tmp"

void options_init(struct options *opts)
{
  static const struct options null = {
    .tmproot = DEFAULT_TEMP_DIR,
    .sockmode = DEFAULT_SOCK_PERMS,
    .close_streams = true,
    .bufsize = 1024,
    .cmd_start = -1,
    .no_exec = true,
  };

  *opts = null;
  const char *tmp = getenv("TMPDIR");
  if (tmp) opts->tmproot = tmp;
}

static int ensure_entry(struct options *opts)
{
  if (opts->actions.size == opts->actions.cap) {
    const unsigned inc = 2 + opts->actions.cap / 2;
    const unsigned ncap = opts->actions.cap + inc;
    action_config *nbase =
      realloc(opts->actions.base, sizeof *opts->actions.base * ncap);
    if (nbase == NULL) {
      errno = ENOMEM;
      return -1;
    }
    opts->actions.base = nbase;
    opts->actions.cap = ncap;
  }

  int result = opts->actions.size++;
  opts->actions.base[result].constr = 0;
  return result;
}

static int add_aclentry(struct options *opts, const char *msg)
{
  if (opts->ipacl.size == opts->ipacl.cap) {
    const unsigned inc = 2 + opts->ipacl.cap / 2;
    const unsigned ncap = opts->ipacl.cap + inc;
    const char **nbase =
      realloc(opts->ipacl.base, sizeof *opts->ipacl.base * ncap);
    if (nbase == NULL) {
      errno = ENOMEM;
      return -1;
    }
    opts->ipacl.base = nbase;
    opts->ipacl.cap = ncap;
  }

  int result = opts->ipacl.size++;
  opts->ipacl.base[result] = msg;
  return result;
}

int options_parse(struct options *opts, const char *prog,
		  int argc, const char *const *argv)
{
  opts->shared.progname = prog;
  for (int argi = 0; argi < argc; argi++) {
    if (argv[argi][0] == '-') {
      if (!strcmp(argv[argi] + 1, "B")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s unixsockname\n", prog, argv[argi]);
	  return 1;
	}
	int pos = ensure_entry(opts);
	if (pos < 0) {
	  perror(argv[argi]);
	  return 1;
	}
	opts->actions.base[pos].constr = &multipiper_constr;
	opts->actions.base[pos].spec.multipiper.sockname = argv[++argi];
	opts->actions.base[pos].spec.multipiper.sockmode = opts->sockmode;
	opts->actions.base[pos].spec.multipiper.bufsize = opts->bufsize;
      } else if (!strcmp(argv[argi] + 1, "S")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s tcpsockaddr/port\n", prog, argv[argi]);
	  return 1;
	}
	const char *arg = argv[++argi];
	const char *col = strrchr(arg, '/');
	if (!col) {
	  fprintf(stderr, "usage: %s %s tcpsockaddr/port\n", prog, argv[argi]);
	  return 1;
	}
	int pos = ensure_entry(opts);
	if (pos < 0) {
	  perror(argv[argi]);
	  return 1;
	}
	struct tcpsock_conf *conf = &opts->actions.base[pos].spec.tcpsock;
	assert((size_t) (col - arg) < sizeof conf->node);
	strncpy(conf->node, arg, sizeof conf->node);
	conf->node[col - arg] = '\0';
	strncpy(conf->svc, col + 1, sizeof conf->svc);
	assert(conf->svc[sizeof conf->svc - 1] == '\0');
	conf->acllen = opts->ipacl.size;
	conf->acl = malloc(sizeof *conf->acl * conf->acllen);
	if (conf->acl == NULL) {
	  fprintf(stderr, "%s: no memory to copy ACL\n", prog);
	  return 1;
	}
	for (unsigned i = 0; i < conf->acllen; i++)
	  conf->acl[i] = opts->ipacl.base[i];
	opts->actions.base[pos].constr = &tcpsock_constr;
      } else if (!strcmp(argv[argi] + 1, "T")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s tmpdirname\n", prog, argv[argi]);
	  return 1;
	}
	opts->tmproot = argv[++argi];
      } else if (!strcmp(argv[argi] + 1, "P")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s pidfilename\n", prog, argv[argi]);
	  return 1;
	}
	opts->pidfile = argv[++argi];
      } else if (!strcmp(argv[argi] + 1, "a")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s host\n", prog, argv[argi]);
	  return 1;
	}
	const char *arg = argv[++argi];
	if (add_aclentry(opts, arg) < 0) {
	  fprintf(stderr, "%s: failed to add %s to ACL\n", prog, arg);
	  return 1;
	}
      } else if (!strcmp(argv[argi] + 1, "d")) {
	opts->await_end = true;
	opts->close_streams = false;
      } else if (!strcmp(argv[argi] + 1, "v")) {
	opts->shared.verbosity++;
      } else if (!strcmp(argv[argi] + 1, "o")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s filemode\n", prog, argv[argi]);
	  return 1;
	}
	opts->sockmode = strtol(argv[++argi], NULL, 8);
      } else if (!strcmp(argv[argi] + 1, "n")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s cmdname\n", prog, argv[argi]);
	  return 1;
	}
	opts->cmdname = argv[++argi];
      } else if (!strcmp(argv[argi] + 1, "z")) {
	if (argi + 1 == argc) {
	  fprintf(stderr, "usage: %s %s buffer-size\n", prog, argv[argi]);
	  return 1;
	}
	char *end;
	opts->bufsize = strtoull(argv[++argi], &end, 0);
	if (end) {
	  switch (*end) {
	  case 'k':
	  case 'K':
	    opts->bufsize *= 1024;
	    break;

	  case 'm':
	  case 'M':
	    opts->bufsize *= 1024LL * 1024;
	    break;

	  case 'g':
	  case 'G':
	    opts->bufsize *= 1024LL * 1024 * 1024;
	    break;

	  case 't':
	  case 'T':
	    opts->bufsize *= 1024LL * 1024 * 1024 * 1024;
	    break;
	  }
	}
      } else if (!strcmp(argv[argi] + 1, "-")) {
	opts->cmd_start = argi + 1;
	opts->no_exec = false;
	break;
      } else {
	fprintf(stderr, "%s: unknown switch %s\n", prog, argv[argi]);
	return 1;
      }
    } else if (argv[argi][0] == '+') {
      if (!strcmp(argv[argi] + 1, "v")) {
	opts->shared.verbosity--;
      } else if (!strcmp(argv[argi] + 1, "a")) {
	free(opts->ipacl.base);
	opts->ipacl.size = opts->ipacl.cap = 0;
      } else if (!strcmp(argv[argi] + 1, "-")) {
	opts->no_exec = true;
      } else {
	fprintf(stderr, "%s: unknown switch %s\n", prog, argv[argi]);
	return 1;
      }
    }
  }

  return 0;
}

void options_usage(FILE *out, const char *prog)
{
  fprintf(out, "usage: %s [options] -- command\n", prog);
  fprintf(out, "\t-B file (socket file)\n");
  fprintf(out, "\t-S addr (TCP bind address; host/port)\n");
  fprintf(out, "\t-T dir  (directory for pipes; default %s or TMPDIR env)\n",
	  DEFAULT_TEMP_DIR);
  fprintf(out, "\t-P file (pid file)\n");
  fprintf(out, "\t+-      (don't run)\n");
  fprintf(out, "\t-d      (debug mode)\n");
  fprintf(out, "\t-/+v    (increase/decrease verbosity)\n");
  fprintf(out, "\t-o mode (socket permissions; default %#4o)\n",
	  DEFAULT_SOCK_PERMS);
  fprintf(out, "\t-n name (Java process name)\n");
  fprintf(out, "\t-z size (buffer size; KMGT suffixes)\n");
  fprintf(out, "\t-a host (add host to ACL for -S)\n");
  fprintf(out, "\t+a      (clear ACL for -S)\n");
}
