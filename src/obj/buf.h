// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef included_buf_h
#define included_buf_h

#include <stdint.h>

#include <sys/types.h>

struct buf {
  size_t pos, lim, max;
  void *base;
};

/* Set the buffer ready to cover a whole region of memory. */
inline void buf_init(struct buf *b, void *base, size_t max) {
  b->max = max;
  b->base = base;
  b->pos = 0;
  b->lim = max;
}

/* Discard all data, and set the buffer ready for writing. */
inline void buf_clear(struct buf *b) {
  b->pos = 0;
  b->lim = b->max;
}

/* Set the buffer ready for reading. */
inline void buf_flip(struct buf *b) {
  b->lim = b->pos;
  b->pos = 0;
}

inline void buf_rewind(struct buf *b) {
  b->pos = 0;
}

inline size_t buf_rem(const struct buf *b) { return b->lim - b->pos; }

inline void *buf_base(const struct buf *b) {
  return &b->pos[(unsigned char *) b->base];
}

/* Move the live portion of the buffer to the start, and set the
   remainder as the new live portion.  The buffer is ready for
   writing. */
void buf_slide(struct buf *b);

ssize_t buf_send(int fd, struct buf *b, size_t max, int flags);
ssize_t buf_recv(int fd, struct buf *b, size_t max, int flags);
ssize_t buf_write(int fd, struct buf *b, size_t max);
ssize_t buf_read(int fd, struct buf *b, size_t max);

uint_fast16_t buf_getu16(struct buf *);
void buf_putu16(struct buf *, uint_fast16_t);
uint_fast32_t buf_getu32(struct buf *);
void buf_putu32(struct buf *, uint_fast32_t);

void buf_skip(struct buf *, size_t);

/* Restrict the buffer to receive no more than a specified limit. */
void buf_limit(struct buf *, size_t);

size_t buf_move(struct buf *to, struct buf *from, size_t max);

#endif
