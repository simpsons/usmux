#ifndef multipiper_included
#define multipiper_included

#include <react/types.h>

#include "method.h"

struct multipiper_conf {
  const char *sockname;
  mode_t sockmode;
  size_t bufsize;
};

method_constr multipiper_constr;

#endif
