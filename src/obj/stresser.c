// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <ctype.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>

#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/un.h>

#include "buf.h"

static void hex_block(FILE *out, const char *pre,
		      const struct buf *buf, size_t lim);

int main(int argc, const char *const *argv)
{
  if (argc < 2) {
    fprintf(stderr, "Usage: %s sockname\n", argv[0]);
    exit(EXIT_SUCCESS);
  }

  srand(time(NULL));

  const char *sockname = argv[1];

  int sock = socket(PF_UNIX, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("socket");
    exit(EXIT_FAILURE);
  }

  struct sockaddr_un addr;
  memset(&addr, 0, sizeof addr);
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, sockname, sizeof addr.sun_path);

  if (connect(sock, (struct sockaddr *) &addr, sizeof addr) < 0) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  unsigned char buf_sp[3][1024 * 16];
  struct buf out, in, expect;

  /* Set the output buffer empty and ready for reading. */
  buf_init(&out, buf_sp[0], sizeof buf_sp[0]);
  buf_flip(&out);

  /* Set the input buffer at full capacity, and ready for writing. */
  buf_init(&in, buf_sp[1], sizeof buf_sp[1]);

  /* Set the expectation buffer to empty and ready for reading. */
  buf_init(&expect, buf_sp[2], sizeof buf_sp[2]);
  buf_flip(&expect);

  size_t generated = 0, sent = 0, received = 0, compared = 0;
  bool eof = false;
  for ( ; ; ) {
    bool active = false;

    {
      /* Ensure that we have a full buffer of random data. */
      buf_slide(&out);
      const size_t rem = buf_rem(&out);
      if (rem > 0) {
	unsigned char *const base = buf_base(&out);
	for (size_t i = 0; i < rem; i++)
	  base[i] = rand();
	buf_skip(&out, rem);
	generated += rem;
	printf("Generated %zu random bytes.\n", rem);
	active = true;
      }
      buf_flip(&out);
    }

    {
      fd_set infds, outfds;
      FD_ZERO(&infds);
      FD_ZERO(&outfds);

      size_t offer;
      {
	/* How much data can we send?  We have some to send, and space
	   to keep a copy; choose the minimum. */
	size_t rem = buf_rem(&out);
	buf_slide(&expect);
	size_t space = buf_rem(&expect);
	buf_flip(&expect);
	offer = rem < space ? rem : space;
	if (offer > 512) {
	  offer = 512 + (offer - 512) * rand() / RAND_MAX;
	}
	if (offer > 0) {
	  printf("Bytes to send: %zu available; %zu space; %zu offered\n",
		 rem, space, offer);
	  FD_SET(sock, &outfds);
	}
      }

      size_t allow = 0;
      if (!eof) {
	/* How much data can we receive?  We have some space, and we
	   expect a certain amount; choose the minimum. */
	size_t rem = buf_rem(&in);
	size_t got = buf_rem(&expect);
	allow = rem < got ? rem : got;
	if (allow > 0) {
	  printf("Bytes to receive: %zu space; %zu got; %zu allowed\n",
		 rem, got, allow);
	  FD_SET(sock, &infds);
	}
      }

      int rc = select(sock + 1, &infds, &outfds, NULL, NULL);
      if (rc < 0) {
	perror("select");
	exit(EXIT_FAILURE);
      }

      if (FD_ISSET(sock, &outfds)) {
	if (offer > 0) {
	  struct buf tmp = out;
	  ssize_t done = buf_send(sock, &out, offer, MSG_NOSIGNAL);
	  if (done == -1) {
	    if (errno != EAGAIN) {
	      perror("buf_send");
	      exit(EXIT_FAILURE);
	    }
	  } else {
	    assert(done > 0);
	    printf("Transmitted %zu bytes.\n", (size_t) done);
	    sent += done;
	    buf_slide(&expect);
	    size_t copied = buf_move(&expect, &tmp, done);
	    buf_flip(&expect);
	    assert(copied == (size_t) done);
	  }
	  active = true;
	}

      }

      if (FD_ISSET(sock, &infds)) {
	assert(allow > 0);
	ssize_t done = buf_recv(sock, &in, allow, 0);
	if (done == -1) {
	  if (errno != EAGAIN) {
	    perror("buf_recv");
	    exit(EXIT_FAILURE);
	  }
	} else if (done == 0) {
	  printf("Server terminated.\n");
	  eof = true;
	} else {
	  printf("Received %zu bytes.\n", (size_t) done);
	  received += done;
	}
	active = true;
      }
    }

    printf("Sent: %zu; received %zu; %zu in transit\n",
	   sent, received, sent - received);

    {
      /* Consume matching bytes from the server against those in the
	 expectation buffer. */
      buf_flip(&in);
      size_t got1 = buf_rem(&in);
      size_t got2 = buf_rem(&expect);
      size_t max = got1 < got2 ? got1 : got2;
      if (max > 0) {
	/* Find the first difference. */
	unsigned char *base1 = buf_base(&in);
	unsigned char *base2 = buf_base(&expect);
	size_t matched = 0;
	while (matched < max && base1[matched] == base2[matched])
	  matched++;
	compared += matched;
	buf_skip(&in, matched);
	buf_skip(&expect, matched);

	if (matched < max) {
	  size_t start = matched > 16 ? matched - 16 : 0;
	  size_t end = start + 32 < max ? start + 32 : max;
	  size_t offset = matched - start;
	  printf("Mistake detected at %zu bytes (offset %zu)\n",
		 compared, offset);

	  struct buf tmp;
	  buf_init(&tmp, base2 + start, end - start);
	  hex_block(stdout, "expected: ", &tmp, -1);
	  buf_init(&tmp, base1 + start, end - start);
	  hex_block(stdout, "received: ", &tmp, -1);

	  exit(EXIT_FAILURE);
	}
	active = true;
      }

      buf_slide(&in);
    }

    if (!active) {
      printf("Activity ended.\n");
      break;
    }
  }

  if (close(sock) < 0) {
    perror("close");
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}

static void hex_block(FILE *out, const char *pre,
		      const struct buf *buf, size_t lim)
{
  if (lim > buf_rem(buf))
    lim = buf_rem(buf);
  const unsigned char *base = buf_base(buf);

  for (size_t j = 0; j < lim; j += 16) {
    fprintf(out, "%s", pre);

    const size_t nj = j + 16;
    size_t ulim = (nj < lim ? nj : lim) + 16 - nj;

    /* Print hex values in groups of 16. */
    for (size_t i = 0; i < 16 ; i++)
      if (i < ulim)
	fprintf(out, "%02X ", base[i + j]);
      else
	fprintf(out, ".. ");

    /* Print printable characters in groups of 16, on the same line. */
    for (size_t i = 0; i < 16 ; i++)
      if (i < ulim) {
	int c = base[i + j];
	if (isgraph(c))
	  fprintf(out, "%c", c);
	else
	  putc('.', out);
      } else
	putc(' ', out);

    putc('\n', out);
  }

  if (lim < buf_rem(buf))
    fprintf(out, "%s...\n", pre);
}
