#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>

#include <unistd.h>
#include <fcntl.h>

#include <ddslib/dllist.h>
#include <react/core.h>
#include <react/event.h>
#include <react/fd.h>
#include <react/idle.h>

#include "buf.h"
#include "multipiper.h"

struct conn;

typedef dllist_hdr(struct conn) connlist;

enum status {
  LIVE,
  DEAD,
  CTRL,
};

static unsigned nextpiper;

struct piper {
  int verbosity;
  unsigned id, nextid;
  react_core_t core;
  const char *tmpdir;
  connlist conns[3];
  char upname[FILENAME_MAX], downname[FILENAME_MAX];
  bool up_made, down_made, sock_made;
  char sockname[FILENAME_MAX];
  int sockfd, upfd, downfd;
  react_t accept_event, up_event, down_event, tidy_event;
  struct conn *nextconn;
  int term;
  unsigned *remp;
  bool *failure;
};

#define max(a,b) ((a) > (b) ? (a) : (b))

#define BUFSIZE max(8 * 1024, FILENAME_MAX * 2 + 48)

struct conn {
  unsigned id;
  enum status status;
  struct piper *piper;
  char upname[FILENAME_MAX], downname[FILENAME_MAX];
  dllist_elem(struct conn) others;
  int upfd, downfd, sockfd;
  bool upout, upin, downout, downin, up_made, down_made;
  bool client_okay;
  react_t upin_event, upout_event, downin_event, downout_event;
  struct buf upbuf;
  char data[BUFSIZE];
};

static void safeclose(int *fp)
{
  close(*fp);
  *fp = -1;
}

static void safecloseevent(react_t *p)
{
  react_close(*p);
  *p = react_ERROR;
}

static void move_connection(struct conn *conn, enum status st)
{
  if (conn == conn->piper->nextconn) {
    conn->piper->nextconn = NULL;
  } else {
    dllist_unlink(&conn->piper->conns[conn->status], others, conn);
  }
  conn->status = st;
  dllist_append(&conn->piper->conns[conn->status], others, conn);
}

static void destroy_conn(struct conn *conn)
{
  if (conn == NULL) return;

  if (conn->up_made) {
    assert(*conn->upname);
    remove(conn->upname);
  }
  if (conn->down_made) {
    assert(*conn->downname);
    remove(conn->downname);
  }

  safeclose(&conn->upfd);
  safeclose(&conn->downfd);
  safeclose(&conn->sockfd);
  assert(conn->upfd == -1);
  assert(conn->downfd == -1);
  assert(conn->sockfd == -1);

  safecloseevent(&conn->upin_event);
  safecloseevent(&conn->upout_event);
  safecloseevent(&conn->downin_event);
  safecloseevent(&conn->downout_event);
  assert(conn->upin_event == react_ERROR);
  assert(conn->downin_event == react_ERROR);
  assert(conn->upout_event == react_ERROR);
  assert(conn->downout_event == react_ERROR);

  /* Either the connection is the next prepared one, or it belongs to
     one of the lists. */
  if (conn == conn->piper->nextconn)
    conn->piper->nextconn = NULL;
  else
    dllist_unlink(&conn->piper->conns[conn->status], others, conn);
  free(conn);
}

static int prime_tidy(struct piper *self)
{
  /* There's nothing to do if there are no stale connections. */
  if (dllist_isempty(&self->conns[DEAD])) return 0;
  if (react_isprimed(self->tidy_event)) return 0;
  return react_prime_idle(self->tidy_event);
}

/* Close resources for the connection, and add it to the dead list. */
static void terminate(struct conn *conn)
{
  safecloseevent(&conn->upin_event);
  safecloseevent(&conn->downin_event);
  safecloseevent(&conn->upout_event);
  safecloseevent(&conn->downout_event);

  safeclose(&conn->downfd);
  safeclose(&conn->upfd);
  safeclose(&conn->sockfd);

  move_connection(conn, DEAD);
  prime_tidy(conn->piper);
}

static int prime_upout(struct conn *conn)
{
  if (conn->upout) return 0;
  assert(conn->upfd >= 0);
  assert(conn->upout_event != react_ERROR);
  if (react_isprimed(conn->upout_event)) return 0;
  return react_prime_fdout(conn->upout_event, conn->upfd);
}

static int prime_upin(struct conn *conn)
{
  if (conn->upin) return 0;
  assert(conn->sockfd >= 0);
  assert(conn->upin_event != react_ERROR);
  if (react_isprimed(conn->upin_event)) return 0;
  return react_prime_fdin(conn->upin_event, conn->sockfd);
}

static int prime_downout(struct conn *conn)
{
  assert(!conn->downout);
  assert(conn->sockfd >= 0);
  assert(conn->downout_event != react_ERROR);
  if (react_isprimed(conn->downout_event)) return 0;
  return react_prime_fdout(conn->downout_event, conn->sockfd);
}

static int prime_downin(struct conn *conn)
{
  assert(!conn->downin);
  assert(conn->downfd >= 0);
  assert(conn->downin_event != react_ERROR);
  if (react_isprimed(conn->downin_event)) return 0;
  return react_prime_fdin(conn->downin_event, conn->downfd);
}

static int prime_control_up(struct piper *self)
{
  /* We don't need to be ready to send control data if we don't have
     any. */
  if (dllist_isempty(&self->conns[CTRL])) return 0;

  /* We're okay if already primed. */
  if (react_isprimed(self->up_event)) return 0;

  /* We have at least one connection in the control list, waiting to
     send its pipe details to the server. */
  assert(self->upfd >= 0);
  return react_prime_fdout(self->up_event, self->upfd);
}

static int prime_control_down(struct piper *self)
{
  if (react_isprimed(self->down_event)) return 0;

  /* Make sure we can detect when the server has had enough. */
  assert(self->downfd >= 0);
  return react_prime_fdin(self->down_event, self->downfd);
}

static void on_tidy(void *vc)
{
  struct piper *self = vc;

  /* Delete any stale connections. */
  struct conn *conn;
  while ((conn = dllist_first(&self->conns[DEAD])) != NULL)
    destroy_conn(conn);
}

static void on_ctrl_ready(void *vc)
{
  struct piper *self = vc;

  if (self->verbosity >= 5)
    fprintf(stderr, "mp%u getting signal from server\n", self->id);

  /* TODO: Read one byte from the stream, indicating that the server
     is ready.  If so, decrement an external counter to indicate that
     we are ready too.  If not (i.e., EOF), set an error flag. */
  char buf[1];
  ssize_t done = read(self->downfd, buf, 1);
  if (done == 0) {
    *self->failure = true;
    if (self->verbosity >= 3)
      fprintf(stderr, "mp%u read EOF from server signal\n", self->id);
  } else if (done < 1) {
    *self->failure = true;
    if (self->verbosity >= 3)
      fprintf(stderr, "mp%u read fail on server signal: %s\n", self->id,
	      strerror(errno));
  } else {
    if (self->verbosity >= 4)
      fprintf(stderr, "mp%u okay byte from server signal\n", self->id);
    (*self->remp)--;
  }
}

static void on_ctrl_down(void *vc)
{
  struct piper *self = vc;

  /* Any further data from the server on the control channel should be
     regarded as termination. */
  self->term = -1;
}

static void on_ctrl_up(void *vc)
{
  struct piper *self = vc;

  struct conn *conn = dllist_first(&self->conns[CTRL]);
  if (!conn) return;

  /* Attempt to write the control data of the first incomplete
     connection. */
  ssize_t done = buf_write(self->upfd, &conn->upbuf, -1);
  if (done < 0) {
    perror("buf_write");
    self->term = errno;
    return;
  }

  /* If there's some data left, we can't complete the connection. */
  if (buf_rem(&conn->upbuf) > 0) {
    if (prime_control_up(self) < 0) {
      perror("prime_control_up");
      self->term = errno;
    }
    return;
  }

  if (self->verbosity >= 4)
    fprintf(stderr, "mp%u.%u control data sent\n", self->id, conn->id);

  /* Move it from the control list to the live list. */
  move_connection(conn, LIVE);

  /* Get the session meta-data. */
  buf_init(&conn->upbuf, conn->data, BUFSIZE);
#ifdef _GNU_SOURCE
  struct ucred creds;
  socklen_t creds_len = sizeof creds;
  if (getsockopt(conn->sockfd, SOL_SOCKET,
		 SO_PEERCRED, &creds, &creds_len) < 0) {
    perror("getsockopt(SO_PEERCRED)");
    buf_putu16(&conn->upbuf, 0);
  } else {
    buf_putu16(&conn->upbuf, 10);
    buf_putu32(&conn->upbuf, 0x554E4958u);
    buf_putu16(&conn->upbuf, creds.pid);
    buf_putu16(&conn->upbuf, creds.uid);
    buf_putu16(&conn->upbuf, creds.gid);
  }
#endif
  /* The buffer must be in write mode. */

  /* Initialize other fields. */
  conn->upout = conn->downout = conn->upin = conn->downin = false;

  if (prime_upin(conn)) {
    perror("prime_upin");
    fprintf(stderr, "upfd = %d\n", conn->upfd);
    terminate(conn);
    return;
  }

  if (prime_upout(conn)) {
    perror("prime_upout");
    terminate(conn);
    return;
  }

  if (prime_downin(conn)) {
    perror("prime_downin");
    terminate(conn);
    return;
  }

  if (prime_downout(conn)) {
    perror("prime_downout");
    terminate(conn);
    return;
  }

  if (self->verbosity >= 4)
    fprintf(stderr, "mp%u.%u now running\n", self->id, conn->id);
}






static void check_upstream(struct conn *conn)
{
  /* Invariant: upbuf always points to the undefined space in the
     buffer. */

  /* We should attempt to fill our buffer, if it has some space
     remaining, and we are ready to read from the client. */
  if (conn->upin &&
      conn->sockfd > -1 &&
      conn->upfd > -1 &&
      buf_rem(&conn->upbuf) > 0) {
    ssize_t done = buf_recv(conn->sockfd, &conn->upbuf, -1, 0);
    conn->upin = false;
    if (done < 0) {
      perror("buf_recv");
      terminate(conn);
    } else if (done == 0) {
      conn->client_okay = false;
      react_cancel(conn->upin_event);
      conn->upin = false;
      safeclose(&conn->sockfd);
      conn->downin = conn->downout = false;
      react_cancel(conn->downin_event);
      react_cancel(conn->downout_event);
    }
  }

  /* Put the buffer into 'read' mode. */
  buf_flip(&conn->upbuf);

  /* We should attempt to clear our buffer, if it is not empty, and we
     are ready to write to the server. */
  if (conn->upout && conn->upfd > -1 && buf_rem(&conn->upbuf) > 0) {
    ssize_t done = buf_write(conn->upfd, &conn->upbuf, -1);
    conn->upout = false;
    if (done < 0) {
      perror("buf_write");
      terminate(conn);
    }
  }

  /* We should close our upstream connection to the server, if we
     detected EOF from the client, and have no more data to send. */
  if (conn->upfd > -1 && buf_rem(&conn->upbuf) == 0 && !conn->client_okay) {
    terminate(conn);
  }

  /* We should detect when we can write more data upstream, provided
     we still have a descriptor to write to. */
  if (conn->upfd > -1) {
    if (prime_upout(conn) < 0) {
      perror("prime_upout");
      terminate(conn);
    }
  }

  /* Put the buffer back into 'write' mode. */
  buf_slide(&conn->upbuf);

  if (conn->sockfd > -1 && conn->client_okay) {
    if (prime_upin(conn) < 0) {
      perror("prime_upin");
      terminate(conn);
    }
  }
}

static void on_up_write(void *vc)
{
  struct conn *conn = vc;
  conn->upout = true;
  check_upstream(conn);
}

static void on_up_read(void *vc)
{
  struct conn *conn = vc;
  conn->upin = true;
  check_upstream(conn);
}



static void check_downstream(struct conn *conn)
{
  if (!conn->downout) return;
  if (!conn->downin) return;

  ssize_t done = splice(conn->downfd, NULL, conn->sockfd, NULL, 40960, 0);
  conn->downout = false;
  conn->downin = false;
  if (done < 0) {
    perror("splice down");
    terminate(conn);
    return;
  }

  /* If the server closes the connection, we immediately terminate
     everything. */
  if (done == 0) {
    terminate(conn);
    return;
  }

  if (prime_downin(conn) < 0) {
    perror("prime_downin");
    terminate(conn);
    return;
  }

  if (prime_downout(conn) < 0) {
    perror("prime_downout");
    terminate(conn);
    return;
  }
}

static void on_down_write(void *vc)
{
  struct conn *conn = vc;
  conn->downout = true;
  check_downstream(conn);
}

static void on_down_read(void *vc)
{
  struct conn *conn = vc;
  conn->downin = true;
  check_downstream(conn);
}

/* Create the connection structure and the named pipes, so no further
   allocation is required when the next connection comes in on the
   socket. */
static int ensure_next_connection(struct piper *self)
{
  /* Stop if we're already prepared. */
  if (self->nextconn != NULL) return 0;

  /* Create the connection structure. */
  struct conn *conn = malloc(sizeof *conn);
  if (conn == NULL) {
    errno = ENOMEM;
    return -1;
  }
  static const struct conn null = {
    .upfd = -1,
    .downfd = -1,
    .sockfd = -1,
    .upin_event = react_ERROR,
    .upout_event = react_ERROR,
    .downin_event = react_ERROR,
    .downout_event = react_ERROR,
  };
  *conn = null;
  conn->id = self->nextid++;

  /* Make this structure ready for the next connection.  This makes it
     safe to destroy with destroy_connection, which expects it to be
     in a list, or retained in this field. */
  self->nextconn = conn;

  /* Choose names for the data pipes. */
  if (snprintf(conn->upname, sizeof conn->upname,
	       "%s/mp%u.%u-up", self->tmpdir, self->id, conn->id) < 0)
    goto failure;
  if (snprintf(conn->downname, sizeof conn->downname,
	       "%s/mp%u.%u-down", self->tmpdir, self->id, conn->id) < 0)
    goto failure;

  /* Create the pipes. */
  if (mkfifo(conn->upname, 0600) < 0) {
    fprintf(stderr, "mkfifo(%s %o): %s\n",
	    conn->upname, 0600, strerror(errno));
    goto failure;
  }
  conn->up_made = true;
  if (mkfifo(conn->downname, 0600) < 0) {
    fprintf(stderr, "mkfifo(%s %o): %s\n",
	    conn->downname, 0600, strerror(errno));
    goto failure;
  }
  conn->down_made = true;

  /* Open the pipes. */
  conn->downfd = open(conn->downname, O_RDONLY | O_NONBLOCK);
  if (conn->downfd < 0) {
    perror("open(in)");
    goto failure;
  }
  conn->upfd = open(conn->upname, O_RDWR | O_NONBLOCK);
  if (conn->upfd < 0) {
    perror("open(out)");
    goto failure;
  }

  conn->upin_event = react_open(self->core);
  if (conn->upin_event == react_ERROR) goto failure;
  conn->upout_event = react_open(self->core);
  if (conn->upout_event == react_ERROR) goto failure;
  conn->downin_event = react_open(self->core);
  if (conn->downin_event == react_ERROR) goto failure;
  conn->downout_event = react_open(self->core);
  if (conn->downout_event == react_ERROR) goto failure;

  /* Prepare the control data. */
  int done = snprintf(conn->data, BUFSIZE, "%s\n%s\n",
		      conn->upname, conn->downname);
  if (done < 0)
    goto failure;
  buf_init(&conn->upbuf, conn->data, done);

  /* Record other essential infomation about this connection. */
  conn->piper = self;
  conn->client_okay = true;

  /* Set all the events to invoke the right functions. */
  react_direct(conn->upin_event, &on_up_read, conn);
  react_direct(conn->upout_event, &on_up_write, conn);
  react_direct(conn->downin_event, &on_down_read, conn);
  react_direct(conn->downout_event, &on_down_write, conn);

  /* Now we have the structure ready, we are prepared to accept new
     connections. */
  assert(self->sockfd >= 0);
  return react_prime_fdin(self->accept_event, self->sockfd);

  int e;
 failure:
  e = errno;
  destroy_conn(conn);
  errno = e;
  return -1;
}

static void destroy_piper(struct piper *self)
{
  struct conn *conn;

  /* Destroy all connections. */
  destroy_conn(self->nextconn);
  while ((conn = dllist_first(&self->conns[CTRL])) != NULL)
    destroy_conn(conn);
  while ((conn = dllist_first(&self->conns[LIVE])) != NULL)
    destroy_conn(conn);
  while ((conn = dllist_first(&self->conns[DEAD])) != NULL)
    destroy_conn(conn);

  /* Stop watching our own descriptors before we close them.  Release
     any other reactor resources too. */
  safecloseevent(&self->up_event);
  safecloseevent(&self->down_event);
  safecloseevent(&self->accept_event);
  safecloseevent(&self->tidy_event);

  /* Close our file descriptors. */
  safeclose(&self->upfd);
  safeclose(&self->downfd);
  safeclose(&self->sockfd);

  /* Remove our files. */
  if (self->verbosity >= 4)
    fprintf(stderr, "mp%u removing global files\n", self->id);
  if (self->up_made) {
    assert(*self->upname);
    remove(self->upname);
  }
  if (self->down_made) {
    assert(*self->downname);
    remove(self->downname);
  }
  if (self->sock_made) {
    assert(*self->sockname);
    remove(self->sockname);
  }

  /* Everything should be gone now, so delete us. */
  assert(self->nextconn == NULL);
  assert(dllist_isempty(&self->conns[CTRL]));
  assert(dllist_isempty(&self->conns[LIVE]));
  assert(dllist_isempty(&self->conns[DEAD]));
  free(self);
}


static void on_acceptable(void *vc)
{
  struct piper *self = vc;

  /* Accept the new connection. */
  struct sockaddr addr;
  socklen_t addrlen = sizeof addr;
  int cli = accept(self->sockfd, &addr, &addrlen);
  if (cli < 0) {
    perror("accept");
    self->term = errno;
    return;
  }

  /* Move the connection structure to the control list. */
  struct conn *conn = self->nextconn;
  assert(conn != NULL);
  conn->sockfd = cli;
  move_connection(conn, CTRL);
  if (prime_control_up(self) < 0) {
    self->term = errno;
  } else {
    ensure_next_connection(self);
  }
}

static struct piper *make_piper(react_core_t core,
				const char *tmpdir,
				int verbosity,
				const struct multipiper_conf *conf)
{
  if (tmpdir == NULL) {
    errno = EINVAL;
    return NULL;
  }

  /* Create object and statically initialize. */
  struct piper *self = malloc(sizeof *self);
  if (self == NULL) return NULL;
  static const struct piper null = {
    .conns = {
      dllist_HDRINIT,
      dllist_HDRINIT,
      dllist_HDRINIT,
    },
    .sockfd = -1,
    .upfd = -1,
    .downfd = -1,
    .accept_event = react_ERROR,
    .up_event = react_ERROR,
    .down_event = react_ERROR,
    .tidy_event = react_ERROR,
  };
  *self = null;
  self->id = nextpiper++;

  /* Initialize other fields. */
  self->tmpdir = tmpdir;
  self->core = core;
  self->verbosity = verbosity;
  
  /* Create events to detect new connections, and to be able to read
     and write control information. */
  self->accept_event = react_open(core);
  if (self->accept_event == react_ERROR) {
    if (verbosity >= 0)
      fprintf(stderr, "accept handle creation failed\n");
    goto failure;
  }
  self->up_event = react_open(core);
  if (self->up_event == react_ERROR) {
    if (verbosity >= 0)
      fprintf(stderr, "ctrlup handle creation failed\n");
    goto failure;
  }
  self->down_event = react_open(core);
  if (self->down_event == react_ERROR) {
    if (verbosity >= 0)
      fprintf(stderr, "ctrldown handle creation failed\n");
    goto failure;
  }
  self->tidy_event = react_open(core);
  if (self->tidy_event == react_ERROR) {
    if (verbosity >= 0)
      fprintf(stderr, "tidy handle creation failed\n");
    goto failure;
  }
  react_direct(self->up_event, &on_ctrl_up, self);
  react_direct(self->down_event, &on_ctrl_down, self);
  react_direct(self->accept_event, &on_acceptable, self);
  react_direct(self->tidy_event, &on_tidy, self);

  /* Create names for the control FIFOs. */
  if (snprintf(self->upname, sizeof self->upname,
	       "%s/mp%u-up", tmpdir, self->id) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not write upstream pipe name %s/mp%u-up\n",
	      tmpdir, self->id);
    errno = ENOMEM;
    goto failure;
  }
  if (snprintf(self->downname, sizeof self->downname,
	       "%s/mp%u-down", tmpdir, self->id) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not write downstream pipe name %s/mp%u-down\n",
	      tmpdir, self->id);
    errno = ENOMEM;
    goto failure;
  }

  /* Create the FIFOs. */
  if (mkfifo(self->upname, 0600) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not create upstream pipe %s\n", self->upname);
    goto failure;
  }
  self->up_made = true;
  if (mkfifo(self->downname, 0600) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not create downstream pipe %s\n", self->downname);
    goto failure;
  }
  self->down_made = true;

  /* Open the connection to receive notifcations from the server.  We
   * must open it non-blocking, or we block until the peer opens the
   * FIFO. */
  self->downfd = open(self->downname, O_RDONLY | O_NONBLOCK);
  if (self->downfd < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not open downstream pipe %s\n", self->downname);
    goto failure;
  }
  if (self->verbosity >= 4)
    fprintf(stderr, "mp%u opened %s for reading\n", self->id, self->downname);

  /* Open the connection to send control information to the server.
   * We must open it non-blocking and for both read and write,
   * otherwise we block until the peer opens the FIFO, or we fail
   * immediately with ENXIO. */
  self->upfd = open(self->upname, O_RDWR | O_NONBLOCK);
  if (self->upfd < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not open upstream pipe %s\n", self->upname);
    goto failure;
  }
  if (self->verbosity >= 4)
    fprintf(stderr, "mp%u opened %s for writing\n", self->id, self->upname);

  /* Build the UNIX-domain socket. */
  if (snprintf(self->sockname, sizeof self->sockname,
	       "%s", conf->sockname) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not create socket name %s\n", conf->sockname);
    errno = ENOMEM;
    goto failure;
  }
  self->sockfd = socket(PF_UNIX, SOCK_STREAM, 0);
  if (self->sockfd < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not create socket\n");
    goto failure;
  }
  struct sockaddr_un addr;
  memset(&addr, 0, sizeof addr);
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, self->sockname, sizeof addr.sun_path);
  assert(addr.sun_path[sizeof addr.sun_path - 1] == '\0');
  if (bind(self->sockfd, (struct sockaddr *) &addr, sizeof addr) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not bind socket to %s\n", addr.sun_path);
    goto failure;
  }
  self->sock_made = true;
  if (chmod(self->sockname, conf->sockmode) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not change mode of socket %s to %#04o\n",
	      addr.sun_path, conf->sockmode);
    goto failure;
  }
  if (listen(self->sockfd, 5) < 0) {
    if (verbosity >= 0)
      fprintf(stderr, "could not listen on socket %s\n", addr.sun_path);
    goto failure;
  }

  return self;

 failure:
  ;
  int e = errno;
  destroy_piper(self);
  errno = e;
  return NULL;
}



static void await(void *vp, unsigned *remp, bool *failure)
{
  struct piper *self = vp;

  self->remp = remp;
  self->failure = failure;
  react_direct(self->down_event, &on_ctrl_ready, self);

  if (prime_control_down(self) < 0) {
    fprintf(stderr, "mp%u unable to prime signal from server: %s\n",
	    self->id, strerror(errno));
    *self->failure = true;
  } else {
    if (self->verbosity >= 4)
      fprintf(stderr, "mp%u waiting for signal from server\n", self->id);
  }
}

static void activate(void *vp)
{
  struct piper *self = vp;

  react_direct(self->down_event, &on_ctrl_down, self);

    /* Make sure we can detect a termination signal from the server. */
  if (prime_control_down(self) < 0) {
    self->term = errno;
    return;
  }

  /* Make sure we have a connection structure ready for the first
     connection, and that we are primed to accept it. */
  if (ensure_next_connection(self) < 0) {
    self->term = errno;
    return;
  }
}

static void destroy(void *vp)
{
  destroy_piper(vp);
}


static int test(void *vp)
{
  struct piper *self = vp;
  return self->term;
}

static int writecode(void *vp, char *buf, size_t len)
{
  struct piper *self = vp;
  return snprintf(buf, len, "multipipe:%s;%s", self->upname, self->downname);
}

static void serverdetach(void *vp)
{
  /* We're inside the daemon process, and we're about to be replaced
     with an exec* call. */

  struct piper *self = vp;

  /* Stop watching the descriptors we're about to close. */
  react_cancel(self->up_event);
  react_cancel(self->down_event);
  react_cancel(self->accept_event);

  /* Close down the descriptors we duplicated from the command
     process. */
  safeclose(&self->upfd);
  safeclose(&self->downfd);
  safeclose(&self->sockfd);
}

static const struct method_suite vtab = {
  .await = &await,
  .test = &test,
  .activate = &activate,
  .destroy = &destroy,
  .writecode = &writecode,
  .serverdetach = &serverdetach,
};

int multipiper_constr(struct method *meth,
		      const struct utils *utils,
		      const shared_opts *shared,
		      const void *config)
{
  meth->ctxt = make_piper(utils->core, utils->tmpdir, shared->verbosity, config);
  if (meth->ctxt == NULL) {
    meth->ctxt = NULL;
    meth->vtab = NULL;
    return -1;
  }
  meth->vtab = &vtab;
  return 0;
}
