// -*- c-file-style: "k&r"; c-basic-offset: 2; tab-width: 8; indent-tabs-mode: t -*-
// vim:set sts=2 ts=8:

/*
 * Copyright (c) 2012,2014-15, Regents of the University of Lancaster
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 * 
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 * 
 *  * Neither the name of the University of Lancaster nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef included_mux_proto_h
#define included_mux_proto_h

#include "mux_types.h"


#define mux_MUNK ((mux_msgtype) 0)

#define mux_MNOP ((mux_msgtype) 1)

/* The server is ready to accept sessions.  Data: 2-byte meta-data
 * length limit */
#define mux_MRDY ((mux_msgtype) 2)

/* The daemon wants to open a new session.  Data: client id;
 * meta-data */
#define mux_MNEW ((mux_msgtype) 3)

/* The server accepts a new session, an offers an identifier.
   Data: client id; server id */
#define mux_MACK ((mux_msgtype) 4)

/* The sender permits its peer to send some additionaly bytes.
   Data: id; length */
#define mux_MCTS ((mux_msgtype) 5)

/* The sender sends bytes.  Data: id; length; byte[length] */
#define mux_MSTR ((mux_msgtype) 6)

/* The sender will send no more bytes.  Data: id */
#define mux_MEOS ((mux_msgtype) 7)

/* The receiver will send no more bytes.  Data: id */
#define mux_MSTP ((mux_msgtype) 8)

#define mux_MAX_MSG_TYPE mux_MSTP

const char *mux_getmsgname(mux_msgtype);

#endif
