JAVACFLAGS += -source 1.7 -target 1.7

list-deps::
	@echo GNU-make POSIX.1-2001 ISO-C99 Java-1.7 Linux React Jardeps

list-rtdeps::
	@echo Java-1.7

list-exports::
	@echo linux

DEPS.c.react=2
DEPS.c.ddslib=1

autobuild-docs:: docs

docs:: out/usmux.pdf
	cp out/usmux.pdf $(ARCHIVE_DIR)/usmux$(DOC_CORE_SFX).pdf
